/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.command.reply.CommandReply;
import com.linkedlogics.megaco.parameter.ContextID;
import com.linkedlogics.megaco.parameter.TerminationID;
import com.linkedlogics.megaco.parameter.TopologyDirection;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author eatakishiyev
 */
public class Context implements Serializable {

    protected Future task ;
    private final ReentrantLock rlock = new ReentrantLock();

    private ContextID contextID;
    private TopologyDirection topologyDirection;
    private int priority = 0;
    private final HashMap<TerminationID, Termination> terminations = new HashMap<>();

    private final List<CommandReply> commandReplies = new ArrayList<>();

    private Object attachment;

    public Context(ContextID contextID) {
        this.contextID = contextID;
    }

    public ContextID getContextID() {
        return contextID;
    }

    public void setContextID(ContextID contextID) {
        this.contextID = contextID;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public TopologyDirection getTopologyDirection() {
        return topologyDirection;
    }

    public void setTopologyDirection(TopologyDirection topologyDirection) {
        this.topologyDirection = topologyDirection;
    }

    public Map<TerminationID, Termination> getTerminations() {
        return Collections.unmodifiableMap(terminations);
    }

    public Collection<Termination> listTerminations() {
        return Collections.unmodifiableCollection(terminations.values());
    }

    public void addTermination(Termination termination) {
        rlock.lock();
        try {
            termination.setContextID(contextID);
            terminations.put(termination.getTerminationID(), termination);
        } finally {
            rlock.unlock();
        }
    }

    protected Termination substractTermination(TerminationID terminationID) {
        rlock.lock();
        try {
            return terminations.remove(terminationID);
        } finally {
            rlock.unlock();
        }
    }

    public Termination getTermination(TerminationID terminationID) {
        rlock.lock();
        try {
            return terminations.get(terminationID);
        } finally {
            rlock.unlock();
        }
    }

    public void addCommandReply(CommandReply commandReply) {
        commandReplies.add(commandReply);
    }

    public void attach(Object attachment) {
        this.attachment = attachment;
    }

    public Object attachment() {
        return this.attachment;
    }
}
