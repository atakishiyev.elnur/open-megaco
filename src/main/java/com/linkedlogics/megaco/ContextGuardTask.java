/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.parameter.ContextID;

/**
 *
 * @author eatakishiyev
 */
public class ContextGuardTask implements Runnable {

    private final ContextID contextID;
    private final MegacoProvider provider;

    public ContextGuardTask(ContextID contextID, MegacoProvider provider) {
        this.contextID = contextID;
        this.provider = provider;
    }

    @Override
    public void run() {
        provider.removeContext(contextID);
    }

    public ContextID getContextID() {
        return contextID;
    }

}
