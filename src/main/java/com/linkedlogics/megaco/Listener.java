/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.command.reply.AmmsReply;
import com.linkedlogics.megaco.command.reply.AuditReply;
import com.linkedlogics.megaco.command.reply.NotifyReply;
import com.linkedlogics.megaco.command.reply.ServiceChangeReply;
import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import com.linkedlogics.megaco.parameter.TransactionID;

/**
 *
 * @author eatakishiyev
 */
public interface Listener {

    public void onAddReply(MegacoMgcTransaction transaction, Context context, AmmsReply addReply);

    public void onMoveReply(MegacoMgcTransaction transaction, Context context, AmmsReply moveReply);

    public void onModifyReply(MegacoMgcTransaction transaction, Context context, AmmsReply modReply);

    public void onSubtractReply(MegacoMgcTransaction transaction, Context context, AmmsReply subtractReply);

    public void onAuditCapReply(MegacoMgcTransaction transaction, Context context, AuditReply auditCapReply);

    public void onAuditValueReply(MegacoMgcTransaction transaction, Context context, AuditReply auditValueReply);

    public void onServiceChangeReply(MegacoMgcTransaction transaction, Context context, ServiceChangeReply serviceChangeReply);

    public void onNotifyReply(TransactionID transactionId, Context context, NotifyReply notifyReply);

    public void onError(TransactionID transactionId, Context context, ErrorDescriptor errorDescriptor);

    public void onTransactionTimeOut(MegacoMgcTransaction transaction);

}
