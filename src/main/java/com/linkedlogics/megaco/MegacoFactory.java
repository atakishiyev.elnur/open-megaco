/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.message.Message;
import com.linkedlogics.megaco.parameter.MegacoMessage;
import com.linkedlogics.megaco.parameter.MessageBody;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;

/**
 *
 * @author eatakishiyev
 */
public class MegacoFactory {

    private final MegacoStack megacoStack;

    public MegacoFactory(MegacoStack megacoStack) {
        this.megacoStack = megacoStack;
    }

    public MegacoMessage createMessage(byte[] data) throws AsnException, IOException {
        MegacoMessage megacoMessage = new MegacoMessage();
        AsnInputStream ais = new AsnInputStream(data);

        int tag = ais.readTag();
        megacoMessage.decode(ais.readSequenceStream());
        return megacoMessage;
    }

    public MegacoMessage createMessage() throws AsnException, IOException {
        MegacoMessage megacoMessage = new MegacoMessage();
        Message message = new Message();
        message.setVersion(MegacoStack.VERSION);
        message.setMid(megacoStack.mid);
        megacoMessage.setMessage(message);
        return megacoMessage;

    }

    public MegacoMessage createMessage(MessageBody messageBody) {
        MegacoMessage megacoMessage = new MegacoMessage();
        Message message = new Message(MegacoStack.VERSION, megacoStack.mid, messageBody);
        megacoMessage.setMessage(message);
        return megacoMessage;

    }
}
