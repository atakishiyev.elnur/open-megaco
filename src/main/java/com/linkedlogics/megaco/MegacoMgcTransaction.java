/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.action.ActionRequest;
import com.linkedlogics.megaco.command.request.AddRequest;
import com.linkedlogics.megaco.command.request.ModRequest;
import com.linkedlogics.megaco.command.request.NotifyRequest;
import com.linkedlogics.megaco.command.request.SubtractRequest;
import com.linkedlogics.megaco.descriptor.AmmDescriptor;
import com.linkedlogics.megaco.descriptor.AuditDescriptor;
import com.linkedlogics.megaco.descriptor.ObservedEventsDescriptor;
import com.linkedlogics.megaco.parameter.CommandRequest;
import com.linkedlogics.megaco.parameter.ContextID;
import com.linkedlogics.megaco.parameter.TerminationID;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.mobicents.protocols.asn.AsnException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author eatakishiyev
 */
public class MegacoMgcTransaction implements Runnable, MegacoTransaction {

    private final Logger logger = LoggerFactory.getLogger(MegacoMgcTransaction.class);
    private final long transactionId;
    protected final HashMap<ContextID, ActionRequest> actions = new HashMap<>();
    protected MegacoProvider provider;
    protected int retransmittedCount = 0;
    protected long requestTime = 0;
    protected long responseTime = 0;
    private Object attachment;
    protected boolean active = true;

    protected ScheduledFuture task;

    protected MegacoMgcTransaction(long transactionId, MegacoProvider provider) {
        this.transactionId = transactionId;
        this.provider = provider;
    }

    public void terminateTransaction() {
        if (task != null) {
            task.cancel(true);
        }
    }

    public void sendAddRequest(ContextID contextID, TerminationID[] terminationIDs, AmmDescriptor... ammDescriptors) {
        try {
            ActionRequest actionRequest = actions.get(contextID);

            if (actionRequest == null) {
                actionRequest = new ActionRequest(contextID);
                actions.put(contextID, actionRequest);
            }

            AddRequest addRequest = new AddRequest();
            addRequest.setTerminationIDList(new TerminationIDList(terminationIDs));
            if (ammDescriptors != null) {
                for (AmmDescriptor ammDescriptor : ammDescriptors) {
                    addRequest.addAmmDescriptor(ammDescriptor);
                }
            }

            CommandRequest commandRequest = new CommandRequest(addRequest);
            actionRequest.addCommandRequest(commandRequest);
        } finally {
        }
    }

    public void sendModifyRequest(ContextID contextID, TerminationID[] terminationIDs, AmmDescriptor... ammDescriptors) {
        try {
            ActionRequest actionRequest = actions.get(contextID);

            if (actionRequest == null) {
                actionRequest = new ActionRequest(contextID);
                actions.put(contextID, actionRequest);
            }

            ModRequest modRequest = new ModRequest();
            modRequest.setTerminationIDList(new TerminationIDList(terminationIDs));
            for (AmmDescriptor ammDescriptor : ammDescriptors) {
                modRequest.addAmmDescriptor(ammDescriptor);
            }

            CommandRequest commandRequest = new CommandRequest(modRequest);
            actionRequest.addCommandRequest(commandRequest);
        } finally {
        }
    }

    public void sendNotifyRequest(ContextID contextID, TerminationID[] terminationIDs, ObservedEventsDescriptor observedEventDescriptor) {
        try {
            ActionRequest actionRequest = actions.get(contextID);
            if (actionRequest == null) {
                actionRequest = new ActionRequest(contextID);
                actions.put(contextID, actionRequest);
            }

            NotifyRequest notifyRequest = new NotifyRequest(new TerminationIDList(terminationIDs), observedEventDescriptor);

            CommandRequest commandRequest = new CommandRequest(notifyRequest);
            actionRequest.addCommandRequest(commandRequest);
        } finally {
        }
    }

    public void sendModifyRequest(ContextID contextID, TerminationIDList terminationIDList, AmmDescriptor... ammDescriptors) {
        try {
            ActionRequest actionRequest = actions.get(contextID);

            if (actionRequest == null) {
                actionRequest = new ActionRequest(contextID);
                actions.put(contextID, actionRequest);
            }

            ModRequest modRequest = new ModRequest();
            modRequest.setTerminationIDList(terminationIDList);
            for (AmmDescriptor ammDescriptor : ammDescriptors) {
                modRequest.addAmmDescriptor(ammDescriptor);
            }

            CommandRequest commandRequest = new CommandRequest(modRequest);
            actionRequest.addCommandRequest(commandRequest);
        } finally {
        }
    }

    public void sendSubtractRequest(ContextID contextID, TerminationID[] terminationIDs, AuditDescriptor auditDescriptor) {
        try {
            ActionRequest actionRequest = actions.get(contextID);

            if (actionRequest == null) {
                actionRequest = new ActionRequest(contextID);
                actions.put(contextID, actionRequest);
            }

            CommandRequest commandRequest = new CommandRequest(new SubtractRequest(new TerminationIDList(terminationIDs), auditDescriptor));
            actionRequest.addCommandRequest(commandRequest);
        } finally {
        }
    }

    public void sendSubtractRequest(ContextID contextID, TerminationIDList terminationIDs, AuditDescriptor auditDescriptor) {
        try {
            ActionRequest actionRequest = actions.get(contextID);

            if (actionRequest == null) {
                actionRequest = new ActionRequest(contextID);
                actions.put(contextID, actionRequest);
            }

            CommandRequest commandRequest = new CommandRequest(new SubtractRequest(terminationIDs, auditDescriptor));
            actionRequest.addCommandRequest(commandRequest);
        } finally {
        }
    }

    public void send(long timeOut) throws AsnException, IOException {
        provider.getMegacoStack().sendRequest(this, timeOut);
    }

//    protected void retry() throws AsnException, IOException {
//        if (provider != null) {
//            retransmittedCount++;
//            if (retransmittedCount > provider.maxRetransmitCount) {
//                task.cancel(false);
//                provider.listener.onTransactionTimeOut(this);
//                provider.removeTransaction(this.transactionId);
//                return;
//            }
//            provider.retry(this);
//
//        } else {
//            logger.warn("Retry fails: Provider not set to transaction . TransactionId = " + transactionId);
//            provider.removeTransaction(this.transactionId);
//            task.cancel(false);
//        }
//    }
    public long getTransactionId() {
        return transactionId;
    }

    @Override
    public void run() {
        provider.listener.onTransactionTimeOut(this);
        provider.removeTransaction(this.transactionId);
    }

    public void attach(Object attachment) {
        this.attachment = attachment;
    }

    public Object getAttachment() {
        return attachment;
    }

}
