/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.action.ActionReply;
import com.linkedlogics.megaco.action.ActionRequest;
import com.linkedlogics.megaco.command.reply.AddReply;
import com.linkedlogics.megaco.command.reply.CommandReply;
import com.linkedlogics.megaco.command.reply.ModReply;
import com.linkedlogics.megaco.command.reply.MoveReply;
import com.linkedlogics.megaco.command.reply.NotifyReply;
import com.linkedlogics.megaco.command.reply.ReplyType;
import com.linkedlogics.megaco.command.reply.SubtractReply;
import com.linkedlogics.megaco.command.request.AddRequest;
import com.linkedlogics.megaco.command.request.Command;
import com.linkedlogics.megaco.command.request.ModRequest;
import com.linkedlogics.megaco.command.request.NotifyRequest;
import com.linkedlogics.megaco.command.request.RequestType;
import com.linkedlogics.megaco.command.request.ServiceChangeReason;
import com.linkedlogics.megaco.command.request.ServiceChangeRequest;
import com.linkedlogics.megaco.command.request.SubtractRequest;
import com.linkedlogics.megaco.descriptor.AmmDescriptor;
import com.linkedlogics.megaco.descriptor.EventsDescriptor;
import com.linkedlogics.megaco.descriptor.MediaDescriptor;
import com.linkedlogics.megaco.descriptor.ObservedEventsDescriptor;
import com.linkedlogics.megaco.descriptor.SignalsDescriptor;
import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import com.linkedlogics.megaco.parameter.AuditReturnParameter;
import com.linkedlogics.megaco.parameter.CommandRequest;
import com.linkedlogics.megaco.parameter.ContextID;
import com.linkedlogics.megaco.parameter.MegacoMessage;
import com.linkedlogics.megaco.parameter.MessageBody;
import com.linkedlogics.megaco.parameter.ObservedEvent;
import com.linkedlogics.megaco.parameter.SeqSigList;
import com.linkedlogics.megaco.parameter.ServiceChangeMethod;
import com.linkedlogics.megaco.parameter.ServiceChangeParm;
import com.linkedlogics.megaco.parameter.ServiceState;
import com.linkedlogics.megaco.parameter.Signal;
import com.linkedlogics.megaco.parameter.SignalRequest;
import com.linkedlogics.megaco.parameter.StreamParms;
import com.linkedlogics.megaco.parameter.TerminationAudit;
import com.linkedlogics.megaco.parameter.TerminationID;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import com.linkedlogics.megaco.parameter.TransactionID;
import com.linkedlogics.megaco.parameter.TransactionResult;
import com.linkedlogics.megaco.transaction.Transaction;
import com.linkedlogics.megaco.transaction.TransactionReply;
import com.linkedlogics.megaco.transaction.TransactionRequest;
import com.linkedlogics.megaco.transaction.TransactionType;
import com.linkedlogics.networking.Connection;
import com.linkedlogics.sctp.SctpConnection;
import com.linkedlogics.state.machines.FiniteStateMachine;
import com.linkedlogics.state.machines.State;
import com.sun.nio.sctp.MessageInfo;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import org.mobicents.protocols.asn.AsnException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.linkedlogics.sctp.SctpListener;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author eatakishiyev
 */
public class MegacoProvider implements SctpListener {

    private final int concurrentSessions;

    private final ReentrantLock mutex = new ReentrantLock();

    protected final HashMap<Long, MegacoTransaction> transactions = new HashMap<>();
    private final ConcurrentHashMap<ContextID, Context> contexts = new ConcurrentHashMap<>();
    private transient final AtomicInteger contextIdSequencer = new AtomicInteger(1);
    protected transient final ScheduledExecutorService terminationGuard = Executors.newScheduledThreadPool(8);

//    private final LongKeyConcurrentHashMap replies = new LongKeyConcurrentHashMap(1000000);
    private final transient Logger logger = LoggerFactory.getLogger(MegacoProvider.class);

    protected transient final FiniteStateMachine controlAssociation = new FiniteStateMachine();
    protected transient Listener listener;
    private int outStreamCount = 0;
    protected long terminationLifeTime = 300;

    protected long longTimer = 30;// It should be configurable in seconds
//    

    private final String localAddress;// It should be configurable
    private final int localPort;// It should be configurable
    private final String remoteAddress;// It should be configurable
    private final int remotePort;// It should be configurable

//    private volatile AtomicInteger traverser = new AtomicInteger(0);
    private final MegacoStack megacoStack;
//    

    private final String name;// It should be configurable

    private final AtomicLong acknowledgementCount = new AtomicLong(0);
    private final AtomicLong acknowledgementDelay = new AtomicLong(0);

    private final SctpConnection connection;

    private final transient MegacoFactory megacoFactory;
    private final Random streamRandomizer = new Random();

    private int contextGuardTimerValue;

    private final transient ScheduledExecutorService expirationTimer = Executors.newScheduledThreadPool(2, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "MegacoTransactionExpirationTimer");
        }
    });

    private final transient ScheduledExecutorService contextGuardTimer = Executors.newScheduledThreadPool(3, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "ContextGuardTimer");
        }
    });

    protected MegacoProvider(MegacoStack stack, String name, int concurrentSessions, String localAddress, int localPort,
            String remoteAddress, int remotePort, int inStream, int outStream, boolean isServer, long terminationLifeTime) throws Exception {
        this.megacoStack = stack;
        this.terminationLifeTime = terminationLifeTime;
        this.concurrentSessions = concurrentSessions;
        if (isServer) {
            this.connection = megacoStack.sctpProvider.
                    createServerAssociation(new InetSocketAddress(localAddress, localPort),
                            new InetSocketAddress(remoteAddress, remotePort), inStream, outStream);
        } else {
            this.connection = megacoStack.sctpProvider.createClientAssociation(new InetSocketAddress(localAddress, localPort),
                    new InetSocketAddress(remoteAddress, remotePort), inStream, outStream);
        }

        this.outStreamCount = outStream;

        connection.setListener(this);
        this.connection.open();
        this.localAddress = localAddress;
        this.localPort = localPort;

        this.remoteAddress = remoteAddress;
        this.remotePort = remotePort;
        this.name = name;

        this.megacoFactory = new MegacoFactory(stack);

        this.initMGCControlAssociation();
    }

    @Override
    public void onData(byte[] data, MessageInfo msgInfo) {

        try {
            MegacoMessage incomingMessage = megacoFactory.createMessage(data);

            ErrorDescriptor errorDescriptor = incomingMessage.getMessage().getMessageBody().getMessageError();
            if (errorDescriptor != null) {
//                    listener.onError(null, null, errorDescriptor);
            } else {
                this.onMegacoMessage(incomingMessage);
            }
        } catch (Exception ex) {
            logger.error("Error while handling message: data = " + DatatypeConverter.printHexBinary(data), ex);
        }
    }

    private void onMegacoMessage(MegacoMessage incomingMessage) throws Exception {

        MegacoMessage responseMessage = this.megacoFactory.createMessage();
        Iterator<Transaction> transactionsIter = incomingMessage.getMessage().getMessageBody().getTransactions().iterator();
        while (transactionsIter.hasNext()) {
            Transaction transaction = transactionsIter.next();

            long transactionId = transaction.getTransactionID().getTransactionId();
            if (logger.isDebugEnabled()) {
                logger.debug(transactionId + ":[H.248]:onMegacoMessage: Processing H.248 message.");
            }

            try {
                TransactionType tt = transaction.getTransactionType();
                switch (tt) {
                    case REQUEST:

                        TransactionReply transactionReply = new TransactionReply();
                        transactionReply.setTransactionResult(new TransactionResult());

                        if (logger.isDebugEnabled()) {
                            logger.debug(transactionId + ":[H.248]:onMegacoMessage:Handling request transaction.");
                        }
                        this.processTransactionRequest(transactionReply, (TransactionRequest) transaction);
                        break;
                    case REPLY:
                        MegacoMgcTransaction megacoTransaction = (MegacoMgcTransaction) this.removeTransaction(transactionId);
                        if (megacoTransaction == null) {
                            logger.error(transaction + ":[H.248]:onMegacoMessage: No transaction exists for reply. TransactionId = " + transactionId);
                        } else {
                            if (logger.isDebugEnabled()) {
                                logger.debug(transactionId + ":[H.248]:onMegacoMessage: Transaction found. Processing megaco response.");
                            }

                            megacoTransaction.responseTime = System.currentTimeMillis();
                            this.processTransactionReply(megacoTransaction, transaction);
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error("ErrorOccured while processing transaction. TransactionId = " + transaction.getTransactionID().getTransactionId(), ex);
            }
//            });

        }

    }

    @Override
    public void onCommunicationUp(Connection assoc) {
        logger.info("Communication established");
    }

    public void start() throws AsnException, IOException {
        if (megacoStack.getOperationMode() == OperationMode.MG) {
            ServiceChangeParm serviceChangeParm = new ServiceChangeParm(ServiceChangeMethod.RESTART);
            serviceChangeParm.setServiceChangeReason(ServiceChangeReason.COLD_BOOT);

            ServiceChangeRequest serviceChangeRequest = new ServiceChangeRequest(serviceChangeParm, TerminationID.TERMINATION_ID_CHOOSE);
            serviceChangeRequest.setServiceChangeParms(serviceChangeParm);

            CommandRequest commandRequest = new CommandRequest(serviceChangeRequest);
            ActionRequest actionRequest = new ActionRequest(ContextID.CONTEXT_ID_CHOOSE);
            actionRequest.addCommandRequest(commandRequest);
            TransactionRequest transactionRequest = new TransactionRequest(1232);
            transactionRequest.addAction(actionRequest);
            MessageBody messageBody = new MessageBody(transactionRequest);
            MegacoMessage megacoMessage = this.megacoFactory.createMessage(messageBody);

            sendMegacoMessage(megacoMessage);
        } else {

        }
    }

    public void stop() {

    }

    private void initMGCControlAssociation() throws Exception {
        State stateOutOfSvc = controlAssociation.createState(ServiceState.OUT_OF_SVC.name());
        State stateInSvc = controlAssociation.createState(ServiceState.IN_SVC.name());
        State stateTest = controlAssociation.createState(ServiceState.TEST.name());
        State stateGracefull = controlAssociation.createState("GRACEFUL");

        //OutOfService state transitions
        stateOutOfSvc.createTransition(ServiceChangeMethod.FORCED.name(), stateOutOfSvc);
        stateOutOfSvc.createTransition(ServiceChangeMethod.GRACEFUL.name(), stateOutOfSvc);
        stateOutOfSvc.createTransition(ServiceChangeMethod.RESTART.name(), stateInSvc);
        stateOutOfSvc.createTransition(ServiceChangeMethod.DISCONNECTED.name(), stateInSvc);
        stateOutOfSvc.createTransition(ServiceChangeMethod.FAILOVER.name(), stateInSvc);
        stateOutOfSvc.createTransition(ServiceChangeMethod.HANDOFF.name(), stateInSvc);
        stateOutOfSvc.createTransition(ServiceState.TEST.name(), stateTest);
        //InService state transitions
        stateInSvc.createTransition(ServiceChangeMethod.RESTART.name(), stateInSvc);
        stateInSvc.createTransition(ServiceChangeMethod.DISCONNECTED.name(), stateInSvc);
        stateInSvc.createTransition(ServiceChangeMethod.FORCED.name(), stateOutOfSvc);
        stateInSvc.createTransition(ServiceState.TEST.name(), stateTest);
        //Test state transitions
        stateTest.createTransition(ServiceChangeMethod.FORCED.name(), stateOutOfSvc);
        controlAssociation.setStart(stateOutOfSvc);

    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public Listener getListener() {
        return listener;
    }

    private void processTransactionReply(MegacoMgcTransaction megacoMgcTransaction, Transaction transaction) {
        TransactionReply transactionReply = (TransactionReply) transaction;
        TransactionID transactionID = transactionReply.getTransactionID();
        List<ActionReply> actions = transactionReply.getTransactionResult().getActionReplies();

        for (ActionReply actionReply : actions) {
            List<CommandReply> commandReplys = actionReply.getCommandReplys();
            for (CommandReply commandReply : commandReplys) {
                ReplyType rt = commandReply.getType();
                switch (rt) {
                    case ADD_REPLY:
                        Context ctx = createContext(actionReply.getContextId());
                        ((Listener) listener).onAddReply(megacoMgcTransaction, ctx, (AddReply) commandReply);
                        break;
                    case MOD_REPLY:
                        ctx = contexts.get(actionReply.getContextId());
                        ((Listener) listener).onModifyReply(megacoMgcTransaction, ctx, (ModReply) commandReply);
                        break;
                    case MOVE_REPLY:
                        ctx = contexts.get(actionReply.getContextId());
                        ((Listener) listener).onMoveReply(megacoMgcTransaction, ctx, (MoveReply) commandReply);
                        break;
                    case SUBTRACT_REPLY:
                        ctx = removeContext(actionReply.getContextId());
                        ((Listener) listener).onSubtractReply(megacoMgcTransaction, ctx, (SubtractReply) commandReply);
                        break;
                    case SERVICE_CHANGE_REPLY:
                }
            }
        }
    }

    private void processTransactionRequest(TransactionReply transactionReply, TransactionRequest transactionRequest) throws Exception {
        TransactionID transactionId = transactionRequest.getTransactionID();
        List<ActionRequest> actions = transactionRequest.getActions();
        for (ActionRequest actionRequest : actions) {

            ActionReply actionReply = new ActionReply(actionRequest.getContextId());// auto generating reply

            List<CommandRequest> commandRequests = actionRequest.getCommandRequests();
            for (CommandRequest commandRequest : commandRequests) {
                Command command = commandRequest.getCommand();
                RequestType commandType = command.getRequestType();

                if (megacoStack.operationMode == OperationMode.MGC && commandType != RequestType.NOTIFY_REQ) {
                    throw new UnexpectedCommandException("Node is operating as MGC. Unexpected Command " + commandType + " received by peer.");
                }

                switch (commandType) {
                    case ADD_REQ:
                        if (logger.isDebugEnabled()) {
                            logger.debug(transactionId + ":[H.248]:ProcessTransactionRequest: Processing AddRequest.");
                        }
                        AddRequest addRequest = (AddRequest) command;
                        Context ctx = null;
                        if (actionRequest.getContextId().equals(ContextID.CONTEXT_ID_CHOOSE)) {
                            ctx = createContext();
                            if (logger.isDebugEnabled()) {
                                logger.debug(transactionId + ":[H.248]:ProcessTransactionRequest: "
                                        + "AddRequest come with CONTEX_ID_CHOOSE context id, created new empty context. " + ctx.getContextID());
                            }
                        } else {
                            ctx = contexts.get(actionRequest.getContextId());
                            if (logger.isDebugEnabled()) {
                                logger.debug(transactionId + ":[H.248]:ProcessTransactionRequest: "
                                        + "AddRequest come with " + actionRequest.getContextId());
                            }
                        }

                        if (ctx == null) {
                            logger.warn(transactionId + ":[H.248]:ProcessTransactionRequest: "
                                    + "No context found/created.");
                            actionReply.setErrorDescriptor(ErrorDescriptor.ERROR_411);
                            actionReply.setContextId(actionRequest.getContextId());
                            transactionReply.getTransactionResult().addActionReply(actionReply);
                            transactionReply.setTransactionId(transactionId);

                            MessageBody messageBody = new MessageBody(transactionReply);
                            MegacoMessage megacoMessage = this.getMegacoFactory().createMessage(messageBody);

                            sendMegacoMessage(megacoMessage);

                            return;
                        }

                        actionReply.setContextId(ctx.getContextID());//override context id with new created one

                        try {
                            Termination termination = (Termination) megacoStack.terminationConcreteClass.newInstance();
                            termination.setContextID(ctx.getContextID());
                            termination.setTerminationID(new TerminationID(megacoStack.generateTerminationId()));
                            termination.provider = this;

                            if (logger.isDebugEnabled()) {
                                logger.debug(transactionId + ":[H.248]:ProcessTransactionRequest: New termination created "
                                        + termination.getTerminationID() + " TerminationConcreteClass = " + termination.getClass().getCanonicalName());
                            }

                            //Preparing reply
                            AddReply addReply = new AddReply();
                            addReply.setTerminationIDList(new TerminationIDList(termination.getTerminationID()));
                            actionReply.addCommandReply(addReply);
                            TerminationAudit terminationAudit = new TerminationAudit();

                            //Traverse through descriptors
                            List<AmmDescriptor> ammDescriptors = addRequest.getDescriptors();
                            for (AmmDescriptor ammDescriptor : ammDescriptors) {
                                if (ammDescriptor instanceof MediaDescriptor) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug(transactionId + ":[H.248]:ProcessTransactionRequest: Handling descriptor type MediaDescriptor");
                                    }

                                    MediaDescriptor mediaDescriptor = ((MediaDescriptor) ammDescriptor);
                                    termination.setTerminationStateDescriptor(mediaDescriptor.getTermStateDescr());
                                    boolean oneStream = mediaDescriptor.getStreams().isOneStream();
                                    if (oneStream) {//one stream configured in termination
                                        if (logger.isDebugEnabled()) {
                                            logger.debug(transactionId + ":[H.248]:ProcessTransactionRequest: One stream MediaDescriptor");
                                        }
                                        StreamParms streamParams = mediaDescriptor.getStreams().getOneStream();

                                        Stream stream = (Stream) megacoStack.streamConcreteClass.newInstance();
                                        if (logger.isDebugEnabled()) {
                                            logger.debug(transactionId + ":[H.248]:ProcessTransactionRequest: Stream created. "
                                                    + megacoStack.streamConcreteClass.getCanonicalName());
                                        }
                                        stream.setStreamParms(streamParams);
                                        stream.termination = termination;
                                        termination.setStream(stream);
                                        AmmDescriptor[] descriptors = stream.onCreated();
                                        if (descriptors != null) {
                                            for (AmmDescriptor descriptor : descriptors) {
                                                terminationAudit.addAuditReturnParameter(new AuditReturnParameter(descriptor));
                                            }
                                        }
                                    } else {//multiple stream configured in termination
//                                        List<StreamDescriptor> streamDescriptors = mediaDescriptor.getStreams().getMultiStream();
//                                        for (StreamDescriptor streamDescriptor : streamDescriptors) {
//                                            Stream stream = (Stream) megacoStack.streamConcreteClass.newInstance();
//                                            stream.setStreamID(streamDescriptor.getStreamId());
//                                            stream.setStreamParms(streamDescriptor.getStreamParms());
//                                            stream.termination = termination;
//                                            termination.addStream(stream);
//                                            AmmDescriptor[] descriptors = stream.onCreated();
//                                            for (AmmDescriptor descriptor : descriptors) {
//                                                terminationAudit.addAuditReturnParameter(new AuditReturnParameter(descriptor));
//                                            }
//                                        }
                                        throw new UnsupportedOperationException("Multi stream termination not supporting at this time.");
                                    }
//                                    terminationAudit.addAuditReturnParameter(new AuditReturnParameter(mediaDescriptor));
                                } else if (ammDescriptor instanceof EventsDescriptor) {
                                    EventsDescriptor eventsDescriptor = (EventsDescriptor) ammDescriptor;
                                    termination.setEventsDescriptor(eventsDescriptor);
                                    terminationAudit.addAuditReturnParameter(new AuditReturnParameter(eventsDescriptor));
                                } else if (ammDescriptor instanceof SignalsDescriptor) {
                                    SignalsDescriptor signalsDescriptor = (SignalsDescriptor) ammDescriptor;
                                    List<SignalRequest> signalRequests = signalsDescriptor.getSignalRequests();
                                    for (SignalRequest signalRequest : signalRequests) {
                                        Signal signal = signalRequest.getSignal();
                                        if (signal != null) {
                                            this.processSignal(transactionId, termination, ctx, signal);
                                        } else if (signalRequest.getSeqSigList() != null) {
                                            SeqSigList seqSigList = signalRequest.getSeqSigList();
                                            List<Signal> signals = seqSigList.getSignalList();
                                            for (Signal _signal : signals) {
                                                this.processSignal(transactionId, termination, ctx, _signal);
                                            }
                                        }
                                    }
                                    terminationAudit.addAuditReturnParameter(new AuditReturnParameter(signalsDescriptor));
                                }

                                if (!terminationAudit.getAuditReturnParameters().isEmpty()) {
                                    addReply.setTerminationAudit(terminationAudit);
                                }
                            }

                            ctx.addTermination(termination);
                            termination.scheduleGuard();
                            termination.onCreated();

                        } catch (IllegalAccessException | InstantiationException ex) {
                            logger.error("", ex);
                        }

                        break;
                    case MOD_REQ:
                        ctx = contexts.get(actionRequest.getContextId());
                        ModRequest modifyRequest = (ModRequest) command;

                        if (ctx == null) {
                            ErrorDescriptor errorDescriptor = ErrorDescriptor.ERROR_411;
                            TerminationAudit terminationAudit = new TerminationAudit();
                            terminationAudit.addAuditReturnParameter(new AuditReturnParameter(errorDescriptor));
                        } else {
                            ModReply modifyReply = new ModReply();
                            List<AmmDescriptor> descriptors = modifyRequest.getDescriptors();
                            List<TerminationID> terminationIDs = modifyRequest.getTerminationIDList().getTerminationIDs();

                            TerminationAudit terminationAudit = new TerminationAudit();
                            for (TerminationID terminationID : terminationIDs) {
                                Termination termination = ctx.getTermination(terminationID);
                                if (termination == null) {
                                    logger.error("No termination " + terminationID + " found in the context " + ctx);
                                    continue;
                                }

                                for (AmmDescriptor ammDescriptor : descriptors) {
                                    if (ammDescriptor instanceof SignalsDescriptor) {
                                        SignalsDescriptor signalsDescriptor = (SignalsDescriptor) ammDescriptor;
                                        List<SignalRequest> signalRequests = signalsDescriptor.getSignalRequests();
                                        for (SignalRequest signalRequest : signalRequests) {
                                            Signal signal = signalRequest.getSignal();
                                            if (signal != null) {
                                                this.processSignal(transactionId, termination, ctx, signal);
                                            } else {
                                                SeqSigList seqSigList = signalRequest.getSeqSigList();
                                                List<Signal> signals = seqSigList.getSignalList();
                                                for (Signal _signal : signals) {
                                                    this.processSignal(transactionId, termination, ctx, _signal);
                                                }
                                            }
                                        }
                                        terminationAudit.addAuditReturnParameter(new AuditReturnParameter(signalsDescriptor));
                                        termination.onModified();
                                    } else if (ammDescriptor instanceof EventsDescriptor) {
                                        EventsDescriptor eventDescriptor = (EventsDescriptor) ammDescriptor;
                                        termination.setEventsDescriptor(eventDescriptor);
                                        terminationAudit.addAuditReturnParameter(new AuditReturnParameter(eventDescriptor));
                                        termination.onModified();

                                    } else if (ammDescriptor instanceof MediaDescriptor) {
                                        MediaDescriptor mediaDescriptor = ((MediaDescriptor) ammDescriptor);
                                        termination.setTerminationStateDescriptor(mediaDescriptor.getTermStateDescr());
                                        boolean oneStream = mediaDescriptor.getStreams().isOneStream();
                                        if (oneStream) {//one stream configured in termination
                                            StreamParms streamParams = mediaDescriptor.getStreams().getOneStream();
                                            Stream stream = termination.getStream();
                                            stream.setStreamParms(streamParams);
                                            AmmDescriptor[] resultDescriptors = stream.onModified();
                                            if (resultDescriptors != null) {
                                                for (AmmDescriptor descriptor : resultDescriptors) {
                                                    terminationAudit.addAuditReturnParameter(new AuditReturnParameter(descriptor));
                                                }
                                            }
                                        } else {//multiple stream configured in termination
                                            throw new UnsupportedOperationException("Multi streaming not supporting on this termination.");
//                                            List<StreamDescriptor> streamDescriptors = mediaDescriptor.getStreams().getMultiStream();
//                                            for (StreamDescriptor streamDescriptor : streamDescriptors) {
//                                                Stream stream = (Stream) megacoStack.streamConcreteClass.newInstance();
//                                                stream.setStreamID(streamDescriptor.getStreamId());
//                                                stream.setStreamParms(streamDescriptor.getStreamParms());
//                                                stream.termination = termination;
//                                                termination.addStream(stream);
//                                                AmmDescriptor[] _descriptors = stream.onCreated();
//                                                for (AmmDescriptor descriptor : _descriptors) {
//                                                    terminationAudit.addAuditReturnParameter(new AuditReturnParameter(descriptor));
//                                                }
//                                            }
                                        }
                                    }
                                }

                                modifyReply.setTerminationIDList(modifyRequest.getTerminationIDList());
                                if (!terminationAudit.getAuditReturnParameters().isEmpty()) {
                                    modifyReply.setTerminationAudit(terminationAudit);
                                }
                                actionReply.addCommandReply(modifyReply);
                            }
                        }
                        break;
                    case MOVE_REQ:
                        //TODO: implement move request
                        break;
                    case SUBTRACT_REQ:
                        SubtractRequest subtractRequest = (SubtractRequest) command;
                        ctx = contexts.get(actionRequest.getContextId());
                        if (ctx == null) {
                            ErrorDescriptor errorDescriptor = ErrorDescriptor.ERROR_411;
                            TerminationAudit terminationAudit = new TerminationAudit();
                            terminationAudit.addAuditReturnParameter(new AuditReturnParameter(errorDescriptor));
                        } else {
                            SubtractReply subtractReply = new SubtractReply();
                            List<TerminationID> terminationIDs = subtractRequest.getTerminationIDList().getTerminationIDs();
                            for (TerminationID terminationID : terminationIDs) {
                                Termination termination = ctx.substractTermination(terminationID);
                                if (termination != null) {
                                    termination.cancelGuard();
                                    termination.onDestroyed(false);
                                    Stream stream = termination.getStream();
                                    if (stream != null) {
                                        stream.onDestroyed(false);
                                    }
                                }
                            }

                            if (ctx.getTerminations().isEmpty()) {
                                this.contexts.remove(ctx.getContextID());
                            }

                            subtractReply.setTerminationIDList(subtractRequest.getTerminationIDList());
                            actionReply.addCommandReply(subtractReply);
                        }
                        break;
                    case NOTIFY_REQ:
                        ctx = contexts.get(actionRequest.getContextId());
                        if (ctx == null) {
                            logger.error("No active context found for NotifyRequest. TransactionId = " + transactionId + " ContextId = " + actionRequest.getContextId());
                        } else {
                            NotifyRequest notifyRequest = (NotifyRequest) command;
                            ObservedEventsDescriptor observedEventsDescriptor
                                    = notifyRequest.getObservedEventsDescriptor();
                            for (ObservedEvent observedEvent : observedEventsDescriptor.getObservedEventList()) {
                                int pkgId = observedEvent.getEventName().getPackageName();
                                com.linkedlogics.megaco.packages.Package pkg = megacoStack.configuredPackages.get(pkgId);
                                if (pkg != null) {
                                    for (TerminationID terminationID : notifyRequest.getTerminationID().getTerminationIDs()) {
                                        Termination termination = ctx.getTermination(terminationID);
                                        pkg.handleEvent(this, transactionId, termination, ctx, observedEvent);
                                    }
                                } else {
                                    logger.warn("No appropriate configured package"
                                            + " found for incoming event: "
                                            + observedEvent.getEventName());
                                }
                            }
                            actionReply.addCommandReply(new NotifyReply(notifyRequest.getTerminationID()));
                        }
                        break;
                    case SERVICE_CHANGE_REQ:
                        //TODO: implement
                        break;
                }
            }
            transactionReply.getTransactionResult().addActionReply(actionReply);
            transactionReply.setTransactionId(transactionId);

            MessageBody messageBody = new MessageBody(transactionReply);
            MegacoMessage megacoMessage = this.getMegacoFactory().createMessage(messageBody);

            sendMegacoMessage(megacoMessage);
        }
    }

    public Context getContext(ContextID contextID) {
        mutex.lock();
        try {
            return contexts.get(contextID);
        } finally {
            mutex.unlock();
        }
    }

    private Context createContext() {
        mutex.lock();
        try {
            ContextID contextID = new ContextID(contextIdSequencer.getAndIncrement());
            Context context = new Context(contextID);
            contexts.put(contextID, context);
            return context;
        } finally {
            mutex.unlock();
        }
    }

    private Context createContext(ContextID contextID) {
        mutex.lock();
        try {
            Context ctx = new Context(contextID);
            contexts.put(contextID, ctx);
            if (contextGuardTimerValue > 0) {
                ctx.task = contextGuardTimer.schedule(new ContextGuardTask(contextID, this),
                        contextGuardTimerValue, TimeUnit.SECONDS);
            }
            return ctx;
        } finally {
            mutex.unlock();
        }
    }

    protected Context removeContext(ContextID contextID) {
        mutex.lock();
        try {
            Context ctx = contexts.remove(contextID);
            if (ctx != null && ctx.task != null) {
                ctx.task.cancel(true);
            }
            return ctx;
        } finally {
            mutex.unlock();
        }
    }

    public Termination substractTermination(Context context, TerminationID terminationID) {
        mutex.lock();
        try {
            Termination termination = context.substractTermination(terminationID);
            if (context.getTerminations().isEmpty()) {
                contexts.remove(context.getContextID());
            }
            return termination;
        } finally {
            mutex.unlock();
        }
    }

    public Collection<Context> getContexts() {
        return Collections.unmodifiableCollection(contexts.values());
    }

    public Termination getTermination(ContextID contextID, TerminationID terminationID) {
        mutex.lock();
        try {
            Context context = contexts.get(contextID);
            if (context == null) {
                return null;
            }
            return context.getTermination(terminationID);
        } finally {
            mutex.unlock();
        }
    }

    private void processSignal(TransactionID transactionID, Termination termination,
            Context context, Signal signal) throws Exception {
        com.linkedlogics.megaco.packages.Package configuredPackage
                = megacoStack.configuredPackages.get(signal.getSignalName().getPackageName());

        if (configuredPackage != null) {
            configuredPackage.handleSignal(this, transactionID, termination, context,
                    signal);
        } else {
            logger.warn("No package configuration found for " + signal.getSignalName());
        }
    }

    private void processEvent(TransactionID transactionID, Termination termination,
            Context contex, ObservedEvent event) {
        com.linkedlogics.megaco.packages.Package configuredPackage
                = megacoStack.configuredPackages.get(event.getEventName().getPackageName());
        if (configuredPackage != null) {
            configuredPackage.handleEvent(this, transactionID, termination, contex,
                    event);
        } else {
            logger.warn("No package configuration found for " + event.getEventName());
        }
    }

    public MegacoTransaction findTransaction(TransactionID transactionID) {
        return this.transactions.get(transactionID.getTransactionId());
    }

    private void sendMegacoMessage(MegacoMessage msg) throws AsnException, IOException {

        int number = streamRandomizer.nextInt(outStreamCount);
//        System.out.println("OutStreamCount = " + outStreamCount + " GeneratedId = " + number);
        connection.write(msg.encode(), number, MegacoStack.SCTP_MEGACO_PAYLOAD_ID);
//        connection.write(msg.encode());
    }

    protected void removeTimeOutTermination(TerminationID terminationID, ContextID ctxId) {
        Context ctx = contexts.get(ctxId);
        if (ctx != null) {
            Termination termination = ctx.substractTermination(terminationID);
            if (termination != null) {
                termination.onDestroyed(true);
                Stream stream = termination.getStream();
                if (stream != null) {
                    stream.onDestroyed(true);
                }
            }
            if (ctx.getTerminations().isEmpty()) {
                this.contexts.remove(ctxId);
            }
        }
    }

    public MegacoMgcTransaction createTransaction() {
        return new MegacoMgcTransaction(megacoStack.generateTransactionId(), this);
    }

    @Override
    public void onCommunicationDown(Connection assoc) {
        logger.warn("Communication Lost");
    }

    @Override
    public void onCommunicationShutdown(Connection assoc) {
        logger.warn("Communication Shutdown");
    }

    protected void sendTransaction(MegacoMgcTransaction transaction, long timeOut) throws AsnException, IOException {
        mutex.lock();
        try {
            this._send(transaction);
            transactions.put(transaction.getTransactionId(), transaction);

            transaction.task = expirationTimer.schedule(transaction,
                    timeOut, TimeUnit.SECONDS);

        } finally {
            mutex.unlock();
        }
    }

//    protected boolean sendTransaction(MegacoMgcTransaction transaction, long timeout) throws AsnException, IOException, InterruptedException {
//        this._send(transaction);
//        transactions.put(transaction.getTransactionId(), transaction);
//        if (!transaction.await(timeout)) {
//            removeTransaction(transaction.getTransactionId());
////            this.listener.onTransactionTimeOut(transaction);
//            return false;
//        } else {
//            return true;
//        }
//    }
    private void _send(MegacoMgcTransaction transaction) throws AsnException, IOException {
        TransactionRequest transactionRequest = new TransactionRequest(transaction.getTransactionId());
        for (ActionRequest actionRequest : transaction.actions.values()) {
            transactionRequest.addAction(actionRequest);
        }

        MessageBody messageBody = new MessageBody(transactionRequest);
        MegacoMessage message = getMegacoFactory().createMessage(messageBody);

        this.sendMegacoMessage(message);

    }

    public String getName() {
        return name;
    }

    public String getLocalAddress() {
        return localAddress;
    }

    public int getLocalPort() {
        return localPort;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public long getLongTimer() {
        return longTimer;
    }

    public void setLongTimer(long longTimer) {
        this.longTimer = longTimer;
    }

    public long getAverageAcknowledgementDelay() {
        long aad = acknowledgementDelay.get() / acknowledgementCount.get();
        return aad;
    }

    public MegacoFactory getMegacoFactory() {
        return this.megacoFactory;
    }

    protected MegacoTransaction removeTransaction(long transactionId) {
        mutex.lock();
        try {
            MegacoMgcTransaction transaction = (MegacoMgcTransaction) transactions.remove(transactionId);
            if (transaction != null) {
                transaction.task.cancel(false);
                transaction.active = false;
            }
            return transaction;
        } finally {
            mutex.unlock();
        }
    }

    protected MegacoTransaction getTransaction(long transactionId) {
        mutex.lock();
        try {
            return transactions.get(transactionId);
        } finally {
            mutex.unlock();
        }
    }

    public long transactionsSize() {
        mutex.lock();
        try {
            return transactions.size();
        } finally {
            mutex.unlock();
        }
    }

    public MegacoStack getMegacoStack() {
        return megacoStack;
    }

    @Override
    public int getProtocolId() {
        return MegacoStack.SCTP_MEGACO_PAYLOAD_ID;
    }

    public List<Termination> listTerminations() {
        List<Termination> terminations = new ArrayList<>();
        contexts.values().forEach((context) -> {
            terminations.addAll(context.getTerminations().values());
        });

        return terminations;
    }

    public int getContextGuardTimerValue() {
        return contextGuardTimerValue;
    }

    public void setContextGuardTimerValue(int contextGuardTimerValue) {
        this.contextGuardTimerValue = contextGuardTimerValue;
    }

}
