/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.parameter.AddressType;
import com.linkedlogics.megaco.parameter.DomainName;
import com.linkedlogics.megaco.parameter.IP4Address;
import com.linkedlogics.megaco.parameter.IP6Address;
import com.linkedlogics.megaco.parameter.Mid;
import com.linkedlogics.megaco.parameter.PathName;
import com.linkedlogics.sctp.SctpProvider;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.mobicents.protocols.asn.AsnException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author eatakishiyev
 */
public class MegacoStack implements Serializable {

    private transient final Logger logger = LoggerFactory.getLogger(MegacoStack.class);
//    
    private final Random rnd = new Random();
    private static MegacoStack instance;
    protected final Map<String, MegacoProvider> providersByName = new HashMap<>();
//    

    protected transient final SctpProvider sctpProvider;

    private int concurrentSessions = 10000;

    public final static int VERSION = 3;

    private int maxContextIdCount = 10000;

    protected Mid mid;
    protected final static int SCTP_MEGACO_PAYLOAD_ID = 7;
//    
    private final String CONFIGURATION_FILE = "./conf/megaco_config.xml";
    protected OperationMode operationMode = OperationMode.MG;

    private final Router router;
    private String name;

    protected Class terminationConcreteClass;// It should be configurable
    protected Class streamConcreteClass;// It should be configurable

    protected final HashMap<Integer, com.linkedlogics.megaco.packages.Package> configuredPackages = new HashMap<>();
    protected int incomingMessageHandlersCount;

    protected transient ExecutorService[] incomingMessageHandlers;
    protected transient ExecutorService messageSenderExecutorService;

    private MegacoStack(OperationMode operationMode, Mid mid) throws Exception {
        this.sctpProvider = SctpProvider.getInstance();
        this.router = new Router(this);
        this.operationMode = operationMode;
        this.mid = mid;
        this.loadConfiguration(CONFIGURATION_FILE);
    }

    public static MegacoStack createInstance(OperationMode operationMode, Mid mid) throws Exception {
        if (instance == null) {
            synchronized (MegacoStack.class) {
                if (instance == null) {
                    instance = new MegacoStack(operationMode, mid);

                }
            }
        }
        return instance;
    }

    public static MegacoStack instance() {
        return instance;
    }

    public int getMaxContextIdCount() {
        return maxContextIdCount;
    }

    public void setMaxContextIdCount(int maxContextIdCount) {
        this.maxContextIdCount = maxContextIdCount;
    }

    public int getConcurrentSessions() {
        return concurrentSessions;
    }

    public void setConcurrentSessions(int concurrentSessions) {
        this.concurrentSessions = concurrentSessions;
    }

    private void loadConfiguration(String mgcConfiguration) throws Exception {
        InputStream resource = new FileInputStream(mgcConfiguration);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(resource);
//        
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
//        XPathExpression expression = xPath.compile("/config/operationMode");
//        this.operationMode = OperationMode.valueOf(expression.evaluate(document));

        Node property = (Node) xPath.compile("/config/incomingMessageHandlersCount").evaluate(document, XPathConstants.NODE);
        this.incomingMessageHandlersCount = Integer.parseInt(property.getAttributes().getNamedItem("value").getNodeValue());

        property = (Node) xPath.compile("/config/sctpExecutors").evaluate(document, XPathConstants.NODE);
        this.sctpProvider.setExecutorsCount(Integer.parseInt(property.getAttributes().getNamedItem("value").getNodeValue()));
        sctpProvider.start();

        this.incomingMessageHandlers = new ExecutorService[incomingMessageHandlersCount];

        for (int i = 0; i < incomingMessageHandlersCount; i++) {
            this.incomingMessageHandlers[i] = Executors.newSingleThreadExecutor(new ThreadFactory() {
                @Override
                public Thread newThread(Runnable r) {
                    return new Thread(r, "MegacoIncomingMessageHandler");
                }
            });
        }

        int messageSenderThreadCount = Integer.parseInt(property.getAttributes().getNamedItem("value").getNodeValue());
        this.messageSenderExecutorService = Executors.newFixedThreadPool(messageSenderThreadCount, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "MessageSenderThread");
            }
        });

        if (operationMode == OperationMode.MG) {
            Node mgNode = (Node) xPath.compile("/config/mg").evaluate(document, XPathConstants.NODE);

            this.name = mgNode.getAttributes().getNamedItem("name").getNodeValue();

            String strTerminationConcreteClass = ((Node) xPath.compile("termination").evaluate(mgNode, XPathConstants.NODE)).getAttributes().getNamedItem("class").getNodeValue();
            String strStreamConcreteClass = ((Node) xPath.compile("stream").evaluate(mgNode, XPathConstants.NODE)).getAttributes().getNamedItem("class").getNodeValue();

            this.terminationConcreteClass = Class.forName(strTerminationConcreteClass);
            this.streamConcreteClass = Class.forName(strStreamConcreteClass);
        }

        NodeList packagesList = (NodeList) xPath.compile("/config/packages/package").evaluate(document, XPathConstants.NODESET);
        for (int j = 0; j < packagesList.getLength(); j++) {
            Node pkg = packagesList.item(j);
            int pkgId = Integer.valueOf(pkg.getAttributes().getNamedItem("id").getNodeValue());
            String pkgName = pkg.getAttributes().getNamedItem("name").getNodeValue();
            String pkgClass = pkg.getAttributes().getNamedItem("class").getNodeValue();

            com.linkedlogics.megaco.packages.Package _package
                    = (com.linkedlogics.megaco.packages.Package) Class.forName(pkgClass).newInstance();

            configuredPackages.put(pkgId, _package);
        }
    }

    private Mid createMid(Node midNode) throws IOException {
        String peerMidType = midNode.getAttributes().getNamedItem("type").getNodeValue();
        AddressType addressType = AddressType.valueOf(peerMidType);

        Mid peerMid = null;

        switch (addressType) {
            case IP4ADDRESS:
                String peerMidAddress = midNode.getAttributes().getNamedItem("address").getNodeValue();
                int peerMidPort = Integer.parseInt(midNode.getAttributes().getNamedItem("port").getNodeValue());
                peerMid = new Mid(new IP4Address(InetAddress.getByName(peerMidAddress), peerMidPort));
                break;
            case IP6ADDRESS:
                peerMidAddress = midNode.getAttributes().getNamedItem("address").getNodeValue();
                peerMidPort = Integer.parseInt(midNode.getAttributes().getNamedItem("port").getNodeValue());
                peerMid = new Mid(new IP6Address(InetAddress.getByName(peerMidAddress), peerMidPort));
                break;
            case DOMAIN_NAME:
                peerMidAddress = midNode.getAttributes().getNamedItem("address").getNodeValue();
                peerMidPort = Integer.parseInt(midNode.getAttributes().getNamedItem("port").getNodeValue());
                peerMid = new Mid(new DomainName(peerMidAddress, peerMidPort));
                break;
            case PATH_NAME:
                String deviceName = midNode.getAttributes().getNamedItem("deviceName").getNodeValue();
                peerMid = new Mid(new PathName(deviceName));
                break;
            case MTP_ADDRESS:
                break;
        }
        return peerMid;
    }

    public void setOperationMode(OperationMode operationMode) {
        this.operationMode = operationMode;
    }

    public OperationMode getOperationMode() {
        return operationMode;
    }

    public MegacoProvider createProvider(String name, String localAddress, int localPort,
            String remoteAddress, int remotePort, int inStream, int outStream,
            boolean isServer, long terminationTimeOut) throws Exception {

        MegacoProvider megacoProvider = new MegacoProvider(this, name, concurrentSessions,
                localAddress, localPort, remoteAddress, remotePort, inStream, outStream,
                isServer, terminationTimeOut);

        providersByName.put(name, megacoProvider);
        router.addPeer(name);
        return megacoProvider;
    }

    public MegacoProvider getProvider(String name) {
        return providersByName.get(name);
    }

    public void sendRequest(MegacoMgcTransaction transaction, long timeOut) throws AsnException, IOException {
        this.router.send(transaction, timeOut);
    }

    protected long generateTransactionId() {
        long transactionId = 0;

        int _byte = rnd.nextInt(255);
        transactionId = transactionId | _byte;
//            System.out.print(_byte + " ");

        transactionId = transactionId << 8;
        _byte = rnd.nextInt(255);
        transactionId = transactionId | _byte;
//            System.out.print(_byte + " ");

        transactionId = transactionId << 8;
        _byte = rnd.nextInt(255);
        transactionId = transactionId | _byte;
//            System.out.print(_byte + " ");

        transactionId = transactionId << 8;
        _byte = rnd.nextInt(255);
        transactionId = transactionId | _byte;

        return transactionId;
    }

    protected byte[] generateTerminationId() {
        byte[] terminationId = new byte[8];

        terminationId[0] = (byte) rnd.nextInt(255);
        terminationId[1] = (byte) rnd.nextInt(255);
        terminationId[2] = (byte) rnd.nextInt(255);
        terminationId[3] = (byte) rnd.nextInt(255);
        terminationId[4] = (byte) rnd.nextInt(255);
        terminationId[5] = (byte) rnd.nextInt(255);
        terminationId[6] = (byte) rnd.nextInt(255);
        terminationId[7] = (byte) rnd.nextInt(255);

        return terminationId;
    }

    public com.linkedlogics.megaco.packages.Package getPackage(int packageID) {
        return configuredPackages.get(packageID);
    }
}
