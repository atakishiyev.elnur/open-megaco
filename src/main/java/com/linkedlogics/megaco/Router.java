/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.parameter.ServiceState;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.mobicents.protocols.asn.AsnException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author eatakishiyev
 */
public class Router implements Serializable {

    private transient final Logger logger = LoggerFactory.getLogger(Router.class);

    protected Mode mode = Mode.LOADBALANCING;
    private volatile AtomicInteger idx = new AtomicInteger(0);

    private final MegacoStack stack;
    private final List<String> peers = new ArrayList<>();

    public Router(MegacoStack stack) {
        this.stack = stack;
    }

    public void addPeer(String peer) {
        peers.add(peer);
    }

    public void removePeer(String peer) {
        peers.remove(peer);
    }

    public void send(MegacoMgcTransaction transaction, long timeOut) throws AsnException, IOException {
        MegacoProvider provider = selectRoute(transaction);
        if (provider != null) {
            provider.sendTransaction(transaction, timeOut);
        } else {
            throw new IOException("No route to host : " + transaction);
        }
    }

    private MegacoProvider selectRoute(MegacoMgcTransaction transaction) throws AsnException, IOException {
        MegacoProvider provider = null;
        switch (mode) {
            case LOADBALANCING:
                while (true) {
                    int _idx = idx.getAndIncrement();
                    if (_idx >= Integer.MAX_VALUE) {
                        idx.set(0);
                        _idx = 0;
                    }
                    String peer = peers.get(_idx % peers.size());
                    provider = stack.providersByName.get(peer);
                    if (provider != null /*&& provider.controlAssociation.getCurrentState().getName().equals(ServiceState.IN_SVC.name())*/) {
                        transaction.provider = provider;
                        break;
                    }
                }
                break;
            case ACTIVE_STANDBY:
                while (true) {
                    String peer = peers.get(idx.get());
                    provider = stack.providersByName.get(peer);
                    if (provider != null
                            && provider.controlAssociation.getCurrentState().getName().equals(ServiceState.IN_SVC.name())) {
                        transaction.provider = provider;
                        break;
                    }
                    int _idx = idx.incrementAndGet();
                    if (_idx >= peers.size()) {
                        idx.set(0);
                        logger.error("No appropriate route found: " + transaction.getTransactionId());
                        break;
                    }
                }
                break;
        }
        return provider;
    }

    public enum Mode {
        LOADBALANCING,
        ACTIVE_STANDBY
    }
}
