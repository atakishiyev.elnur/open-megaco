/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.descriptor.AmmDescriptor;
import com.linkedlogics.megaco.parameter.StreamID;
import com.linkedlogics.megaco.parameter.StreamParms;

/**
 *
 * @author eatakishiyev
 */
public abstract class Stream {

    private StreamID streamID;
    private StreamParms streamParms;
    protected Termination termination;

    public StreamID getStreamID() {
        return streamID;
    }

    public void setStreamID(StreamID streamID) {
        this.streamID = streamID;
    }

    public StreamParms getStreamParms() {
        return streamParms;
    }

    public void setStreamParms(StreamParms streamParms) {
        this.streamParms = streamParms;
    }

    public Termination getTermination() {
        return this.termination;
    }

    public abstract AmmDescriptor[] onCreated();

    public abstract AmmDescriptor[] onModified();

    public abstract void onDestroyed(boolean timeOut);
}
