/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.parameter.ContextID;
import com.linkedlogics.megaco.descriptor.EventsDescriptor;
import com.linkedlogics.megaco.descriptor.SignalsDescriptor;
import com.linkedlogics.megaco.descriptor.TerminationStateDescriptor;
import com.linkedlogics.megaco.parameter.TerminationID;
import java.io.Serializable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author eatakishiyev
 */
public abstract class Termination implements Runnable, Serializable {

    protected MegacoProvider provider;
    private ContextID contextID;
    private TerminationID terminationID;
    private Stream stream;
    private TerminationStateDescriptor terminationStateDescriptor;

    /**
     * Terminations may have signals applied to them (see clause 7.1.11). Terminations may be
     * programmed to detect events, the occurrence of which can trigger notification messages to the
     * MGC, or action by the MG.
     */
    private EventsDescriptor eventsDescriptor;
    private SignalsDescriptor signalsDescriptor;
    private transient ScheduledFuture task;

    public Termination() {

    }

    public Termination(MegacoProvider provider, ContextID contextID) {
        this.provider = provider;
        this.contextID = contextID;
    }

    public TerminationID getTerminationID() {
        return this.terminationID;
    }

    public void setTerminationID(TerminationID terminationID) {
        this.terminationID = terminationID;
    }

    public ContextID getContextID() {
        return this.contextID;
    }

    public void setContextID(ContextID contextID) {
        this.contextID = contextID;
    }

    public TerminationStateDescriptor getTerminationStateDescriptor() {
        return terminationStateDescriptor;
    }

    public Stream getStream() {
        return stream;
    }

    public void setStream(Stream stream) {
        this.stream = stream;
    }

    public void setSignalsDescriptor(SignalsDescriptor signalsDescriptor) {
        this.signalsDescriptor = signalsDescriptor;
    }

    public SignalsDescriptor getSignalsDescriptor() {
        return signalsDescriptor;
    }

    public void setTerminationStateDescriptor(TerminationStateDescriptor terminationStateDescriptor) {
        this.terminationStateDescriptor = terminationStateDescriptor;
    }

    public EventsDescriptor getEventsDescriptor() {
        return eventsDescriptor;
    }

    public void setEventsDescriptor(EventsDescriptor eventsDescriptor) {
        this.eventsDescriptor = eventsDescriptor;
    }

    public MegacoProvider getProvider() {
        return provider;
    }

    public abstract void onCreated();

    public abstract void onModified();

    public abstract void onDestroyed(boolean timeOut);

    void scheduleGuard() {
        this.task = provider.terminationGuard.schedule(this, provider.terminationLifeTime, TimeUnit.SECONDS);
    }

    void cancelGuard() {
        if (task != null) {
            task.cancel(true);
            task = null;
        }
    }

    @Override
    public void run() {
        provider.removeTimeOutTermination(terminationID, contextID);
        task = null;
    }

}
