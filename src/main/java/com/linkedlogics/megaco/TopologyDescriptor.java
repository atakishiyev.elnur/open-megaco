/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.parameter.TopologyDirection;

/**
 *
 * @author eatakishiyev
 */
public class TopologyDescriptor {

    private Termination termination1;
    private Termination termination2;
    private TopologyDirection topologyDirection;

    public TopologyDescriptor() {
    }

    public TopologyDescriptor(Termination termination1, Termination termination2, TopologyDirection topologyDirection) {
        this.termination1 = termination1;
        this.termination2 = termination2;
        this.topologyDirection = topologyDirection;
    }

    public Termination getTermination1() {
        return termination1;
    }

    public void setTermination1(Termination termination1) {
        this.termination1 = termination1;
    }

    public Termination getTermination2() {
        return termination2;
    }

    public void setTermination2(Termination termination2) {
        this.termination2 = termination2;
    }

    public TopologyDirection getTopologyDirection() {
        return topologyDirection;
    }

    public void setTopologyDirection(TopologyDirection topologyDirection) {
        this.topologyDirection = topologyDirection;
    }

}
