/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.action;

import com.linkedlogics.megaco.command.reply.AddReply;
import com.linkedlogics.megaco.command.reply.AuditCapReply;
import com.linkedlogics.megaco.command.reply.AuditValueReply;
import com.linkedlogics.megaco.command.reply.CommandReply;
import com.linkedlogics.megaco.command.reply.ModReply;
import com.linkedlogics.megaco.command.reply.MoveReply;
import com.linkedlogics.megaco.command.reply.NotifyReply;
import com.linkedlogics.megaco.command.reply.ServiceChangeReply;
import com.linkedlogics.megaco.command.reply.SubtractReply;
import com.linkedlogics.megaco.parameter.ContextID;
import com.linkedlogics.megaco.parameter.ContextRequest;
import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import com.linkedlogics.megaco.parameter.Parameter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ActionReply ::=SEQUENCE
 * {
 * contextID [0] ContextID,
 * errorDescriptor [1] ErrorDescriptor OPTIONAL,
 * contextReply [2] ContextRequest OPTIONAL,
 * commandReply [3] SEQUENCE OF CommandReply
 * }
 *
 * /**
 * CommandReply ::= CHOICE
 * {
 * addReply [0] AmmsReply,
 * moveReply [1] AmmsReply,
 * modReply [2] AmmsReply,
 * subtractReply [3] AmmsReply,
 * -- Add, Move, Modify, Subtract replies have the same parameters
 * auditCapReply [4] AuditReply,
 * auditValueReply [5] AuditReply,
 * notifyReply [6] NotifyReply,
 * serviceChangeReply [7] ServiceChangeReply,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class ActionReply implements Parameter {

    private ContextID contextID;
    private ErrorDescriptor errorDescriptor;//optional 
    private ContextRequest contextReply;//optional
    private final List<CommandReply> commandReplies = new ArrayList<>();

    public ActionReply() {
    }

    public ActionReply(ContextID contextID) {
        this.contextID = contextID;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        contextID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (errorDescriptor != null) {
            errorDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
        if (contextReply != null) {
            contextReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 3);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (CommandReply commandReply : commandReplies) {
            switch (commandReply.getType()) {
                case ADD_REPLY:
                    commandReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
                    break;
                case MOVE_REPLY:
                    commandReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
                    break;
                case MOD_REPLY:
                    commandReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
                    break;
                case SUBTRACT_REPLY:
                    commandReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
                    break;
                case AUDIT_CAP_REPLY:
                    commandReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
                    break;
                case AUDIT_VALUE_REPLY:
                    commandReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
                    break;
                case NOTIFY_REPLY:
                    commandReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
                    break;
                case SERVICE_CHANGE_REPLY:
                    commandReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
                    break;
            }
        }
        aos.FinalizeContent(lenPos1);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.contextID = new ContextID();
                    contextID.decode(ais);
                    break;
                case 1:
                    this.errorDescriptor = new ErrorDescriptor();
                    errorDescriptor.decode(ais);
                    break;
                case 2:
                    this.contextReply = new ContextRequest();
                    contextReply.decode(ais);
                    break;
                case 3:
                    this.decodeCommandReplies(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeCommandReplies(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    AddReply addReply = new AddReply();
                    addReply.decode(ais.readSequenceStream());
                    commandReplies.add(addReply);
                    break;
                case 1:
                    MoveReply moveReply = new MoveReply();
                    moveReply.decode(ais.readSequenceStream());
                    commandReplies.add(moveReply);
                    break;
                case 2:
                    ModReply modReply = new ModReply();
                    modReply.decode(ais.readSequenceStream());
                    commandReplies.add(modReply);
                    break;
                case 3:
                    SubtractReply subtractReply = new SubtractReply();
                    subtractReply.decode(ais.readSequenceStream());
                    commandReplies.add(subtractReply);
                    break;
                case 4:
                    AuditCapReply auditCapReply = new AuditCapReply();
                    auditCapReply.decode(ais.readSequenceStream());
                    commandReplies.add(auditCapReply);
                    break;
                case 5:
                    AuditValueReply auditValueReply = new AuditValueReply();
                    auditValueReply.decode(ais.readSequenceStream());
                    commandReplies.add(auditValueReply);
                    break;
                case 6:
                    NotifyReply notifyReply = new NotifyReply();
                    notifyReply.decode(ais.readSequenceStream());
                    commandReplies.add(notifyReply);
                    break;
                case 7:
                    ServiceChangeReply serviceChangeReply = new ServiceChangeReply();
                    serviceChangeReply.decode(ais.readSequenceStream());
                    commandReplies.add(serviceChangeReply);
                    break;
            }
        }
    }

    public List<CommandReply> getCommandReplys() {
        return commandReplies;
    }

    public void setContextId(ContextID contextId) {
        this.contextID = contextId;
    }

    public ContextID getContextId() {
        return contextID;
    }

    public void setContextReply(ContextRequest contextReply) {
        this.contextReply = contextReply;
    }

    public ContextRequest getContextReply() {
        return contextReply;
    }

    public void setErrorDescriptor(ErrorDescriptor errorDescriptor) {
        this.errorDescriptor = errorDescriptor;
    }

    public ErrorDescriptor getErrorDescriptor() {
        return errorDescriptor;
    }

    public void addCommandReply(CommandReply commandReply) {
        commandReplies.add(commandReply);
    }
}
