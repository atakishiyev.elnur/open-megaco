/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.action;

import com.linkedlogics.megaco.command.request.Command;
import com.linkedlogics.megaco.parameter.CommandRequest;
import com.linkedlogics.megaco.parameter.ContextAttrAuditRequest;
import com.linkedlogics.megaco.parameter.ContextID;
import com.linkedlogics.megaco.parameter.ContextRequest;
import com.linkedlogics.megaco.parameter.Parameter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ActionRequest ::= SEQUENCE
 * {
 * contextID [0] ContextID,
 * contextRequest [1] ContextRequest OPTIONAL,
 * contextAttrAuditReq [2] ContextAttrAuditRequest OPTIONAL,
 * commandRequests [3] SEQUENCE OF CommandRequest
 * }
 *
 * @author eatakishiyev
 */
public class ActionRequest implements Parameter {

    private ContextID contextID;
    private ContextRequest contextRequest;
    private ContextAttrAuditRequest contextAttrAuditRequest;
    private final List<CommandRequest> commandRequests = new ArrayList<>();

    public ActionRequest() {
    }

    public ActionRequest(ContextID contextID) {
        this.contextID = contextID;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        contextID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (contextRequest != null) {
            contextRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
        if (contextAttrAuditRequest != null) {
            contextAttrAuditRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 3);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (CommandRequest commandRequest : commandRequests) {
            commandRequest.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.contextID = new ContextID();
                    contextID.decode(ais);
                    break;
                case 1:
                    this.contextRequest = new ContextRequest();
                    contextRequest.decode(ais);
                    break;
                case 2:
                    this.contextAttrAuditRequest = new ContextAttrAuditRequest();
                    contextAttrAuditRequest.decode(ais);
                    break;
                case 3:
                    this.decodeCommandRequests(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeCommandRequests(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            if (tag == Tag.SEQUENCE) {
                CommandRequest commandRequest = new CommandRequest();
                commandRequest.decode(ais.readSequenceStream());
                commandRequests.add(commandRequest);
            } else {
                throw new AsnException("Unexpected tag received. Expecting SEQUENCE, received " + tag);
            }
        }
    }

    public List<CommandRequest> getCommandRequests() {
        return commandRequests;
    }

    public ContextAttrAuditRequest getContextAttrAuditRequest() {
        return contextAttrAuditRequest;
    }

    public void setContextAttrAuditRequest(ContextAttrAuditRequest contextAttrAuditRequest) {
        this.contextAttrAuditRequest = contextAttrAuditRequest;
    }

    public ContextID getContextId() {
        return contextID;
    }

    public void setContextId(ContextID contextId) {
        this.contextID = contextId;
    }

    public ContextRequest getContextRequest() {
        return contextRequest;
    }

    public void setContextRequest(ContextRequest contextRequest) {
        this.contextRequest = contextRequest;
    }

    public void addCommandRequest(CommandRequest commandRequest) {
        commandRequests.add(commandRequest);
    }

    public void addCommand(Command command, boolean optional, boolean wildcardReturn) {
        CommandRequest commandRequest = new CommandRequest(command);
        commandRequest.setOptional(optional);
        commandRequest.setWildcardReturn(wildcardReturn);
        commandRequests.add(commandRequest);
    }
}
