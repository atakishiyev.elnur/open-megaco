/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.reply;

import com.linkedlogics.megaco.parameter.TerminationAudit;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * u
 * AmmsReply ::= SEQUENCE
 * {
 * terminationID TerminationIDList,
 * terminationAudit TerminationAudit OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public abstract class AmmsReply extends CommandReply {

    private TerminationIDList terminationIDList;
    private TerminationAudit terminationAudit;

    public AmmsReply() {
    }

    public AmmsReply(TerminationIDList terminationIDList) {
        this.terminationIDList = terminationIDList;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        terminationIDList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (terminationAudit != null) {
            terminationAudit.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationIDList = new TerminationIDList();
                    terminationIDList.decode(ais);
                    break;
                case 1:
                    this.terminationAudit = new TerminationAudit();
                    terminationAudit.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

    public TerminationAudit getTerminationAudit() {
        return terminationAudit;
    }

    public void setTerminationAudit(TerminationAudit terminationAudit) {
        this.terminationAudit = terminationAudit;
    }

    public TerminationIDList getTerminationId() {
        return terminationIDList;
    }

    public void setTerminationIDList(TerminationIDList terminationIDList) {
        this.terminationIDList = terminationIDList;
    }


    
}
