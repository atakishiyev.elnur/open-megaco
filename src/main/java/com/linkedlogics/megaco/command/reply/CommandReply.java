/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.reply;

import com.linkedlogics.megaco.parameter.Parameter;

/**
 * CommandReply ::= CHOICE
 * {
 * addReply [0] AmmsReply,
 * moveReply [1] AmmsReply,
 * modReply [2] AmmsReply,
 * subtractReply [3] AmmsReply,
 * -- Add, Move, Modify, Subtract replies have the same parameters
 * auditCapReply [4] AuditReply,
 * auditValueReply [5] AuditReply,
 * notifyReply [6] NotifyReply,
 * serviceChangeReply [7] ServiceChangeReply,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public abstract class CommandReply implements Parameter {

//    private AmmsReply addReply;
//    private AmmsReply moveReply;
//    private AmmsReply modReply;
//    private AmmsReply subtractReply;
//    private AuditReply auditCapReply;
//    private AuditReply auditValueReply;
//    private NotifyReply notifyReply;
//    private ServiceChangeReply serviceChangeReply;
//
    public abstract ReplyType getType();
//
//    private CommandReply() {
//
//    }
//
//    @Override
//    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
//        aos.writeTag(tagClass, false, tag);
//        int lenPos = aos.StartContentDefiniteLength();
//
//        if (addReply != null) {
//            addReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
//        } else if (moveReply != null) {
//            moveReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
//        } else if (modReply != null) {
//            modReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
//        } else if (subtractReply != null) {
//            subtractReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
//        } else if (auditCapReply != null) {
//            auditCapReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
//        } else if (auditValueReply != null) {
//            auditValueReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
//        } else if (notifyReply != null) {
//            notifyReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
//        } else if (serviceChangeReply != null) {
//            serviceChangeReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
//        }
//
//        aos.FinalizeContent(lenPos);
//    }
//
//    public void encode(AsnOutputStream aos) throws AsnException, IOException {
//        if (addReply != null) {
//            addReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
//        } else if (moveReply != null) {
//            moveReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
//        } else if (modReply != null) {
//            modReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
//        } else if (subtractReply != null) {
//            subtractReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
//        } else if (auditCapReply != null) {
//            auditCapReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
//        } else if (auditValueReply != null) {
//            auditValueReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
//        } else if (notifyReply != null) {
//            notifyReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
//        } else if (serviceChangeReply != null) {
//            serviceChangeReply.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
//        }
//    }
//
//    @Override
//    public void decode(AsnInputStream ais) throws AsnException, IOException {
//        while (ais.available() > 0) {
//            int tag = ais.readTag();
//            switch (tag) {
//                case 0:
//                    this.addReply = new AmmsReply();
//                    addReply.decode(ais.readSequenceStream());
//                    this.replyType = ReplyType.ADD_REPLY;
//                    break;
//                case 1:
//                    this.moveReply = new AmmsReply();
//                    moveReply.decode(ais.readSequenceStream());
//                    this.replyType = ReplyType.MOVE_REPLY;
//                    break;
//                case 2:
//                    this.modReply = new AmmsReply();
//                    modReply.decode(ais.readSequenceStream());
//                    this.replyType = ReplyType.MOD_REPLY;
//                    break;
//                case 3:
//                    this.subtractReply = new AmmsReply();
//                    subtractReply.decode(ais.readSequenceStream());
//                    this.replyType = ReplyType.SUBTRACT_REPLY;
//                    break;
//                case 4:
//                    this.auditCapReply = new AuditReply();
//                    auditCapReply.decode(ais.readSequenceStream());
//                    this.replyType = ReplyType.AUDIT_CAP_REPLY;
//                    break;
//                case 5:
//                    this.auditValueReply = new AuditReply();
//                    auditValueReply.decode(ais.readSequenceStream());
//                    this.replyType = ReplyType.AUDIT_VALUE_REPLY;
//                    break;
//                case 6:
//                    this.notifyReply = new NotifyReply();
//                    notifyReply.decode(ais.readSequenceStream());
//                    this.replyType = ReplyType.AUDIT_VALUE_REPLY;
//                    break;
//                case 7:
//                    this.serviceChangeReply = new ServiceChangeReply();
//                    serviceChangeReply.decode(ais.readSequenceStream());
//                    this.replyType = ReplyType.SERVICE_CHANGE_REPLY;
//                    break;
//            }
//        }
//    }
//
//    public static CommandReply createAddReply() {
//        CommandReply instance = new CommandReply();
//        instance.addReply = new AmmsReply();
//        instance.replyType = ReplyType.ADD_REPLY;
//        return instance;
//    }
//
//    public static CommandReply createMoveReply() {
//        CommandReply instance = new CommandReply();
//        instance.moveReply = new AmmsReply();
//        instance.replyType = ReplyType.MOVE_REPLY;
//        return instance;
//    }
//
//    public static CommandReply createModReply() {
//        CommandReply instance = new CommandReply();
//        instance.modReply = new AmmsReply();
//        instance.replyType = ReplyType.MOD_REPLY;
//        return instance;
//    }
//
//    public static CommandReply createSubtractReply() {
//        CommandReply instance = new CommandReply();
//        instance.subtractReply = new AmmsReply();
//        instance.replyType = ReplyType.SUBTRACT_REPLY;
//        return instance;
//    }
//
//    public static CommandReply createAuditCapReply() {
//        CommandReply instance = new CommandReply();
//        instance.auditCapReply = new AuditReply();
//        instance.replyType = ReplyType.AUDIT_CAP_REPLY;
//        return instance;
//    }
//
//    public static CommandReply createAuditValueReply() {
//        CommandReply instance = new CommandReply();
//        instance.auditValueReply = new AuditReply();
//        instance.replyType = ReplyType.AUDIT_VALUE_REPLY;
//        return instance;
//    }
//
//    public static CommandReply createNotifyReply() {
//        CommandReply instance = new CommandReply();
//        instance.notifyReply = new NotifyReply();
//        instance.replyType = ReplyType.NOTIFY_REPLY;
//        return instance;
//    }
//
//    public static CommandReply createServiceChangeReply() {
//        CommandReply instance = new CommandReply();
//        instance.serviceChangeReply = new ServiceChangeReply();
//        instance.replyType = ReplyType.SERVICE_CHANGE_REPLY;
//        return instance;
//    }
//
//    public static CommandReply createCommandReply() {
//        return new CommandReply();
//    }
//
//    public ReplyType getReplyType() {
//        return replyType;
//    }
//
//    public AmmsReply getAddReply() {
//        return addReply;
//    }
//
//    public AuditReply getAuditCapReply() {
//        return auditCapReply;
//    }
//
//    public AuditReply getAuditValueReply() {
//        return auditValueReply;
//    }
//
//    public AmmsReply getModReply() {
//        return modReply;
//    }
//
//    public AmmsReply getMoveReply() {
//        return moveReply;
//    }
//
//    public NotifyReply getNotifyReply() {
//        return notifyReply;
//    }
//
//    public ServiceChangeReply getServiceChangeReply() {
//        return serviceChangeReply;
//    }
//
//    public AmmsReply getSubtractReply() {
//        return subtractReply;
//    }
}
