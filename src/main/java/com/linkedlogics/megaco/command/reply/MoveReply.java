/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.reply;

/**
 *
 * @author eatakishiyev
 */
public class MoveReply extends AmmsReply{

    @Override
    public ReplyType getType() {
        return ReplyType.MOVE_REPLY;
    }
    
}
