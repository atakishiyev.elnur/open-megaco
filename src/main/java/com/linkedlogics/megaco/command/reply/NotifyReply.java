/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.reply;

import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * NotifyReply ::= SEQUENCE{
 * {
 * terminationID [0] TerminationIDList
 * errorDescriptor [1] ErrorDescriptor OPTIONAL
 * }
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class NotifyReply extends CommandReply {

    private TerminationIDList terminationId;
    private ErrorDescriptor errorDescriptor;

    public NotifyReply() {
    }

    public NotifyReply(TerminationIDList terminationId) {
        this.terminationId = terminationId;
    }

    public ErrorDescriptor getErrorDescriptor() {
        return errorDescriptor;
    }

    public void setErrorDescriptor(ErrorDescriptor errorDescriptor) {
        this.errorDescriptor = errorDescriptor;
    }

    public TerminationIDList getTerminationId() {
        return terminationId;
    }

    public void setTerminationId(TerminationIDList terminationId) {
        this.terminationId = terminationId;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        terminationId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (errorDescriptor != null) {
            errorDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationId = new TerminationIDList();
                    terminationId.decode(ais);
                    break;
                case 1:
                    this.errorDescriptor = new ErrorDescriptor();
                    errorDescriptor.decode(ais);
                    break;
            }
        }
    }

    @Override
    public ReplyType getType() {
        return ReplyType.NOTIFY_REPLY;
    }

}
