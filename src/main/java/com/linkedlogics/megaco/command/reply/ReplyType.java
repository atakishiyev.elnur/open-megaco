/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.reply;

/**
 *
 * @author eatakishiyev
 */
public enum ReplyType {
    ADD_REPLY,
    MOVE_REPLY,
    MOD_REPLY,
    SUBTRACT_REPLY,
    AUDIT_CAP_REPLY,
    AUDIT_VALUE_REPLY,
    NOTIFY_REPLY,
    SERVICE_CHANGE_REPLY;
}
