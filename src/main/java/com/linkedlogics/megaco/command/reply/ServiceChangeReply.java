/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.reply;

import com.linkedlogics.megaco.parameter.ServiceChangeResult;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ServiceChangeReply ::= SEQUENCE
 * {
 * terminationID [0] TerminationIDList,
 * serviceChangeResult [1] ServiceChangeResult,
 *
 * ServiceChangeResult ::= CHOICE
 * {
 * errorDescriptor [0] ErrorDescriptor,
 * serviceChangeResParms [1] ServiceChangeResParm
 * }
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class ServiceChangeReply extends CommandReply {

    private TerminationIDList terminationId;
    private ServiceChangeResult serviceChangeResult;

    public ServiceChangeReply() {
    }

    public ServiceChangeReply(TerminationIDList terminationId, ServiceChangeResult serviceChangeResult) {
        this.terminationId = terminationId;
        this.serviceChangeResult = serviceChangeResult;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        terminationId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        serviceChangeResult.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationId = new TerminationIDList();
                    terminationId.decode(ais);
                    break;
                case 1:
                    this.serviceChangeResult = new ServiceChangeResult();
                    serviceChangeResult.decode(ais);
                    break;
            }
        }
    }

    public ServiceChangeResult getServiceChangeResult() {
        return serviceChangeResult;
    }

    public void setServiceChangeResult(ServiceChangeResult serviceChangeResult) {
        this.serviceChangeResult = serviceChangeResult;
    }

    public TerminationIDList getTerminationId() {
        return terminationId;
    }

    public void setTerminationId(TerminationIDList terminationId) {
        this.terminationId = terminationId;
    }

    @Override
    public ReplyType getType() {
        return ReplyType.SERVICE_CHANGE_REPLY;
    }

}
