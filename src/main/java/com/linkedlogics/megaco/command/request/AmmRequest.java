/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

import com.linkedlogics.megaco.descriptor.AmmDescriptor;
import com.linkedlogics.megaco.descriptor.AuditDescriptor;
import com.linkedlogics.megaco.descriptor.DigitMapDescriptor;
import com.linkedlogics.megaco.descriptor.EventBufferDescriptor;
import com.linkedlogics.megaco.descriptor.EventsDescriptor;
import com.linkedlogics.megaco.descriptor.MediaDescriptor;
import com.linkedlogics.megaco.descriptor.ModemDescriptor;
import com.linkedlogics.megaco.descriptor.MuxDescriptor;
import com.linkedlogics.megaco.descriptor.SignalsDescriptor;
import com.linkedlogics.megaco.descriptor.StatisticsDescriptor;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * AmmRequest ::= SEQUENCE
 * {
 * terminationID [0] TerminationIDList,
 * descriptors [1] SEQUENCE OF AmmDescriptor,
 * -- At most one descriptor of each type (see AmmDescriptor)
 * -- allowed in the sequence.
 *
 * }
 *
 * @author eatakishiyev
 */
public abstract class AmmRequest extends Command {

    private TerminationIDList terminationIDList;
    private final List<AmmDescriptor> descriptors = new ArrayList<>();

    public AmmRequest() {

    }

    public AmmRequest(TerminationIDList terminationIDList) {
        this.terminationIDList = terminationIDList;
    }

    public AmmRequest(TerminationIDList terminationIDList, AmmDescriptor... ammDescriptors) {
        this.terminationIDList = terminationIDList;
        if (ammDescriptors != null) {
            for (AmmDescriptor ammDescriptor : ammDescriptors) {
                descriptors.add(ammDescriptor);
            }
        }
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        terminationIDList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (AmmDescriptor ammDescriptor : descriptors) {
            if (ammDescriptor instanceof MediaDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
            } else if (ammDescriptor instanceof ModemDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
            } else if (ammDescriptor instanceof MuxDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
            } else if (ammDescriptor instanceof EventsDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
            } else if (ammDescriptor instanceof EventBufferDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
            } else if (ammDescriptor instanceof SignalsDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
            } else if (ammDescriptor instanceof DigitMapDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
            } else if (ammDescriptor instanceof AuditDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
            } else if (ammDescriptor instanceof StatisticsDescriptor) {
                ammDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 8, aos);
            }
        }
        aos.FinalizeContent(lenPos1);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationIDList = new TerminationIDList();
                    terminationIDList.decode(ais);
                    break;
                case 1:
                    this.decodeDescriptors(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeDescriptors(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            AmmDescriptor ammDescriptor = null;
            switch (tag) {
                case 0:
                    ammDescriptor = new MediaDescriptor();
                    ammDescriptor.decode(ais.readSequenceStream());
                    break;
                case 1:
                    ammDescriptor = new ModemDescriptor();
                    ammDescriptor.decode(ais);
                    break;
                case 2:
                    ammDescriptor = new MuxDescriptor();
                    ammDescriptor.decode(ais);
                    break;
                case 3:
                    ammDescriptor = new EventsDescriptor();
                    ammDescriptor.decode(ais);
                    break;
                case 4:
                    ammDescriptor = new EventBufferDescriptor();
                    ammDescriptor.decode(ais);
                    break;
                case 5:
                    ammDescriptor = new SignalsDescriptor();
                    ammDescriptor.decode(ais.readSequenceStream());
                    break;
                case 6:
                    ammDescriptor = new DigitMapDescriptor();
                    ammDescriptor.decode(ais);
                    break;
                case 7:
                    ammDescriptor = new AuditDescriptor();
                    ammDescriptor.decode(ais);
                    break;
                case 8:
                    ammDescriptor = new StatisticsDescriptor();
                    ammDescriptor.decode(ais);
                    break;
            }
            if (ammDescriptor != null) {
                descriptors.add(ammDescriptor);
            }
        }
    }

    public List<AmmDescriptor> getDescriptors() {
        return descriptors;
    }

    public TerminationIDList getTerminationIDList() {
        return terminationIDList;
    }

    public void setTerminationIDList(TerminationIDList terminationID) {
        this.terminationIDList = terminationID;
    }

    public void addAmmDescriptor(AmmDescriptor ammDescriptor) {
        descriptors.add(ammDescriptor);
    }
}
