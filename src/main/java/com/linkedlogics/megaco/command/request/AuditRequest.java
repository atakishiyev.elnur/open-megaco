/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

import com.linkedlogics.megaco.descriptor.AuditDescriptor;
import com.linkedlogics.megaco.parameter.TerminationID;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * AuditRequest ::= SEQUENCE
 * {
 * terminationID [0] TerminationID,
 * auditDescriptor [1] AuditDescriptor,
 * ...,
 * terminationIDList [2] TerminationIDList OPTIONAL
 * }
 * -- terminationID shall contain the first termination in the
 * -- list when using the terminationIDList construct in AuditRequest
 *
 * @author eatakishiyev
 */
public abstract class AuditRequest extends Command {

    private TerminationID terminationID;
    private AuditDescriptor auditDescriptor;

    private TerminationIDList terminationIDList;

    public AuditRequest() {
    }

    public AuditRequest(TerminationID terminationID, AuditDescriptor auditDescriptor) {
        this.terminationID = terminationID;
        this.auditDescriptor = auditDescriptor;
    }

    public AuditRequest(TerminationIDList terminationIDList, AuditDescriptor auditDescriptor) {
        this.terminationID = terminationIDList.getTerminationIDs().get(0);
        this.terminationIDList = terminationIDList;
        this.auditDescriptor = auditDescriptor;
    }

    public AuditDescriptor getAuditDescriptor() {
        return auditDescriptor;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        terminationID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        auditDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        if (terminationIDList != null) {
            terminationIDList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationID = new TerminationID();
                    terminationID.decode(ais);
                    break;
                case 1:
                    this.auditDescriptor = new AuditDescriptor();
                    auditDescriptor.decode(ais);
                    break;
                case 2:
                    this.terminationIDList = new TerminationIDList();
                    terminationIDList.decode(ais);
                    break;
            }
        }
    }

    public void setAuditDescriptor(AuditDescriptor auditDescriptor) {
        this.auditDescriptor = auditDescriptor;
    }

    public TerminationID getTerminationID() {
        return terminationID;
    }

    public void setTerminationID(TerminationID terminationID) {
        this.terminationID = terminationID;
    }

    public TerminationIDList getTerminationIDList() {
        return terminationIDList;
    }

    public void setTerminationIDList(TerminationIDList terminationIDList) {
        this.terminationIDList = terminationIDList;
    }

}
