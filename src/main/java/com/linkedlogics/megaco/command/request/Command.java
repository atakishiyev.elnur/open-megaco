/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

import com.linkedlogics.megaco.parameter.Parameter;

/**
 * Command ::= CHOICE
 {
 addReq [0] AmmRequest,
 moveReq [1] AmmRequest,
 modReq [2] AmmRequest,
 -- Add, Move, Modify requests have the same parameters
 subtractReq [3] SubtractRequest,
 auditCapRequest [4] AuditRequest,
 auditValueRequest [5] AuditRequest,
 notifyReq [6] NotifyRequest,
 serviceChangeReq [7] ServiceChangeRequest,
 ...
 * }
 *
 * @author eatakishiyev
 */
public abstract class Command implements Parameter {

//    private AmmRequest addRequest;
//    private AmmRequest moveRequest;
//    private AmmRequest modifyRequest;
//    private SubtractRequest subtractRequest;
//    private AuditRequest auditCapRequest;
//    private AuditRequest auditValueRequest;
//    private NotifyRequest notifyRequest;
//    private ServiceChangeRequest serviceChangeRequest;
//
    public abstract RequestType getRequestType();
//
//    private Command() {
//
//    }
//
//    public static Command createAddRequest(AmmRequest addRequest) {
//        Command command = new Command();
//        command.addRequest = addRequest;
//        command.commandType = RequestType.ADD_REQ;
//        return command;
//    }
//
//    public static Command createMoveRequest(AmmRequest moveRequest) {
//        Command command = new Command();
//        command.moveRequest = moveRequest;
//        command.commandType = RequestType.MOVE_REQ;
//        return command;
//    }
//
//    public static Command createModifyRequest(AmmRequest modifyRequest) {
//        Command command = new Command();
//        command.modifyRequest = modifyRequest;
//        command.commandType = RequestType.MOD_REQ;
//        return command;
//    }
//
//    public static Command createSubtractRequest(SubtractRequest subtractRequest) {
//        Command command = new Command();
//        command.subtractRequest = subtractRequest;
//        command.commandType = RequestType.SUBTRACT_REQ;
//        return command;
//    }
//
//    public static Command createAuditCapRequest(AuditRequest auditCapRequest) {
//        Command command = new Command();
//        command.auditCapRequest = auditCapRequest;
//        command.commandType = RequestType.AUDIT_CAP_REQ;
//        return command;
//    }
//
//    public static Command createAuditValRequest(AuditRequest auditValRequest) {
//        Command command = new Command();
//        command.auditValueRequest = auditValRequest;
//        command.commandType = RequestType.AUDIT_VAL_REQ;
//        return command;
//    }
//
//    public static Command createNotifyRequest(NotifyRequest notifyRequest) {
//        Command command = new Command();
//        command.notifyRequest = notifyRequest;
//        command.commandType = RequestType.NOTIFY_REQ;
//        return command;
//    }
//
//    public static Command createServiceChangeRequest(ServiceChangeRequest serviceChangeRequest) {
//        Command command = new Command();
//        command.serviceChangeRequest = serviceChangeRequest;
//        command.commandType = RequestType.SERVICE_CHANGE_REQ;
//        return command;
//    }
//
//    public static Command createCommand() {
//        Command command = new Command();
//        return command;
//    }
//
//    @Override
//    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
//        aos.writeTag(tagClass, false, tag);
//        int lenPos = aos.StartContentDefiniteLength();
//
//        if (addRequest != null) {
//            addRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
//        } else if (moveRequest != null) {
//            moveRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
//        } else if (modifyRequest != null) {
//            modifyRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
//        } else if (subtractRequest != null) {
//            subtractRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
//        } else if (auditCapRequest != null) {
//            auditCapRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
//        } else if (auditValueRequest != null) {
//            auditValueRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
//        } else if (notifyRequest != null) {
//            notifyRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
//        } else if (serviceChangeRequest != null) {
//            serviceChangeRequest.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
//        }
//
//        aos.FinalizeContent(lenPos);
//    }
//
//    @Override
//    public void decode(AsnInputStream ais) throws AsnException, IOException {
//        while (ais.available() > 0) {
//            int tag = ais.readTag();
//            switch (tag) {
//                case 0:
//                    this.addRequest = new AmmRequest();
//                    addRequest.decode(ais.readSequenceStream());
//                    this.commandType = RequestType.ADD_REQ;
//                    break;
//                case 1:
//                    this.moveRequest = new AmmRequest();
//                    moveRequest.decode(ais.readSequenceStream());
//                    this.commandType = RequestType.MOVE_REQ;
//                    break;
//                case 2:
//                    this.modifyRequest = new AmmRequest();
//                    modifyRequest.decode(ais.readSequenceStream());
//                    this.commandType = RequestType.MOD_REQ;
//                    break;
//                case 3:
//                    this.subtractRequest = new SubtractRequest();
//                    subtractRequest.decode(ais.readSequenceStream());
//                    this.commandType = RequestType.SUBTRACT_REQ;
//                    break;
//                case 4:
//                    this.auditCapRequest = new AuditRequest();
//                    auditCapRequest.decode(ais.readSequenceStream());
//                    this.commandType = RequestType.AUDIT_CAP_REQ;
//                    break;
//                case 5:
//                    this.auditValueRequest = new AuditRequest();
//                    auditValueRequest.decode(ais.readSequenceStream());
//                    this.commandType = RequestType.AUDIT_VAL_REQ;
//                    break;
//                case 6:
//                    this.notifyRequest = new NotifyRequest();
//                    notifyRequest.decode(ais.readSequenceStream());
//                    this.commandType = RequestType.NOTIFY_REQ;
//                    break;
//                case 7:
//                    this.serviceChangeRequest = new ServiceChangeRequest();
//                    serviceChangeRequest.decode(ais.readSequenceStream());
//                    this.commandType = RequestType.SERVICE_CHANGE_REQ;
//                    break;
//            }
//        }
//    }
//
//    public AmmRequest getAddRequest() {
//        return addRequest;
//    }
//
//    public AuditRequest getAuditCapRequest() {
//        return auditCapRequest;
//    }
//
//    public AuditRequest getAuditValueRequest() {
//        return auditValueRequest;
//    }
//
//    public AmmRequest getModifyRequest() {
//        return modifyRequest;
//    }
//
//    public AmmRequest getMoveRequest() {
//        return moveRequest;
//    }
//
//    public NotifyRequest getNotifyRequest() {
//        return notifyRequest;
//    }
//
//    public ServiceChangeRequest getServiceChangeRequest() {
//        return serviceChangeRequest;
//    }
//
//    public SubtractRequest getSubtractRequest() {
//        return subtractRequest;
//    }
//
//    public RequestType getCommandType() {
//        return commandType;
//    }

}
