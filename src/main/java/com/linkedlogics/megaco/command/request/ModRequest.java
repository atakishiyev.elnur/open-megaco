/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

/**
 *
 * @author eatakishiyev
 */
public class ModRequest  extends AmmRequest{

    @Override
    public RequestType getRequestType() {
        return RequestType.MOD_REQ;
    }
    
}
