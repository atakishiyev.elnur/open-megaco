/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import com.linkedlogics.megaco.descriptor.ObservedEventsDescriptor;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * NotifyRequest ::= SEQUENCE
 * {
 * terminationID [0] TerminationIDList,
 * observedEventsDescriptor [1] ObservedEventsDescriptor,
 * errorDescriptor [2] ErrorDescriptor OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class NotifyRequest extends Command {

    private TerminationIDList terminationID;
    private ObservedEventsDescriptor observedEventsDescriptor;
    private ErrorDescriptor errorDescriptor;

    public NotifyRequest() {
    }

    public NotifyRequest(TerminationIDList terminationID, ObservedEventsDescriptor observedEventsDescriptor) {
        this.terminationID = terminationID;
        this.observedEventsDescriptor = observedEventsDescriptor;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        terminationID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        observedEventsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        if (errorDescriptor != null) {
            errorDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationID = new TerminationIDList();
                    terminationID.decode(ais);
                    break;
                case 1:
                    this.observedEventsDescriptor = new ObservedEventsDescriptor();
                    observedEventsDescriptor.decode(ais);
                    break;
                case 2:
                    this.errorDescriptor = new ErrorDescriptor();
                    errorDescriptor.decode(ais);
                    break;
            }
        }
    }

    public void setErrorDescriptor(ErrorDescriptor errorDescriptor) {
        this.errorDescriptor = errorDescriptor;
    }

    public ErrorDescriptor getErrorDescriptor() {
        return errorDescriptor;
    }

    public void setObservedEventsDescriptor(ObservedEventsDescriptor observedEventsDescriptor) {
        this.observedEventsDescriptor = observedEventsDescriptor;
    }

    public ObservedEventsDescriptor getObservedEventsDescriptor() {
        return observedEventsDescriptor;
    }

    public void setTerminationID(TerminationIDList terminationID) {
        this.terminationID = terminationID;
    }

    public TerminationIDList getTerminationID() {
        return terminationID;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.NOTIFY_REQ;
    }

}
