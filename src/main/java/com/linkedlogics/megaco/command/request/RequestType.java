/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

/**
 *
 * @author eatakishiyev
 */
public enum RequestType {
    ADD_REQ(0),
    MOVE_REQ(1),
    MOD_REQ(2),
    SUBTRACT_REQ(3),
    AUDIT_CAP_REQ(4),
    AUDIT_VAL_REQ(5),
    NOTIFY_REQ(6),
    SERVICE_CHANGE_REQ(7),
    UNKNOWN(-1);

    private final int value;

    private RequestType(int value) {
        this.value = value;
    }

//    public int value() {
//        return this.value;
//    }

    public static RequestType getInstance(int value) {
        switch (value) {
            case 0:
                return ADD_REQ;
            case 1:
                return MOVE_REQ;
            case 2:
                return MOD_REQ;
            case 3:
                return SUBTRACT_REQ;
            case 4:
                return AUDIT_CAP_REQ;
            case 5:
                return AUDIT_VAL_REQ;
            case 6:
                return NOTIFY_REQ;
            case 7:
                return SERVICE_CHANGE_REQ;
            default:
                return UNKNOWN;
        }
    }
}
