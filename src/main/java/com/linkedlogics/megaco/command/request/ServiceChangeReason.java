/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

/**
 *
 * @author eatakishiyev
 */
public class ServiceChangeReason {

    private final String serviceChangeReason;

    public static ServiceChangeReason SERVICE_RESTORED = new ServiceChangeReason("900 Service Restored");
    public static ServiceChangeReason COLD_BOOT = new ServiceChangeReason("901 Cold Boot");
    public static ServiceChangeReason WARM_BOOT = new ServiceChangeReason("902 Warm Boot");
    public static ServiceChangeReason MGC_DIRECTED_CHANGE = new ServiceChangeReason("903 MGC Directed Change");
    public static ServiceChangeReason TERM_MALFUNCTION = new ServiceChangeReason("904 Term malfunction");
    public static ServiceChangeReason TERMINATION_TAKEN_OUT_OF_SERVICE = new ServiceChangeReason("905 Termination Taken out of service");
    public static ServiceChangeReason LOSS_OF_LOWER_LAYER_CONNECTIVITY = new ServiceChangeReason("906 Loss of lower layer connectivity");
    public static ServiceChangeReason TRANSMISSION_FAILURE = new ServiceChangeReason("907 Transmission failure");
    public static ServiceChangeReason MG_IMPENDING_FAILURE = new ServiceChangeReason("908 MG Impending Failure");
    public static ServiceChangeReason MGC_IMPENDING_FAILURE = new ServiceChangeReason("909 MGC Impending Failure");
    public static ServiceChangeReason PACKAGES_CHANGE_FAILURE = new ServiceChangeReason("916 Packages Change");
    public static ServiceChangeReason CAPABILITIES_CHANGE = new ServiceChangeReason("917 Capabilites Change");
    public static ServiceChangeReason CANCEL_GRACEFUL = new ServiceChangeReason("918 Cancel Graceful");
    public static ServiceChangeReason WARM_FAILOVER = new ServiceChangeReason("919 Warm Failover");
    public static ServiceChangeReason COLD_FAILOVER = new ServiceChangeReason("920 Cold Failover");

    public ServiceChangeReason(String serviceChangeReason) {
        this.serviceChangeReason = serviceChangeReason;
    }

    public String getServiceChangeReason() {
        return serviceChangeReason;
    }

}
