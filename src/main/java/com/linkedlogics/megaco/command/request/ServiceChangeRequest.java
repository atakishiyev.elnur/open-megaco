/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

import com.linkedlogics.megaco.parameter.ServiceChangeParm;
import com.linkedlogics.megaco.parameter.TerminationID;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ServiceChangeRequest ::= SEQUENCE
 * {
 * terminationID [0] TerminationIDList,
 * serviceChangeParms [1] ServiceChangeParm,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class ServiceChangeRequest extends Command {

    private TerminationIDList terminationIdList;
    private ServiceChangeParm serviceChangeParms;

    public ServiceChangeRequest() {
    }

    public ServiceChangeRequest(TerminationIDList terminationIdList, ServiceChangeParm serviceChangeParms) {
        this.terminationIdList = terminationIdList;
        this.serviceChangeParms = serviceChangeParms;
    }

    public ServiceChangeRequest(ServiceChangeParm serviceChangeParm, TerminationID... terminationIDs) {
        this.serviceChangeParms = serviceChangeParm;
        this.terminationIdList = new TerminationIDList(terminationIDs);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        terminationIdList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        serviceChangeParms.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationIdList = new TerminationIDList();
                    terminationIdList.decode(ais);
                    break;
                case 1:
                    this.serviceChangeParms = new ServiceChangeParm();
                    serviceChangeParms.decode(ais);
                    break;
            }
        }
    }

    public ServiceChangeParm getServiceChangeParms() {
        return serviceChangeParms;
    }

    public void setServiceChangeParms(ServiceChangeParm serviceChangeParms) {
        this.serviceChangeParms = serviceChangeParms;
    }

    public TerminationIDList getTerminationIDList() {
        return terminationIdList;
    }

    public void setTerminationIdList(TerminationIDList terminationIdList) {
        this.terminationIdList = terminationIdList;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.SERVICE_CHANGE_REQ;
    }

}
