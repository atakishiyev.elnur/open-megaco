/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.command.request;

import com.linkedlogics.megaco.descriptor.AuditDescriptor;
import com.linkedlogics.megaco.parameter.TerminationIDList;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * SubtractRequest ::= SEQUENCE
 * {
 * terminationID [0] TerminationIDList,
 * auditDescriptor [1] AuditDescriptor OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class SubtractRequest extends Command {

    private TerminationIDList terminationIDList;
    private AuditDescriptor auditDescriptor;

    public SubtractRequest() {
    }

    public SubtractRequest(TerminationIDList terminationIDList) {
        this.terminationIDList = terminationIDList;
    }

    public SubtractRequest(TerminationIDList terminationIDList, AuditDescriptor auditDescriptor) {
        this.terminationIDList = terminationIDList;
        this.auditDescriptor = auditDescriptor;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        terminationIDList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);

        if (auditDescriptor != null) {
            auditDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationIDList = new TerminationIDList();
                    terminationIDList.decode(ais);
                    break;
                case 1:
                    this.auditDescriptor = new AuditDescriptor();
                    auditDescriptor.decode(ais);
                    break;
            }
        }
    }

    public AuditDescriptor getAuditDescriptor() {
        return auditDescriptor;
    }

    public void setAuditDescriptor(AuditDescriptor auditDescriptor) {
        this.auditDescriptor = auditDescriptor;
    }

    public TerminationIDList getTerminationIDList() {
        return terminationIDList;
    }

    public void setTerminationIDList(TerminationIDList terminationID) {
        this.terminationIDList = terminationID;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.SUBTRACT_REQ;
    }

}
