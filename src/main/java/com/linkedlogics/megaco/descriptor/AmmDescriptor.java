package com.linkedlogics.megaco.descriptor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * AmmDescriptor ::= CHOICE
 * {
 * mediaDescriptor [0] MediaDescriptor,
 * modemDescriptor [1] ModemDescriptor,
 * muxDescriptor [2] MuxDescriptor,
 * eventsDescriptor [3] EventsDescriptor,
 * eventBufferDescriptor [4] EventBufferDescriptor,
 * signalsDescriptor [5] SignalsDescriptor,
 * digitMapDescriptor [6] DigitMapDescriptor,
 * auditDescriptor [7] AuditDescriptor,
 * ...,
 * statisticsDescriptor [8] StatisticsDescriptor
 * }
 *
 * @author eatakishiyev
 */
public interface AmmDescriptor extends Descriptor {

}
