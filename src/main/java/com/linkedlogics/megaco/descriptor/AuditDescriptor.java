/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.IndAuditParameter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.BitSetStrictLength;
import org.mobicents.protocols.asn.Tag;

/**
 * AuditDescriptor ::= SEQUENCE
 * {
 * auditToken [0] BIT STRING
 * {
 * muxToken(0),
 * modemToken(1),
 * mediaToken(2),
 * eventsToken(3),
 * signalsToken(4),
 * digitMapToken(5),
 * statsToken(6),
 * observedEventsToken(7),
 * packagesToken(8),
 * eventBufferToken(9)
 * } OPTIONAL,
 * ...,
 * auditPropertyToken [1] SEQUENCE OF IndAuditParameter OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class AuditDescriptor implements Descriptor, AmmDescriptor {

    private BitSetStrictLength auditToken;
    private final List<IndAuditParameter> auditPropertyToken = new ArrayList<>();

    public AuditDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (auditToken != null) {
            aos.writeBitString(Tag.CLASS_CONTEXT_SPECIFIC, 0, auditToken);
        }
        if (auditPropertyToken.size() > 0) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
            int lenPos1 = aos.StartContentDefiniteLength();
            for (IndAuditParameter indAuditParameter : auditPropertyToken) {
                indAuditParameter.encode(aos);
            }
            aos.FinalizeContent(lenPos1);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.auditToken = ais.readBitString();
                    break;
                case 1:
                    this.decodeIndAuditParameter(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeIndAuditParameter(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            IndAuditParameter indAuditParameter = new IndAuditParameter();
            indAuditParameter.decode(ais);
            auditPropertyToken.add(indAuditParameter);
        }
    }

    private void initBitSet() {
        if (auditToken == null) {
            auditToken = new BitSetStrictLength(10);
        }
    }

    public void setMuxToken(boolean muxToken) {
        setToken(0, muxToken);
    }

    public boolean getMuxToken() {
        return getToken(0);
    }

    public void setModemToken(boolean modemToken) {
        setToken(1, modemToken);
    }

    public boolean getModemToken() {
        return getToken(1);
    }

    public void setMediaToken(boolean mediaToken) {
        setToken(2, mediaToken);
    }

    public boolean getMediaToken() {
        return getToken(2);
    }

    public void setEventsToken(boolean eventsToken) {
        setToken(3, eventsToken);
    }

    public boolean getEventsToken() {
        return getToken(3);
    }

    public void setSignalsToken(boolean signalsToken) {
        setToken(4, signalsToken);
    }

    public boolean getSignalsToken() {
        return getToken(4);
    }

    public void setDigitMapToken(boolean digitMapToken) {
        setToken(5, digitMapToken);
    }

    public boolean getDigitMapToken() {
        return getToken(5);
    }

    public void setStatsToken(boolean statsToken) {
        setToken(6, statsToken);
    }

    public boolean getStatsToken() {
        return getToken(6);
    }

    public void setObservedEventsToken(boolean observedEventsToken) {
        setToken(7, observedEventsToken);
    }

    public boolean getObservedEventsToken() {
        return getToken(7);
    }

    public void setPackagesToken(boolean packagesToken) {
        setToken(8, packagesToken);
    }

    public boolean getPackagesToken() {
        return getToken(8);
    }

    public void setEventBufferToken(boolean eventBufferToken) {
        setToken(9, eventBufferToken);
    }

    public boolean getEventBufferToken() {
        return getToken(9);
    }

    private boolean getToken(int index) {
        return auditToken == null ? false : auditToken.get(index);
    }

    private void setToken(int index, boolean value) {
        initBitSet();
        auditToken.set(index, value);
    }

}
