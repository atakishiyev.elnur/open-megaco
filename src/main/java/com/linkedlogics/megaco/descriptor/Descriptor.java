/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.Decodable;
import com.linkedlogics.megaco.parameter.Encodable;
import java.io.Serializable;

/**
 *
 * @author eatakishiyev
 */
public interface Descriptor extends Encodable, Decodable,Serializable{
    
}
