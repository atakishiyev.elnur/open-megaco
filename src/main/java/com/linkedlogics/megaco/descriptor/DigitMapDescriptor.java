/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.DigitMapName;
import com.linkedlogics.megaco.parameter.DigitMapValue;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * DigitMapDescriptor ::= SEQUENCE
 * {
 * digitMapName DigitMapName OPTIONAL,
 * digitMapValue DigitMapValue OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class DigitMapDescriptor implements Descriptor, AmmDescriptor {

    private DigitMapName digitMapName;
    private DigitMapValue digitMapValue;

    public DigitMapDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (digitMapName != null) {
            digitMapName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }
        if (digitMapValue != null) {
            digitMapValue.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.digitMapName = new DigitMapName();
                    digitMapName.decode(ais);
                    break;
                case 1:
                    this.digitMapValue = new DigitMapValue();
                    digitMapValue.decode(ais);
                    break;
            }
        }
    }

    public DigitMapName getDigitMapName() {
        return digitMapName;
    }

    public void setDigitMapName(DigitMapName digitMapName) {
        this.digitMapName = digitMapName;
    }

    public DigitMapValue getDigitMapValue() {
        return digitMapValue;
    }

    public void setDigitMapValue(DigitMapValue digitMapValue) {
        this.digitMapValue = digitMapValue;
    }

}
