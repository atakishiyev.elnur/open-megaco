/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.EventSpec;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * EventBufferDescriptor ::= SEQUENCE OF EventSpec
 *
 * @author eatakishiyev
 */
public class EventBufferDescriptor implements Descriptor,AmmDescriptor {

    private final List<EventSpec> eventSpecs = new ArrayList<>();

    public EventBufferDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        for (EventSpec eventSpec : eventSpecs) {
            eventSpec.encode(Tag.CLASS_CONTEXT_SPECIFIC, Tag.SEQUENCE, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            EventSpec eventSpec = new EventSpec();
            eventSpec.decode(ais);
            eventSpecs.add(eventSpec);
        }
    }

    public List<EventSpec> getEventSpecs() {
        return eventSpecs;
    }

}
