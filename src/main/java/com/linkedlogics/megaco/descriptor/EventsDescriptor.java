/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.RequestID;
import com.linkedlogics.megaco.parameter.RequestedEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * EventsDescriptor ::= SEQUENCE
 * {
 * requestID [0] RequestID OPTIONAL,
 * -- RequestID must be present if eventList
 * -- is non empty
 * eventList SEQUENCE OF RequestedEvent,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class EventsDescriptor implements Descriptor ,AmmDescriptor{

    private RequestID requestID;
    private final List<RequestedEvent> eventList = new ArrayList<>();

    public EventsDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (requestID != null) {
            requestID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        if (!eventList.isEmpty()) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
            int lenPos1 = aos.StartContentDefiniteLength();

            for (RequestedEvent requestedEvent : eventList) {
                requestedEvent.encode(Tag.CLASS_CONTEXT_SPECIFIC, Tag.SEQUENCE, aos);
            }

            aos.FinalizeContent(lenPos1);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.requestID = new RequestID();
                    requestID.decode(ais);
                    break;
                case 1:
                    this.decodeRequestedEvents(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeRequestedEvents(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            RequestedEvent requestedEvent = new RequestedEvent();
            requestedEvent.decode(ais);
            eventList.add(requestedEvent);
        }
    }

    public List<RequestedEvent> getEventList() {
        return eventList;
    }

    public void setRequestID(RequestID requestID) {
        this.requestID = requestID;
    }

    public RequestID getRequestID() {
        return requestID;
    }

}
