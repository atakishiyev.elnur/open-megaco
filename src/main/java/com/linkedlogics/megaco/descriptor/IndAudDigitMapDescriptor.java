/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.DigitMapName;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudDigitMapDescriptor ::= SEQUENCE
 * {
 * digitMapName [0] DigitMapName OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class IndAudDigitMapDescriptor implements Descriptor {

    private DigitMapName digitMapName;

    public IndAudDigitMapDescriptor() {
    }

    public IndAudDigitMapDescriptor(DigitMapName digitMapName) {
        this.digitMapName = digitMapName;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        digitMapName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        ais = ais.readSequenceStream();
        while (ais.available() > 0) {
            int tag = ais.readTag();
            this.digitMapName = new DigitMapName();
            digitMapName.decode(ais);
        }
    }

    public DigitMapName getDigitMapName() {
        return digitMapName;
    }

    public void setDigitMapName(DigitMapName digitMapName) {
        this.digitMapName = digitMapName;
    }

}
