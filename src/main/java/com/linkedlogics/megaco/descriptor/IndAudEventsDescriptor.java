/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.EventName;
import com.linkedlogics.megaco.parameter.PkgdName;
import com.linkedlogics.megaco.parameter.RequestID;
import com.linkedlogics.megaco.parameter.StreamID;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudEventsDescriptor ::= SEQUENCE
 * {
 * requestID [0] RequestID OPTIONAL,
 * pkgdName [1] PkgdName,
 * streamID [2] StreamID OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class IndAudEventsDescriptor implements Descriptor {

    private RequestID requestId;// optional
    private EventName eventName;
    private StreamID streamId; //optional

    public IndAudEventsDescriptor() {
    }

    public IndAudEventsDescriptor(EventName pkgdName) {
        this.eventName = pkgdName;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (requestId != null) {
            requestId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        eventName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        if (streamId != null) {
            streamId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.requestId = new RequestID();
                    this.requestId.decode(ais);
                    break;
                case 1:
                    this.eventName = new EventName();
                    eventName.decode(ais);
                    break;
                case 2:
                    this.streamId = new StreamID();
                    streamId.decode(ais);
                    break;
            }
        }
    }

    public EventName getEventName() {
        return eventName;
    }

    public void setEventName(EventName pkgdName) {
        this.eventName = pkgdName;
    }

    public RequestID getRequestId() {
        return requestId;
    }

    public void setRequestId(RequestID requestId) {
        this.requestId = requestId;
    }

    public StreamID getStreamId() {
        return streamId;
    }

    public void setStreamId(StreamID streamId) {
        this.streamId = streamId;
    }

}
