/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.IndAudPropertyParm;
import com.linkedlogics.megaco.parameter.NullType;
import com.linkedlogics.megaco.parameter.StreamMode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudLocalControlDescriptor ::= SEQUENCE
 * {
 * streamMode [0] NULL OPTIONAL,
 * reserveValue [1] NULL OPTIONAL,
 * reserveGroup [2] NULL OPTIONAL,
 * propertyParms [3] SEQUENCE OF IndAudPropertyParm OPTIONAL,
 * ...,
 * streamModeSel [4] StreamMode OPTIONAL
 * }
 * -- must not have both streamMode and streamModeSel
 * -- if both are present only streamModeSel shall be honoured
 *
 * @author eatakishiyev
 */
public class IndAudLocalControlDescriptor implements Descriptor {

    private NullType streamMode;// Optional
    private NullType reserveValue; // Optional
    private NullType reserveGroup; // Optional
    private final List<IndAudPropertyParm> propertyParms = new ArrayList<>(); // Optional
    private StreamMode streamModeSel; // Optional

    public IndAudLocalControlDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (streamMode != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 0);
        }

        if (reserveValue != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 1);
        }

        if (reserveGroup != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 2);
        }

        if (propertyParms.size() > 0) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 3);
            int lenPos1 = aos.StartContentDefiniteLength();
            for (IndAudPropertyParm propertyParm : propertyParms) {
                propertyParm.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
            }
            aos.FinalizeContent(lenPos1);
        }

        if (streamModeSel != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 4, streamModeSel.value());
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.streamMode = new NullType();
                    ais.readNull();
                    break;
                case 1:
                    this.reserveValue = new NullType();
                    ais.readNull();
                    break;
                case 2:
                    this.reserveGroup = new NullType();
                    ais.readNull();
                    break;
                case 3:
                    this.decodePropertyParms(ais.readSequenceStream());
                    break;
                case 4:
                    this.streamModeSel = StreamMode.getInstance((int) ais.readInteger());
                    break;
            }
        }
    }

    private void decodePropertyParms(AsnInputStream ais) throws IOException, AsnException {
        while(ais.available() > 0){
            int tag = ais.readTag();
            IndAudPropertyParm indAudPropertyParm = new IndAudPropertyParm();
            indAudPropertyParm.decode(ais);
            propertyParms.add(indAudPropertyParm);
        }
    }

    public NullType getStreamMode() {
        return streamMode;
    }

    public void setStreamMode(NullType streamMode) {
        this.streamMode = streamMode;
    }

    public NullType getReserveValue() {
        return reserveValue;
    }

    public void setReserveValue(NullType reserveValue) {
        this.reserveValue = reserveValue;
    }

    public List<IndAudPropertyParm> getPropertyParms() {
        return propertyParms;
    }

    public NullType getReserveGroup() {
        return reserveGroup;
    }

    public void setReserveGroup(NullType reserveGroup) {
        this.reserveGroup = reserveGroup;
    }

    public StreamMode getStreamModeSel() {
        return streamModeSel;
    }

    public void setStreamModeSel(StreamMode streamModeSel) {
        this.streamModeSel = streamModeSel;
    }

}
