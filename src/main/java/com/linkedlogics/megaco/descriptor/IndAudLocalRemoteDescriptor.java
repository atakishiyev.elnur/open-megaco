/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.IndAudPropertyGroup;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudLocalRemoteDescriptor ::= SEQUENCE
 * {
 * propGroupID [0] INTEGER(0..65535) OPTIONAL,
 * propGrps [1] IndAudPropertyGroup,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class IndAudLocalRemoteDescriptor implements Descriptor {

    private Integer propGroupID;//Optional [0..65535]
    private IndAudPropertyGroup propGrps;

    public IndAudLocalRemoteDescriptor() {
    }

    public IndAudLocalRemoteDescriptor(IndAudPropertyGroup propGrps) {
        this.propGrps = propGrps;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (propGroupID != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, propGroupID);
        }
        propGrps.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.propGroupID = (int) ais.readInteger();
                    break;
                case 1:
                    this.propGrps = new IndAudPropertyGroup();
                    propGrps.decode(ais);
                    break;
            }
        }
    }

    public Integer getPropGroupID() {
        return propGroupID;
    }

    public void setPropGroupID(Integer propGroupID) {
        this.propGroupID = propGroupID;
    }

    public IndAudPropertyGroup getPropGrps() {
        return propGrps;
    }

    public void setPropGrps(IndAudPropertyGroup propGrps) {
        this.propGrps = propGrps;
    }

}
