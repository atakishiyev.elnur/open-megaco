/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.IndAudStreamParms;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudMediaDescriptor ::= SEQUENCE
 * {
 * termStateDescr [0] IndAudTerminationStateDescriptor OPTIONAL,
 * streams [1] CHOICE
 * {
 * oneStream [0] IndAudStreamParms,
 * multiStream [1] SEQUENCE OF IndAudStreamDescriptor
 * } OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class IndAudMediaDescriptor implements Descriptor {

    private IndAudTerminationStateDescriptor termStateDesc;
    //Choice
    private IndAudStreamParms oneStream;
    private final List<IndAudStreamDescriptor> multiStream = new ArrayList<>();

    public IndAudMediaDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (termStateDesc != null) {
            termStateDesc.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        if (oneStream != null || multiStream != null) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);//streams CHOICE
            int lenPos1 = aos.StartContentDefiniteLength();
            if (oneStream != null) {
                oneStream.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
            } else if (multiStream != null) {
                aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
                int lenPos2 = aos.StartContentDefiniteLength();
                for (IndAudStreamDescriptor indAudStreamDescriptor : multiStream) {
                    indAudStreamDescriptor.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
                }
                aos.FinalizeContent(lenPos2);
            }
            aos.FinalizeContent(lenPos1);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.termStateDesc = new IndAudTerminationStateDescriptor();
                    termStateDesc.decode(ais.readSequenceStream());
                    break;
                case 1:
                    this.oneStream = new IndAudStreamParms();
                    oneStream.decode(ais.readSequenceStream());
                    break;
                case 2:
                    this.decodeMultiStream(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeMultiStream(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            IndAudStreamDescriptor indAudStreamDescriptor = new IndAudStreamDescriptor();
            indAudStreamDescriptor.decode(ais);
            multiStream.add(indAudStreamDescriptor);
        }
    }

    public IndAudTerminationStateDescriptor getTermStateDesc() {
        return termStateDesc;
    }

    public void setTermStateDesc(IndAudTerminationStateDescriptor termStateDesc) {
        this.termStateDesc = termStateDesc;
    }

    public IndAudStreamParms getOneStream() {
        return oneStream;
    }

    public List<IndAudStreamDescriptor> getMultiStream() {
        return multiStream;
    }

    public void setOneStream(IndAudStreamParms oneStream) {
        this.oneStream = oneStream;
        multiStream.clear();
    }

}
