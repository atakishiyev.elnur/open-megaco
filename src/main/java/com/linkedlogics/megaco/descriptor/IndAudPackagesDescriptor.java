/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.IndAuditParameter;
import com.linkedlogics.megaco.parameter.Name;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudPackagesDescriptor ::= SEQUENCE
 * {
 * packageName [0] Name,
 * packageVersion [1] INTEGER(0..99),
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class IndAudPackagesDescriptor implements Descriptor {

    private Name packageName;
    private Integer packageVersion;

    public IndAudPackagesDescriptor() {
    }

    public IndAudPackagesDescriptor(Name packageName, Integer packageVersion) {
        this.packageName = packageName;
        this.packageVersion = packageVersion;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        packageName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 1, packageVersion);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        ais = ais.readSequenceStream();
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.packageName = new Name();
                    packageName.decode(ais);
                    break;
                case 1:
                    this.packageVersion = (int) ais.readInteger();
                    break;
            }
        }
    }

    public Name getPackageName() {
        return packageName;
    }

    public void setPackageName(Name packageName) {
        this.packageName = packageName;
    }

    public Integer getPackageVersion() {
        return packageVersion;
    }

    public void setPackageVersion(Integer packageVersion) {
        this.packageVersion = packageVersion;
    }

}
