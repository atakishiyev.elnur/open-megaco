/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.IndAudSeqSigList;
import com.linkedlogics.megaco.parameter.IndAudSignal;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudSignalsDescriptor ::=CHOICE
 * {
 * signal [0] IndAudSignal,
 * seqSigList [1] IndAudSeqSigList,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class IndAudSignalsDescriptor implements Descriptor {

    private IndAudSignal signal;
    private IndAudSeqSigList seqSigList;

    public IndAudSignalsDescriptor() {
    }

    public IndAudSignalsDescriptor(IndAudSignal signal) {
        this.signal = signal;
    }

    public IndAudSignalsDescriptor(IndAudSeqSigList seqSigList) {
        this.seqSigList = seqSigList;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (signal != null) {
            signal.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else {
            seqSigList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        ais = ais.readSequenceStream();
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    signal = new IndAudSignal();
                    signal.decode(ais);
                    break;
                case 1:
                    seqSigList = new IndAudSeqSigList();
                    seqSigList.decode(ais);
                    break;
            }
        }
    }

    public IndAudSignal getSignal() {
        return signal;
    }

    public IndAudSeqSigList getSeqSigList() {
        return seqSigList;
    }

}
