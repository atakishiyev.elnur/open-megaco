/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.IndAudStreamParms;
import com.linkedlogics.megaco.parameter.StreamID;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudStreamDescriptor ::= SEQUENCE
 * {
 * streamID [0] StreamID,
 * streamParms [1] IndAudStreamParms
 * }
 *
 * @author eatakishiyev
 */
public class IndAudStreamDescriptor implements Descriptor {

    private StreamID streamId;
    private IndAudStreamParms streamParams;

    public IndAudStreamDescriptor() {
    }

    public IndAudStreamDescriptor(StreamID streamId, IndAudStreamParms streamParams) {
        this.streamId = streamId;
        this.streamParams = streamParams;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        streamId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        streamParams.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.streamId = new StreamID();
                    streamId.decode(ais);
                    break;
                case 1:
                    this.streamParams = new IndAudStreamParms();
                    streamParams.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

    public StreamID getStreamId() {
        return streamId;
    }

    public void setStreamId(StreamID streamId) {
        this.streamId = streamId;
    }

    public IndAudStreamParms getStreamParams() {
        return streamParams;
    }

    public void setStreamParams(IndAudStreamParms streamParams) {
        this.streamParams = streamParams;
    }

}
