/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.IndAudPropertyParm;
import com.linkedlogics.megaco.parameter.NullType;
import com.linkedlogics.megaco.parameter.ServiceState;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudTerminationStateDescriptor ::= SEQUENCE
 * {
 * propertyParms [0] SEQUENCE OF IndAudPropertyParm,
 * eventBufferControl [1] NULL OPTIONAL,
 * serviceState [2] NULL OPTIONAL,
 * ...,
 * serviceStateSel [3] ServiceState OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class IndAudTerminationStateDescriptor implements Descriptor {

    private final List<IndAudPropertyParm> propertyParms = new ArrayList<>();
    private NullType eventBufferControl;//optional
    private NullType serviceState;//optional
    private ServiceState serviceStateSel;//optional

    public IndAudTerminationStateDescriptor() {
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    decodePropertyParms(ais.readSequenceStream());
                    break;
                case 1:
                    this.eventBufferControl = new NullType();
                    ais.readNull();
                    break;
                case 2:
                    this.serviceState = new NullType();
                    ais.readNull();
                    break;
                case 3:
                    this.serviceStateSel = ServiceState.getInstance((int) ais.readInteger());
                    break;
            }
        }
    }

    private void decodePropertyParms(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            IndAudPropertyParm indAudPropertyParm = new IndAudPropertyParm();
            indAudPropertyParm.decode(ais);
            propertyParms.add(indAudPropertyParm);
        }
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 0);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (IndAudPropertyParm audPropertyParm : propertyParms) {
            audPropertyParm.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        if (eventBufferControl != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 1);
        }

        if (serviceState != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 2);
        }

        if (serviceStateSel != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 3, serviceStateSel.value());
        }

        aos.FinalizeContent(lenPos);
    }

    public void setEventBufferControl(NullType eventBufferControl) {
        this.eventBufferControl = eventBufferControl;
    }

    public NullType getEventBufferControl() {
        return eventBufferControl;
    }

    public void setServiceState(NullType serviceState) {
        this.serviceState = serviceState;
    }

    public NullType getServiceState() {
        return serviceState;
    }

    public void setServiceStateSel(ServiceState serviceStateSel) {
        this.serviceStateSel = serviceStateSel;
    }

    public ServiceState getServiceStateSel() {
        return serviceStateSel;
    }

    public List<IndAudPropertyParm> getPropertyParms() {
        return propertyParms;
    }

}
