/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.PropertyParm;
import com.linkedlogics.megaco.parameter.StreamMode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * LocalControlDescriptor ::= SEQUENCE
 * {
 * streamMode [0] StreamMode OPTIONAL,
 * reserveValue [1] BOOLEAN OPTIONAL,
 * reserveGroup [2] BOOLEAN OPTIONAL,
 * propertyParms [3] SEQUENCE OF PropertyParm,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class LocalControlDescriptor implements Descriptor {

    private StreamMode streamMode;
    private Boolean reserveValue;
    private Boolean reserveGroup;
    private final List<PropertyParm> propertyParms = new ArrayList<>();

    public LocalControlDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (streamMode != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, streamMode.value());
        }
        if (reserveValue != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 1, reserveValue);
        }
        if (reserveGroup != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 2, reserveGroup);
        }

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 3);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (PropertyParm propertyParm : propertyParms) {
            propertyParm.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.streamMode = StreamMode.getInstance((int) ais.readInteger());
                    break;
                case 1:
                    this.reserveValue = ais.readBoolean();
                    break;
                case 2:
                    this.reserveGroup = ais.readBoolean();
                    break;
                case 3:
                    this.decodePropertyParms(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodePropertyParms(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            PropertyParm propertyParm = new PropertyParm();
            propertyParm.decode(ais);
            propertyParms.add(propertyParm);
        }
    }

    public StreamMode getStreamMode() {
        return streamMode;
    }

    public void setStreamMode(StreamMode streamMode) {
        this.streamMode = streamMode;
    }

    public Boolean getReserveValue() {
        return reserveValue;
    }

    public void setReserveValue(Boolean reserveValue) {
        this.reserveValue = reserveValue;
    }

    public List<PropertyParm> getPropertyParms() {
        return propertyParms;
    }

    public Boolean getReserveGroup() {
        return reserveGroup;
    }

    public void setReserveGroup(Boolean reserveGroup) {
        this.reserveGroup = reserveGroup;
    }

    public void addPropertyParm(PropertyParm propertyParm) {
        propertyParms.add(propertyParm);
    }
}
