/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.PropertyGroup;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * LocalRemoteDescriptor ::= SEQUENCE
 * {
 * propGrps [0] SEQUENCE OF PropertyGroup
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class LocalRemoteDescriptor implements Descriptor {

    private final List<PropertyGroup> propGrps = new ArrayList<>();

    public LocalRemoteDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 0);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (PropertyGroup propertyGroup : propGrps) {
            propertyGroup.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.decodePropGrps(ais.readSequenceStream());
                    break;
            }

        }
    }

    private void decodePropGrps(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            PropertyGroup propertyGroup = new PropertyGroup();
            propertyGroup.decode(ais.readSequenceStream());
            propGrps.add(propertyGroup);
        }
    }

    public List<PropertyGroup> getPropGrps() {
        return propGrps;
    }

    public void addPropertyGroup(PropertyGroup propertyGroup) {
        propGrps.add(propertyGroup);
    }
}
