/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.Streams;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * MediaDescriptor ::= SEQUENCE
 * {
 * termStateDescr [0] TerminationStateDescriptor OPTIONAL,
 * streams [1] Streams,
 *
 * }
 *
 * @author eatakishiyev
 */
public class MediaDescriptor implements Descriptor, AmmDescriptor {

    private TerminationStateDescriptor termStateDescr;
    private Streams streams;

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (termStateDescr != null) {
            termStateDescr.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        if (streams != null) {
            streams.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.termStateDescr = new TerminationStateDescriptor();
                    termStateDescr.decode(ais.readSequenceStream());
                    break;
                case 1:
                    this.streams = new Streams();
                    streams.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

    public TerminationStateDescriptor getTermStateDescr() {
        return termStateDescr;
    }

    public void setTermStateDescr(TerminationStateDescriptor termStateDescr) {
        this.termStateDescr = termStateDescr;
    }

    public Streams getStreams() {
        return streams;
    }

    public void setStreams(Streams streams) {
        this.streams = streams;
    }
}
