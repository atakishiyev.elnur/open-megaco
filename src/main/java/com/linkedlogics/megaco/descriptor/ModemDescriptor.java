/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.ModemType;
import com.linkedlogics.megaco.parameter.NonStandardData;
import com.linkedlogics.megaco.parameter.PropertyParm;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ModemDescriptor ::= SEQUENCE
 * {
 * mtl [0] SEQUENCE OF ModemType,
 * mpl [1] SEQUENCE OF PropertyParm,
 * nonStandardData [2] NonStandardData OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class ModemDescriptor implements Descriptor,AmmDescriptor {

    private final List<ModemType> mtl = new ArrayList<>();
    private final List<PropertyParm> mpl = new ArrayList<>();
    private NonStandardData nonStandardData;

    public ModemDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        //MTL
        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 0);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (ModemType modemType : mtl) {
            aos.writeInteger(modemType.value());
        }
        aos.FinalizeContent(lenPos1);

        //MPL
        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
        int lenPos2 = aos.StartContentDefiniteLength();
        for (PropertyParm propertyParm : mpl) {
            propertyParm.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos2);

        if (nonStandardData != null) {
            nonStandardData.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.decodeMtl(ais.readSequenceStream());
                    break;
                case 1:
                    this.decodeMpl(ais.readSequenceStream());
                    break;
                case 2:
                    this.nonStandardData = new NonStandardData();
                    nonStandardData.decode(ais);
                    break;

            }
        }
    }

    private void decodeMtl(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            ModemType modemType = ModemType.getInstance((int) ais.readInteger());
            mtl.add(modemType);
        }
    }

    private void decodeMpl(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            PropertyParm propertyParm = new PropertyParm();
            propertyParm.decode(ais);
            mpl.add(propertyParm);
        }
    }

    public List<PropertyParm> getMpl() {
        return mpl;
    }

    public List<ModemType> getMtl() {
        return mtl;
    }

    public NonStandardData getNonStandardData() {
        return nonStandardData;
    }

    public void setNonStandardData(NonStandardData nonStandardData) {
        this.nonStandardData = nonStandardData;
    }

}
