/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.MuxType;
import com.linkedlogics.megaco.parameter.NonStandardData;
import com.linkedlogics.megaco.parameter.TerminationID;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 *
 * MuxDescriptor ::= SEQUENCE
 * {
 * muxType [0] MuxType,
 * termList [1] SEQUENCE OF TerminationID,
 * nonStandardData [2] NonStandardData OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class MuxDescriptor implements Descriptor, AmmDescriptor {

    private MuxType muxType;
    private final List<TerminationID> termList = new ArrayList<>();
    private NonStandardData nonStandardData;

    public MuxDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, muxType.value());

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
        int lenPos2 = aos.StartContentDefiniteLength();
        for (TerminationID terminationID : termList) {
            terminationID.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos2);

        if (nonStandardData != null) {
            nonStandardData.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.muxType = MuxType.getInstance((int) ais.readInteger());
                    break;
                case 1:
                    this.decodeTerminationList(ais.readSequenceStream());
                    break;
                case 2:
                    this.nonStandardData = new NonStandardData();
                    nonStandardData.decode(ais);
                    break;
            }
        }
    }

    private void decodeTerminationList(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            TerminationID terminationID = new TerminationID();
            terminationID.decode(ais);
            termList.add(terminationID);
        }
    }

    public MuxType getMuxType() {
        return muxType;
    }

    public void setMuxType(MuxType muxType) {
        this.muxType = muxType;
    }

    public NonStandardData getNonStandardData() {
        return nonStandardData;
    }

    public void setNonStandardData(NonStandardData nonStandardData) {
        this.nonStandardData = nonStandardData;
    }

    public List<TerminationID> getTermList() {
        return termList;
    }

}
