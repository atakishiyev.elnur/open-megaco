/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.ObservedEvent;
import com.linkedlogics.megaco.parameter.RequestID;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ObservedEventsDescriptor ::= SEQUENCE
 * {
 * requestId [0] RequestID,
 * observedEventLst [1] SEQUENCE OF ObservedEvent
 * }
 *
 * @author eatakishiyev
 */
public class ObservedEventsDescriptor implements Descriptor {

    private RequestID requestID;
    private final List<ObservedEvent> observedEventList = new ArrayList<>();

    public ObservedEventsDescriptor() {
    }

    public ObservedEventsDescriptor(RequestID requestID) {
        this.requestID = requestID;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        requestID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (ObservedEvent observedEvent : observedEventList) {
            observedEvent.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.requestID = new RequestID();
                    requestID.decode(ais);
                    break;
                case 1:
                    this.decodeObservedEventList(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeObservedEventList(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            ObservedEvent observedEvent = new ObservedEvent();
            observedEvent.decode(ais);
            observedEventList.add(observedEvent);
        }
    }

    public List<ObservedEvent> getObservedEventList() {
        return observedEventList;
    }

    public RequestID getRequestID() {
        return requestID;
    }

    public void setRequestID(RequestID requestID) {
        this.requestID = requestID;
    }

    public void addObservedEvent(ObservedEvent observedEvent) {
        observedEventList.add(observedEvent);
    }
}
