/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.PackagesItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * PackagesDescriptor ::= SEQUENCE OF PackagesItem
 *
 * @author eatakishiyev
 */
public class PackagesDescriptor implements Descriptor {

    private final List<PackagesItem> packagesItems = new ArrayList<>();

    public PackagesDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        for (PackagesItem packagesItem : packagesItems) {
            packagesItem.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            PackagesItem packagesItem = new PackagesItem();
            packagesItem.decode(ais);
            packagesItems.add(packagesItem);
        }
    }

    public List<PackagesItem> getPackagesItems() {
        return packagesItems;
    }

}
