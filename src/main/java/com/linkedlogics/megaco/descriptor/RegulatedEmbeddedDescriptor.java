/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.Parameter;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * RegulatedEmbeddedDescriptor ::= SEQUENCE
 * {
 * secondEvent [0] SecondEventsDescriptor OPTIONAL,
 * signalsDescriptor [1] SignalsDescriptor OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class RegulatedEmbeddedDescriptor implements Descriptor {

    private SecondEventDescriptor secondEvent;
    private SignalsDescriptor signalsDescriptor;

    public RegulatedEmbeddedDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (secondEvent != null) {
            secondEvent.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        if (signalsDescriptor != null) {
            signalsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.secondEvent = new SecondEventDescriptor();
                    secondEvent.decode(ais);
                    break;
                case 1:
                    this.signalsDescriptor = new SignalsDescriptor();
                    signalsDescriptor.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

    public SecondEventDescriptor getSecondEvent() {
        return secondEvent;
    }

    public void setSecondEvent(SecondEventDescriptor secondEvent) {
        this.secondEvent = secondEvent;
    }

    public SignalsDescriptor getSignalsDescriptor() {
        return signalsDescriptor;
    }

    public void setSignalsDescriptor(SignalsDescriptor signalsDescriptor) {
        this.signalsDescriptor = signalsDescriptor;
    }

}
