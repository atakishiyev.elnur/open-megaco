/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.Parameter;
import com.linkedlogics.megaco.parameter.RequestID;
import com.linkedlogics.megaco.parameter.SecondRequestedEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * SecondEventsDescriptor ::= SEQUENCE
 * {
 * requestID [0] RequestID OPTIONAL,
 * eventList [1] SEQUENCE OF SecondRequestedEvent,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class SecondEventDescriptor implements Descriptor {

    private RequestID requestID;
    private final List<SecondRequestedEvent> eventList = new ArrayList<>();

    public SecondEventDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (requestID != null) {
            requestID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (SecondRequestedEvent secondRequestedEvent : eventList) {
            secondRequestedEvent.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.requestID = new RequestID();
                    requestID.decode(ais);
                    break;
                case 1:
                    this.decodeSecondRequestedEvent(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeSecondRequestedEvent(AsnInputStream ais) throws IOException, AsnException {
        while(ais.available() > 0){
            int tag = ais.readTag();
            SecondRequestedEvent secondRequestedEvent = new SecondRequestedEvent();
            secondRequestedEvent.decode(ais);
            eventList.add(secondRequestedEvent);
        }
    }

    public List<SecondRequestedEvent> getEventList() {
        return eventList;
    }

    public RequestID getRequestID() {
        return requestID;
    }

    public void setRequestID(RequestID requestID) {
        this.requestID = requestID;
    }

}
