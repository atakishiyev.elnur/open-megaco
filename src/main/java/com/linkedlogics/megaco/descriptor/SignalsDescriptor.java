/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.SignalRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * SignalsDescriptor ::= SEQUENCE OF SignalRequest
 *
 * @author eatakishiyev
 */
public class SignalsDescriptor implements Descriptor, AmmDescriptor {

    private final List<SignalRequest> signalRequests = new ArrayList<>();

    public SignalsDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        for (SignalRequest signalRequest : signalRequests) {
            signalRequest.encode(aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            SignalRequest signalRequest = new SignalRequest();
            signalRequest.decode(ais);
            signalRequests.add(signalRequest);
        }
    }

    public List<SignalRequest> getSignalRequests() {
        return signalRequests;
    }

    public void addSignalRequest(SignalRequest signalRequest) {
        signalRequests.add(signalRequest);
    }
}
