/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor;

import com.linkedlogics.megaco.parameter.EventBufferControl;
import com.linkedlogics.megaco.parameter.PropertyParm;
import com.linkedlogics.megaco.parameter.ServiceState;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TerminationStateDescriptor ::= SEQUENCE
 * {
 * propertyParms [0] SEQUENCE OF PropertyParm,
 * eventBufferControl [1] EventBufferControl OPTIONAL,
 * serviceState [2] ServiceState OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class TerminationStateDescriptor implements Descriptor {

    private final List<PropertyParm> propertyParms = new ArrayList<>();
    private EventBufferControl eventBufferControl; //optional
    private ServiceState serviceState; //optional

    public TerminationStateDescriptor() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 0);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (PropertyParm propertyParm : propertyParms) {
            propertyParm.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        if (eventBufferControl != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 1, eventBufferControl.value());
        }

        if (serviceState != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, serviceState.value());
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.decodePropertyParm(ais.readSequenceStream());
                    break;
                case 1:
                    eventBufferControl = EventBufferControl.getInstance((int) ais.readInteger());
                    break;
                case 2:
                    serviceState = ServiceState.getInstance((int) ais.readInteger());
                    break;
            }
        }
    }

    private void decodePropertyParm(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            PropertyParm propertyParm = new PropertyParm();
            propertyParm.decode(ais);
            propertyParms.add(propertyParm);
        }
    }

    public EventBufferControl getEventBufferControl() {
        return eventBufferControl;
    }

    public void setEventBufferControl(EventBufferControl eventBufferControl) {
        this.eventBufferControl = eventBufferControl;
    }

    public List<PropertyParm> getPropertyParms() {
        return propertyParms;
    }
    
    public void addPropertyParm(PropertyParm propertyParm){
        propertyParms.add(propertyParm);
    }

    public void setServiceState(ServiceState serviceState) {
        this.serviceState = serviceState;
    }

    public ServiceState getServiceState() {
        return serviceState;
    }

}
