/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.descriptor.error;

import com.linkedlogics.megaco.descriptor.Descriptor;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ErrorDescriptor ::= SEQUENCE
 * {
 * errorCode [0] ErrorCode,
 * errorText [1] ErrorText OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class ErrorDescriptor implements Descriptor {

    /**
     * Syntax error in message
     */
    public static final ErrorDescriptor ERROR_400 = new ErrorDescriptor(400, "Syntax error in message");
    /**
     * Protocol error
     */
    public static final ErrorDescriptor ERROR_401 = new ErrorDescriptor(401, "Protocol error");
    /**
     * Unauthorized
     */
    public static final ErrorDescriptor ERROR_402 = new ErrorDescriptor(402, "Unauthorized");
    /**
     * Syntax error in transaction request
     */
    public static final ErrorDescriptor ERROR_403 = new ErrorDescriptor(403, "Syntax error in transaction request");
    /**
     * Version not supported
     */
    public static final ErrorDescriptor ERROR_406 = new ErrorDescriptor(406, "Version not supported");
    /**
     * Incorrect identifier
     */
    public static final ErrorDescriptor ERROR_410 = new ErrorDescriptor(410, "Incorrect identifier");
    /**
     * The transaction refers to an unknown ContextId
     */
    public static final ErrorDescriptor ERROR_411 = new ErrorDescriptor(411, "The transaction refers to an unknown ContextId");

    private int errorCode;
    private String errorText;

    public ErrorDescriptor() {
    }

    public ErrorDescriptor(int errorCode, String errorText) {
        this.errorCode = errorCode;
        this.errorText = errorText;
    }

    public ErrorDescriptor(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, getErrorCode());
        if (getErrorText() != null) {
            aos.writeStringIA5(Tag.CLASS_CONTEXT_SPECIFIC, 1, getErrorText());
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        ais = ais.readSequenceStream();
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.errorCode = (int) ais.readInteger();
                    break;
                case 1:
                    this.errorText = ais.readIA5String();

            }
        }
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorText() {
        return errorText;
    }

}
