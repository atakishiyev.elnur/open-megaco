/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.exception;

/**
 *
 * @author eatakishiyev
 */
public class IncorrectParameterSetException extends Throwable {

    public IncorrectParameterSetException() {
    }

    public IncorrectParameterSetException(String message) {
        super(message);
    }
}
