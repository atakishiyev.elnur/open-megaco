/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.exception;

/**
 *
 * @author eatakishiyev
 */
public class ResourceUnavailableException extends Exception {

    public ResourceUnavailableException() {
        super();
    }

    public ResourceUnavailableException(String message) {
        super(message);
    }
}
