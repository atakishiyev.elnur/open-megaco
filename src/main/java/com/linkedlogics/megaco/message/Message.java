/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.message;

import com.linkedlogics.megaco.parameter.Decodable;
import com.linkedlogics.megaco.parameter.Encodable;
import com.linkedlogics.megaco.parameter.MessageBody;
import com.linkedlogics.megaco.parameter.Mid;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * Message ::= SEQUENCE
 * {
 * version [0] INTEGER(0..99),
 * -- The version of the protocol defined here is equal to 3.
 * mId [1] MID, -- Name/address of message originator
 *
 * messageBody [2] CHOICE
 * {
 * messageError [0] ErrorDescriptor,
 * transactions [1] SEQUENCE OF Transaction
 * },
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class Message implements Encodable, Decodable {

    private Integer version;
    private Mid mid;
    private MessageBody messageBody;

    public Message() {
    }

    /**
     *
     * @param version The version of the protocol defined here is equal to 3
     * @param mid Name/address of message originator
     * @param messageBody
     */
    public Message(Integer version, Mid mid, MessageBody messageBody) {
        this.version = version;
        this.mid = mid;
        this.messageBody = messageBody;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, version);

        mid.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);

        messageBody.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.version = (int) ais.readInteger();
                    break;
                case 1:
                    this.mid = new Mid();
                    mid.decode(ais.readSequenceStream());
                    break;
                case 2:
                    this.messageBody = new MessageBody();
                    messageBody.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

    public MessageBody getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(MessageBody messageBody) {
        this.messageBody = messageBody;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Mid getMid() {
        return mid;
    }

    public void setMid(Mid mid) {
        this.mid = mid;
    }

}
