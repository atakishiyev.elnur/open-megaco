/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages;

import com.linkedlogics.megaco.parameter.EventName;
import com.linkedlogics.megaco.parameter.ObservedEvent;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;

/**
 *
 * @author eatakishiyev
 */
public abstract class DtmfDetectionPackage implements Package {

    public static final int PCKG_ID = 0x0006;

    public static enum DtmfCharacter {
        D0(0x0010),
        D1(0x0011),
        D2(0x0012),
        D3(0x0013),
        D4(0x0014),
        D5(0x0015),
        D6(0x0016),
        D7(0x0017),
        D8(0x0018),
        D9(0x0019),
        DS(0x0020),
        DO(0x0021),
        DA(0x001A),
        DB(0x001B),
        DC(0x001C),
        DD(0x001D);

        private final int value;

        private DtmfCharacter(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
        
        public String getDtmfCharacter(){
                switch (value) {
                case 0x0010:
                    return "0";
                case 0x0011:
                    return "1";
                case 0x0012:
                    return "2";
                case 0x0013:
                    return "3";
                case 0x0014:
                    return "4";
                case 0x0015:
                    return "5";
                case 0x0016:
                    return "6";
                case 0x0017:
                    return "7";
                case 0x0018:
                    return "8";
                case 0x0019:
                    return "9";
                case 0x0020:
                    return "*";
                case 0x0021:
                    return "#";
                case 0x001A:
                    return "A";
                case 0x001B:
                    return "B";
                case 0x001C:
                    return "C";
                case 0x001D:
                    return "D";
                default:
                    return null;
            }
        }

        public static DtmfCharacter getInstance(String dtmfCharacter) {
            switch (dtmfCharacter) {
                case "0":
                    return D0;
                case "1":
                    return D1;
                case "2":
                    return D2;
                case "3":
                    return D3;
                case "4":
                    return D4;
                case "5":
                    return D5;
                case "6":
                    return D6;
                case "7":
                    return D7;
                case "8":
                    return D8;
                case "9":
                    return D9;
                case "*":
                    return DS;
                case "#":
                    return DO;
                case "A":
                    return DA;
                case "B":
                    return DB;
                case "C":
                    return DC;
                case "D":
                    return DD;
                default:
                    return null;
            }
        }

        public static DtmfCharacter getInstance(int value) {
            switch (value) {
                case 0x0010:
                    return D0;
                case 0x0011:
                    return D1;
                case 0x0012:
                    return D2;
                case 0x0013:
                    return D3;
                case 0x0014:
                    return D4;
                case 0x0015:
                    return D5;
                case 0x0016:
                    return D6;
                case 0x0017:
                    return D7;
                case 0x0018:
                    return D8;
                case 0x0019:
                    return D9;
                case 0x0020:
                    return DS;
                case 0x0021:
                    return DO;
                case 0x001A:
                    return DA;
                case 0x001B:
                    return DB;
                case 0x001C:
                    return DC;
                case 0x001D:
                    return DD;
                default:
                    return null;
            }
        }
    }

    public final ObservedEvent createDtmfDetectedEvent(String dtmfCharacter) throws IOException, AsnException {
        DtmfCharacter dtmfChar = DtmfCharacter.getInstance(dtmfCharacter);
        if (dtmfChar == null) {
            throw new IOException("Unknown dtmf character received: " + dtmfCharacter);
        }

        return new ObservedEvent(new EventName(PCKG_ID, dtmfChar.value));
    }

    @Override
    public int getPackageId() {
        return PCKG_ID;
    }

}
