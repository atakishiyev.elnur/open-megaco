/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages;

import com.linkedlogics.megaco.parameter.Name;
import com.linkedlogics.megaco.parameter.NotifyCompletion;
import com.linkedlogics.megaco.parameter.SigParameter;
import com.linkedlogics.megaco.parameter.Signal;
import com.linkedlogics.megaco.parameter.SignalName;
import com.linkedlogics.megaco.parameter.SignalType;
import com.linkedlogics.megaco.parameter.Value;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;

/**
 *
 * @author eatakishiyev
 */
public abstract class GenericAnnouncementPackage implements Package {

    public static final int PCKG_ID = 0x001d;
    public static final int SIGNAL_FIXED_ANNOUNCEMENT_PLAY = 0x0001;
    public static final int SIGNAL_VARIABLE_ANNOUNCEMENT_PLAY = 0x0002;
    public static final Name PARAMETER_ANNOUNCEMENT_NAME = new Name(new byte[]{0x00, 0x00, 0x00, 0x01});
    public static final Name PARAMETER_NUMBER_OF_CYCLES = new Name(new byte[]{0x00, 0x00, 0x00, 0x02});
    public static final Name PARAMETER_ANNOUNCEMENT_VARIANT = new Name(new byte[]{0x00, 0x00, 0x00, 0x03});
    public static final Name PARAMETER_ANNOUNCEMENT_DIRECTION = new Name(new byte[]{0x00, 0x00, 0x00, 0x04});

    @Override
    public int getPackageId() {
        return PCKG_ID;
    }

    public final Signal createAnnouncementSignal(int signalId, SignalType signalType,
            boolean notifyOnTimeOut, boolean notifyOnInterruptByEvent, boolean notifyOnInterruptByNewSignalDesc,
            boolean notifyOnOtherReason, boolean notifyOnIteration) throws IOException, AsnException {

        Signal signal = new Signal(new SignalName(PCKG_ID, SIGNAL_FIXED_ANNOUNCEMENT_PLAY));
        signal.setSignalType(signalType);

        Value announcementName = new Value();
        announcementName.addInteger(signalId);

        signal.getSigParList().add(new SigParameter(PARAMETER_ANNOUNCEMENT_NAME, announcementName));
        signal.setNotifyCompletion(new NotifyCompletion(notifyOnTimeOut, notifyOnInterruptByEvent,
                notifyOnInterruptByNewSignalDesc, notifyOnOtherReason, notifyOnIteration));

        return signal;
    }

}
