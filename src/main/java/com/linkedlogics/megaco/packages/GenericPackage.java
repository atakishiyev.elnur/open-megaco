/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages;

import com.linkedlogics.megaco.parameter.EventName;
import com.linkedlogics.megaco.parameter.EventParameter;
import com.linkedlogics.megaco.parameter.Name;
import com.linkedlogics.megaco.parameter.ObservedEvent;
import com.linkedlogics.megaco.parameter.Value;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;

/**
 *
 * @author eatakishiyev
 */
public abstract class GenericPackage implements Package {

    public final static int PCKG_ID = 0x0001;
    public final static int SIGNAL_COMPLETION_EVENT_ID = 0x0002;
    public static final Name SIGNAL_IDENTITY_PARAM_ID = new Name(new byte[]{0x00, 0x01});
    public static final Name TERMINATION_METHOD_PARAM_ID = new Name(new byte[]{0x00, 0x02});
    public static final Name SIGNAL_LIST_ID_PARAM_ID = new Name(new byte[]{0x00, 0x03});
    public static final Name REQUEST_ID_PARAM_ID = new Name(new byte[]{0x00, 0x04});

    @Override
    public int getPackageId() {
        return PCKG_ID;
    }

    public ObservedEvent createSignalCompletionEvent(int signalId, TerminationMethod terminationMethod,
            Integer signalListId, Integer requestId) throws IOException, AsnException {
        ObservedEvent observedEvent = new ObservedEvent(new EventName(PCKG_ID,
                SIGNAL_COMPLETION_EVENT_ID));

        Value signalIdentity = new Value();
        signalIdentity.addInteger(signalId);
        EventParameter signalIdentityParam = new EventParameter(SIGNAL_IDENTITY_PARAM_ID,
                signalIdentity);

        Value terminationMethodVal = new Value();
        terminationMethodVal.addInteger(terminationMethod.value);
        EventParameter terminationMethodParam = new EventParameter(TERMINATION_METHOD_PARAM_ID,
                terminationMethodVal);

        observedEvent.getEventParList().add(signalIdentityParam);
        observedEvent.getEventParList().add(terminationMethodParam);

        if (signalListId != null) {
            Value signalListIdVal = new Value();
            signalListIdVal.addInteger(signalListId);
            EventParameter signalListIdParam = new EventParameter(SIGNAL_LIST_ID_PARAM_ID,
                    signalListIdVal);
            observedEvent.getEventParList().add(signalListIdParam);
        }

        if (requestId != null) {
            Value requestIdVal = new Value();
            requestIdVal.addInteger(requestId);
            EventParameter requestIdParam = new EventParameter(REQUEST_ID_PARAM_ID,
                    requestIdVal);
            observedEvent.getEventParList().add(requestIdParam);
        }

        return observedEvent;
    }

    /**
     * "TO" (0x0001):Signal timed out or otherwise completed on
     * its own
     * "EV" (0x0002): Interrupted by event
     * "SD" (0x0003): Halted by new Signals Descriptor
     * "NC" (0x0004): Not completed, other cause
     * "PI" (0x0005): First to penultimate iteration. For last
     * iteration, use TO.
     */
    public enum TerminationMethod {
        TO(0x0001),
        EV(0x0002),
        SD(0x0003),
        NC(0x0004),
        PI(0x0005);
        private final int value;

        private TerminationMethod(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }
}
