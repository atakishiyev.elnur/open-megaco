/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages;

import com.linkedlogics.megaco.Context;
import com.linkedlogics.megaco.MegacoProvider;
import com.linkedlogics.megaco.Termination;
import com.linkedlogics.megaco.parameter.ObservedEvent;
import com.linkedlogics.megaco.parameter.Signal;
import com.linkedlogics.megaco.parameter.TransactionID;
import java.io.Serializable;

/**
 *
 * @author eatakishiyev
 */
public interface Package extends Serializable{

    public int getPackageId();

    public void handleSignal(MegacoProvider provider, TransactionID transactionID,
            Termination termination, Context context, Signal signal);

    public void handleEvent(MegacoProvider provider, TransactionID transactionID,
            Termination termination, Context context, ObservedEvent event);
}
