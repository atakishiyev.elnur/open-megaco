/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages.events;

import com.linkedlogics.megaco.descriptor.EventsDescriptor;
import com.linkedlogics.megaco.descriptor.ObservedEventsDescriptor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author eatakishiyev
 */
public class Cause implements Event {

    private final List<EventsDescriptor> eventsDescriptors = new ArrayList<>();
    private final List<ObservedEventsDescriptor> observedEventsDescriptors = new ArrayList<>();

    @Override
    public String getEventName() {
        return "Cause";
    }

    @Override
    public String getEventId() {
        return "cause";
    }

    @Override
    public int getId() {
        return 0x0001;
    }

    @Override
    public List<EventsDescriptor> getEventDescriptors() {
        return eventsDescriptors;
    }

    @Override
    public List<ObservedEventsDescriptor> getObservedEventsDescriptors() {
        return observedEventsDescriptors;
    }

}
