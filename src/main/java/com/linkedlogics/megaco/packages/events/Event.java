/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages.events;

import com.linkedlogics.megaco.descriptor.EventsDescriptor;
import com.linkedlogics.megaco.descriptor.ObservedEventsDescriptor;
import java.util.List;

/**
 *
 * @author eatakishiyev
 */
public interface Event {

    public String getEventName();

    public String getEventId();
    
    public int getId();

    public List<EventsDescriptor> getEventDescriptors();

    public List<ObservedEventsDescriptor> getObservedEventsDescriptors();
}
