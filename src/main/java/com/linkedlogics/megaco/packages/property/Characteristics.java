/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages.property;

/**
 *
 * @author eatakishiyev
 */
public enum Characteristics {
    READ_ONLY,
    READ_WRITE,
    GLOBAL;
}
