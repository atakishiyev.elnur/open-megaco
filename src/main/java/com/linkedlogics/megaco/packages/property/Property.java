/*
 * To change this license header, choose License Headers in Project Property.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages.property;

/**
 *
 * @author eatakishiyev
 */
public interface Property<T> {

    public String getPropertyName();

    public String getPropertyId();

    public T getDefaultValue();

    public Characteristics getCharacteristics();
}
