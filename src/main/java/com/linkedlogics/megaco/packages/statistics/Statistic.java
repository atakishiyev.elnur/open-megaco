/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.packages.statistics;

/**
 *
 * @author eatakishiyev
 */
public interface Statistic<T> {

    public String getStatisticName();

    public String getStatisticId();

    /**
     * Specify if the statistic can be kept at the termination level, Stream
     * level or either.
     *
     * @return
     */
    public int getLevel();

    /**
     * Specify if the statistic can be kept at the termination level, Stream
     * level or either.
     *
     * @param level
     */
    public void setLevel(int level);

}
