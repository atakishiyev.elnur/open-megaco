/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum AddressType {
    PORT_NUMBER(0),
    IP4ADDRESS(1),
    IP6ADDRESS(2),
    DOMAIN_NAME(3),
    PATH_NAME(4),
    MTP_ADDRESS(5),
    UNKNOWN(-1);

    private final int value;

    private AddressType(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }

    public static AddressType getInstance(int value) {
        switch (value) {
            case 0:
                return PORT_NUMBER;
            case 1:
                return IP4ADDRESS;
            case 2:
                return IP6ADDRESS;
            case 3:
                return DOMAIN_NAME;
            case 4:
                return PATH_NAME;
            case 5:
                return MTP_ADDRESS;
            default:
                return UNKNOWN;
        }
    }
}
