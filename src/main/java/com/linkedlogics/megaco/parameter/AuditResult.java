/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * AuditResult ::= SEQUENCE
 * {
 * terminationID [0] TerminationID,
 * terminationAuditResult [1] TerminationAudit
 * }
 *
 * @author eatakishiyev
 */
public class AuditResult implements Parameter {

    private TerminationID terminationID;
    private TerminationAudit terminationAuditResult;

    public AuditResult() {
    }

    public AuditResult(TerminationID terminationID, TerminationAudit terminationAuditResult) {
        this.terminationID = terminationID;
        this.terminationAuditResult = terminationAuditResult;
    }

    public TerminationAudit getTerminationAuditResult() {
        return terminationAuditResult;
    }

    public void setTerminationAuditResult(TerminationAudit terminationAuditResult) {
        this.terminationAuditResult = terminationAuditResult;
    }

    public TerminationID getTerminationID() {
        return terminationID;
    }

    public void setTerminationID(TerminationID terminationID) {
        this.terminationID = terminationID;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        terminationID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        terminationAuditResult.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationID = new TerminationID();
                    terminationID.decode(ais);
                    break;
                case 1:
                    this.terminationAuditResult = new TerminationAudit();
                    terminationAuditResult.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

}
