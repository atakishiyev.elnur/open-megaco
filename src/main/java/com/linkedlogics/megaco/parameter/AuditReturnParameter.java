/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.descriptor.MediaDescriptor;
import com.linkedlogics.megaco.descriptor.ObservedEventsDescriptor;
import com.linkedlogics.megaco.descriptor.DigitMapDescriptor;
import com.linkedlogics.megaco.descriptor.StatisticsDescriptor;
import com.linkedlogics.megaco.descriptor.EventsDescriptor;
import com.linkedlogics.megaco.descriptor.SignalsDescriptor;
import com.linkedlogics.megaco.descriptor.ModemDescriptor;
import com.linkedlogics.megaco.descriptor.EventBufferDescriptor;
import com.linkedlogics.megaco.descriptor.AuditDescriptor;
import com.linkedlogics.megaco.descriptor.Descriptor;
import com.linkedlogics.megaco.descriptor.PackagesDescriptor;
import com.linkedlogics.megaco.descriptor.MuxDescriptor;
import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * AuditReturnParameter ::= CHOICE
 * {
 * errorDescriptor [0] ErrorDescriptor,
 * mediaDescriptor [1] MediaDescriptor,
 * modemDescriptor [2] ModemDescriptor,
 * muxDescriptor [3] MuxDescriptor,
 * eventsDescriptor [4] EventsDescriptor,
 * eventBufferDescriptor [5] EventBufferDescriptor,
 * signalsDescriptor [6] SignalsDescriptor,
 * digitMapDescriptor [7] DigitMapDescriptor,
 * observedEventsDescriptor [8] ObservedEventsDescriptor,
 * statisticsDescriptor [9] StatisticsDescriptor,
 * packagesDescriptor [10] PackagesDescriptor,
 * emptyDescriptors [11] AuditDescriptor,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class AuditReturnParameter implements Parameter {

    private ErrorDescriptor errorDescriptor;
    private MediaDescriptor mediaDescriptor;
    private ModemDescriptor modemDescriptor;
    private MuxDescriptor muxDescriptor;
    private EventsDescriptor eventsDescriptor;
    private EventBufferDescriptor eventBufferDescriptor;
    private SignalsDescriptor signalsDescriptor;
    private DigitMapDescriptor digitMapDescriptor;
    private ObservedEventsDescriptor observedEventsDescriptor;
    private StatisticsDescriptor statisticsDescriptor;
    private PackagesDescriptor packagesDescriptor;
    private AuditDescriptor emptyDescriptors;

    public AuditReturnParameter() {

    }

    public AuditReturnParameter(Descriptor descriptor) {
        if (descriptor instanceof ErrorDescriptor) {
            errorDescriptor = (ErrorDescriptor) descriptor;
        } else if (descriptor instanceof MediaDescriptor) {
            mediaDescriptor = (MediaDescriptor) descriptor;
        } else if (descriptor instanceof ModemDescriptor) {
            modemDescriptor = (ModemDescriptor) descriptor;
        } else if (descriptor instanceof MuxDescriptor) {
            muxDescriptor = (MuxDescriptor) descriptor;
        } else if (descriptor instanceof EventsDescriptor) {
            eventsDescriptor = (EventsDescriptor) descriptor;
        } else if (descriptor instanceof EventBufferDescriptor) {
            eventBufferDescriptor = (EventBufferDescriptor) descriptor;
        } else if (descriptor instanceof SignalsDescriptor) {
            signalsDescriptor = (SignalsDescriptor) descriptor;
        } else if (descriptor instanceof DigitMapDescriptor) {
            digitMapDescriptor = (DigitMapDescriptor) descriptor;
        } else if (descriptor instanceof ObservedEventsDescriptor) {
            observedEventsDescriptor = (ObservedEventsDescriptor) descriptor;
        } else if (descriptor instanceof StatisticsDescriptor) {
            statisticsDescriptor = (StatisticsDescriptor) descriptor;
        } else if (descriptor instanceof PackagesDescriptor) {
            packagesDescriptor = (PackagesDescriptor) descriptor;
        } else if (descriptor instanceof AuditDescriptor) {
            emptyDescriptors = (AuditDescriptor) descriptor;
        }
    }

    public AuditReturnParameter(ErrorDescriptor errorDescriptor) {
        this.errorDescriptor = errorDescriptor;
    }

    public AuditReturnParameter(MediaDescriptor mediaDescriptor) {
        this.mediaDescriptor = mediaDescriptor;
    }

    public AuditReturnParameter(ModemDescriptor modemDescriptor) {
        this.modemDescriptor = modemDescriptor;
    }

    public AuditReturnParameter(MuxDescriptor muxDescriptor) {
        this.muxDescriptor = muxDescriptor;
    }

    public AuditReturnParameter(EventsDescriptor eventsDescriptor) {
        this.eventsDescriptor = eventsDescriptor;
    }

    public AuditReturnParameter(EventBufferDescriptor eventBufferDescriptor) {
        this.eventBufferDescriptor = eventBufferDescriptor;
    }

    public AuditReturnParameter(SignalsDescriptor signalsDescriptor) {
        this.signalsDescriptor = signalsDescriptor;
    }

    public AuditReturnParameter(DigitMapDescriptor digitMapDescriptor) {
        this.digitMapDescriptor = digitMapDescriptor;
    }

    public AuditReturnParameter(ObservedEventsDescriptor observedEventsDescriptor) {
        this.observedEventsDescriptor = observedEventsDescriptor;
    }

    public AuditReturnParameter(StatisticsDescriptor statisticsDescriptor) {
        this.statisticsDescriptor = statisticsDescriptor;
    }

    public AuditReturnParameter(PackagesDescriptor packagesDescriptor) {
        this.packagesDescriptor = packagesDescriptor;
    }

    public AuditReturnParameter(AuditDescriptor emptyDescriptors) {
        this.emptyDescriptors = emptyDescriptors;
    }

    public DigitMapDescriptor getDigitMapDescriptor() {
        return digitMapDescriptor;
    }

    public AuditDescriptor getEmptyDescriptors() {
        return emptyDescriptors;
    }

    public ErrorDescriptor getErrorDescriptor() {
        return errorDescriptor;
    }

    public EventBufferDescriptor getEventBufferDescriptor() {
        return eventBufferDescriptor;
    }

    public EventsDescriptor getEventsDescriptor() {
        return eventsDescriptor;
    }

    public MediaDescriptor getMediaDescriptor() {
        return mediaDescriptor;
    }

    public ModemDescriptor getModemDescriptor() {
        return modemDescriptor;
    }

    public MuxDescriptor getMuxDescriptor() {
        return muxDescriptor;
    }

    public ObservedEventsDescriptor getObservedEventsDescriptor() {
        return observedEventsDescriptor;
    }

    public PackagesDescriptor getPackagesDescriptor() {
        return packagesDescriptor;
    }

    public SignalsDescriptor getSignalsDescriptor() {
        return signalsDescriptor;
    }

    public StatisticsDescriptor getStatisticsDescriptor() {
        return statisticsDescriptor;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (errorDescriptor != null) {
            errorDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else if (mediaDescriptor != null) {
            mediaDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        } else if (modemDescriptor != null) {
            modemDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        } else if (muxDescriptor != null) {
            muxDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        } else if (eventsDescriptor != null) {
            eventsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
        } else if (eventBufferDescriptor != null) {
            eventBufferDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
        } else if (signalsDescriptor != null) {
            signalsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
        } else if (digitMapDescriptor != null) {
            digitMapDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
        } else if (observedEventsDescriptor != null) {
            observedEventsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 8, aos);
        } else if (statisticsDescriptor != null) {
            statisticsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 9, aos);
        } else if (packagesDescriptor != null) {
            packagesDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 10, aos);
        } else if (emptyDescriptors != null) {
            emptyDescriptors.encode(Tag.CLASS_CONTEXT_SPECIFIC, 11, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    public void encode(AsnOutputStream aos) throws AsnException, IOException {
        if (errorDescriptor != null) {
            errorDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else if (mediaDescriptor != null) {
            mediaDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        } else if (modemDescriptor != null) {
            modemDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        } else if (muxDescriptor != null) {
            muxDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        } else if (eventsDescriptor != null) {
            eventsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
        } else if (eventBufferDescriptor != null) {
            eventBufferDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
        } else if (signalsDescriptor != null) {
            signalsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
        } else if (digitMapDescriptor != null) {
            digitMapDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
        } else if (observedEventsDescriptor != null) {
            observedEventsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 8, aos);
        } else if (statisticsDescriptor != null) {
            statisticsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 9, aos);
        } else if (packagesDescriptor != null) {
            packagesDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 10, aos);
        } else if (emptyDescriptors != null) {
            emptyDescriptors.encode(Tag.CLASS_CONTEXT_SPECIFIC, 11, aos);
        }
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.errorDescriptor = new ErrorDescriptor();
                    errorDescriptor.decode(ais);
                    break;
                case 1:
                    this.mediaDescriptor = new MediaDescriptor();
                    mediaDescriptor.decode(ais.readSequenceStream());
                    break;
                case 2:
                    this.modemDescriptor = new ModemDescriptor();
                    modemDescriptor.decode(ais);
                    break;
                case 3:
                    this.muxDescriptor = new MuxDescriptor();
                    muxDescriptor.decode(ais);
                    break;
                case 4:
                    this.eventsDescriptor = new EventsDescriptor();
                    eventsDescriptor.decode(ais);
                    break;
                case 5:
                    this.eventBufferDescriptor = new EventBufferDescriptor();
                    eventBufferDescriptor.decode(ais);
                    break;
                case 6:
                    this.signalsDescriptor = new SignalsDescriptor();
                    signalsDescriptor.decode(ais.readSequenceStream());
                    break;
                case 7:
                    this.digitMapDescriptor = new DigitMapDescriptor();
                    digitMapDescriptor.decode(ais);
                    break;
                case 8:
                    this.observedEventsDescriptor = new ObservedEventsDescriptor();
                    observedEventsDescriptor.decode(ais);
                    break;
                case 9:
                    this.statisticsDescriptor = new StatisticsDescriptor();
                    statisticsDescriptor.decode(ais);
                    break;
                case 10:
                    this.packagesDescriptor = new PackagesDescriptor();
                    packagesDescriptor.decode(ais);
                    break;
                case 11:
                    this.emptyDescriptors = new AuditDescriptor();
                    emptyDescriptors.decode(ais);
                    break;

            }
        }
    }

}
