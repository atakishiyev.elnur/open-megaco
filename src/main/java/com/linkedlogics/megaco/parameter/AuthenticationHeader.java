/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * AuthenticationHeader ::= SEQUENCE {
 * secParmIndex [0] SecurityParmIndex
 * seqNum [1] SequenceNumber
 * ad [2] AuthData
 * }
 *
 * @author eatakishiyev
 */
public class AuthenticationHeader implements Parameter {

    private SecurityParmIndex secParmIndex;
    private SequenceNum seqNum;
    private AuthData ad;

    public AuthenticationHeader() {
    }

    public AuthenticationHeader(SecurityParmIndex secParmIndex, SequenceNum seqNum, AuthData ad) {
        this.secParmIndex = secParmIndex;
        this.seqNum = seqNum;
        this.ad = ad;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        secParmIndex.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        seqNum.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        ad.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            if (ais.getTagClass() != Tag.CLASS_CONTEXT_SPECIFIC) {
                throw new AsnException("Unexpected tag class received. Expecting ContextSpecific Class, received " + ais.getTagClass());
            }

            switch (tag) {
                case 0:
                    this.secParmIndex = new SecurityParmIndex();
                    this.secParmIndex.decode(ais);
                    break;
                case 1:
                    this.seqNum = new SequenceNum();
                    this.seqNum.decode(ais);
                    break;
                case 2:
                    this.ad = new AuthData();
                    this.ad.decode(ais);
                    break;
            }
        }
    }

    public AuthData getAd() {
        return ad;
    }

    public void setAd(AuthData ad) {
        this.ad = ad;
    }

    public SecurityParmIndex getSecParmIndex() {
        return secParmIndex;
    }

    public void setSecParmIndex(SecurityParmIndex secParmIndex) {
        this.secParmIndex = secParmIndex;
    }

    public SequenceNum getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(SequenceNum seqNum) {
        this.seqNum = seqNum;
    }

}
