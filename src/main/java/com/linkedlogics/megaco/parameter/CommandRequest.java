/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.command.request.AddRequest;
import com.linkedlogics.megaco.command.request.AuditCapRequest;
import com.linkedlogics.megaco.command.request.AuditValueRequest;
import com.linkedlogics.megaco.command.request.Command;
import com.linkedlogics.megaco.command.request.ModRequest;
import com.linkedlogics.megaco.command.request.MoveRequest;
import com.linkedlogics.megaco.command.request.NotifyRequest;
import com.linkedlogics.megaco.command.request.ServiceChangeRequest;
import com.linkedlogics.megaco.command.request.SubtractRequest;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * Command ::= SEQUENCE
 * {
 * command [0]Command,
 * optional [1] NULL OPTIONAL,
 * wildcardReturn [2] NULL OPTIONAL,
 * ...
 * }
 *
 *  * Command ::= CHOICE
 * {
 * addReq [0] AmmRequest,
 * moveReq [1] AmmRequest,
 * modReq [2] AmmRequest,
 * -- Add, Move, Modify requests have the same parameters
 * subtractReq [3] SubtractRequest,
 * auditCapRequest [4] AuditRequest,
 * auditValueRequest [5] AuditRequest,
 * notifyReq [6] NotifyRequest,
 * serviceChangeReq [7] ServiceChangeRequest,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class CommandRequest implements Parameter {

    private Command command;
    private NullType optional;
    private NullType wildcardReturn;

    public CommandRequest() {

    }

    public CommandRequest(Command command) {
        this.command = command;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 0);
        int lenPos1 = aos.StartContentDefiniteLength();
        switch (command.getRequestType()) {
            case ADD_REQ:
                command.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
                break;
            case MOVE_REQ:
                command.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
                break;
            case MOD_REQ:
                command.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
                break;
            case SUBTRACT_REQ:
                command.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
                break;
            case AUDIT_CAP_REQ:
                command.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
                break;
            case AUDIT_VAL_REQ:
                command.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
                break;
            case NOTIFY_REQ:
                command.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
                break;
            case SERVICE_CHANGE_REQ:
                command.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
                break;
        }
        aos.FinalizeContent(lenPos1);

        if (optional != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 1);
        }
        if (wildcardReturn != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 2);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.decodeCommand(ais.readSequenceStream());
                    break;
                case 1:
                    this.optional = new NullType();
                    ais.readNull();
                    break;
                case 2:
                    this.wildcardReturn = new NullType();
                    ais.readNull();
                    break;
            }
        }
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(com.linkedlogics.megaco.command.request.Command command) {
        this.command = command;
    }

    public boolean isOptional() {
        return optional != null;
    }

    public void setOptional(boolean optional) {
        this.optional = optional ? new NullType() : null;
    }

    public boolean isWildcardReturn() {
        return wildcardReturn != null;
    }

    public void setWildcardReturn(boolean wildcardReturn) {
        this.wildcardReturn = wildcardReturn ? new NullType() : null;
    }

    private void decodeCommand(AsnInputStream ais) throws IOException, AsnException {
        int tag = ais.readTag();
        switch (tag) {
            case 0:
                this.command = new AddRequest();
                command.decode(ais.readSequenceStream());
                break;
            case 1:
                this.command = new MoveRequest();
                command.decode(ais.readSequenceStream());
                break;
            case 2:
                this.command = new ModRequest();
                command.decode(ais.readSequenceStream());
                break;
            case 3:
                this.command = new SubtractRequest();
                command.decode(ais.readSequenceStream());
                break;
            case 4:
                this.command = new AuditCapRequest();
                command.decode(ais.readSequenceStream());
                break;
            case 5:
                this.command = new AuditValueRequest();
                command.decode(ais.readSequenceStream());
                break;
            case 6:
                this.command = new NotifyRequest();
                command.decode(ais.readSequenceStream());
                break;
            case 7:
                this.command = new ServiceChangeRequest();
                command.decode(ais.readSequenceStream());
                break;
        }
    }

}
