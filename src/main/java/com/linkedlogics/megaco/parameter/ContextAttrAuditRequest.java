/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 *
 * @author eatakishiyev
 */
public class ContextAttrAuditRequest implements Parameter {

    private NullType topology;
    private NullType emergency;
    private NullType priority;
    private NullType iepscallind;
    private final List<IndAudPropertyParm> contextPropAud = new ArrayList<>();
    private Integer selectpriority;
    private Boolean selectemergency;
    private Boolean selectiepscallind;
    //Choice
    private NullType andAuditSelect; //-- all filter conditions satisfied
    private NullType orAuditSelect; //-- at least one filter condition satisfied

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (topology != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 0);
        }
        if (emergency != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 1);
        }
        if (priority != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 2);
        }
        if (iepscallind != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 3);
        }
        if (contextPropAud.size() > 0) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 4);
            int lenPos1 = aos.StartContentDefiniteLength();
            for (IndAudPropertyParm _contextPropAud : contextPropAud) {
                _contextPropAud.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
            }
            aos.FinalizeContent(lenPos1);
        }
        if (selectpriority != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 5, selectpriority);
        }
        if (selectemergency != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 6, selectemergency);
        }
        if (selectiepscallind != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 7, selectiepscallind);
        }
        if (andAuditSelect != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 8);
        } else if (orAuditSelect != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 9);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void decodeContextPropAud(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            IndAudPropertyParm indAudPropertyParm = new IndAudPropertyParm();
            indAudPropertyParm.decode(ais);
            contextPropAud.add(indAudPropertyParm);
        }
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.topology = new NullType();
                    ais.readNull();
                    break;
                case 1:
                    this.emergency = new NullType();
                    ais.readNull();
                    break;
                case 2:
                    this.priority = new NullType();
                    ais.readNull();
                    break;
                case 3:
                    this.iepscallind = new NullType();
                    ais.readNull();
                    break;
                case 4:
                    this.decodeContextPropAud(ais.readSequenceStream());
                    break;
                case 5:
                    this.selectpriority = (int) ais.readInteger();
                    break;
                case 6:
                    this.selectemergency = ais.readBoolean();
                    break;
                case 7:
                    this.selectiepscallind = ais.readBoolean();
                    break;
                case 8:
                    this.andAuditSelect = new NullType();
                    ais.readNull();
                    break;
                case 9:
                    this.orAuditSelect = new NullType();
                    ais.readNull();
                    break;
            }
        }
    }

    public List<IndAudPropertyParm> getContextPropAud() {
        return contextPropAud;
    }

    public NullType getTopology() {
        return topology;
    }

    public void setTopology(NullType topology) {
        this.topology = topology;
    }

    public NullType getEmergency() {
        return emergency;
    }

    public void setEmergency(NullType emergency) {
        this.emergency = emergency;
    }

    public NullType getPriority() {
        return priority;
    }

    public void setPriority(NullType priority) {
        this.priority = priority;
    }

    public NullType getIepscallind() {
        return iepscallind;
    }

    public void setIepscallind(NullType iepscallind) {
        this.iepscallind = iepscallind;
    }

    public Boolean getSelectemergency() {
        return selectemergency;
    }

    public void setSelectemergency(Boolean selectemergency) {
        this.selectemergency = selectemergency;
    }

    public Boolean getSelectiepscallind() {
        return selectiepscallind;
    }

    public void setSelectiepscallind(Boolean selectiepscallind) {
        this.selectiepscallind = selectiepscallind;
    }

    public Integer getSelectpriority() {
        return selectpriority;
    }

    public void setSelectpriority(Integer selectpriority) {
        this.selectpriority = selectpriority;
    }

    public NullType getAndAuditSelect() {
        return andAuditSelect;
    }

    public void setAndAuditSelect(NullType andAuditSelect) {
        this.andAuditSelect = andAuditSelect;
        this.orAuditSelect = null;
    }

    public NullType getOrAuditSelect() {
        return orAuditSelect;
    }

    public void setOrAuditSelect(NullType orAuditSelect) {
        this.orAuditSelect = orAuditSelect;
        this.andAuditSelect = null;
    }

}
