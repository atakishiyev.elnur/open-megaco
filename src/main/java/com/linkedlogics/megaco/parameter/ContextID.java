/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 *
 * @author eatakishiyev
 */
public class ContextID implements Parameter {

    public static ContextID CONTEXT_ID_CHOOSE = new ContextID(0xFFFFFFFE);
    public static ContextID CONTEXT_ID_ALL = new ContextID(0xFFFFFFFF);

    private long value;

    public ContextID() {
    }

    public ContextID(long value) {
        this.value = value;
    }

    public void encode(AsnOutputStream aos) throws AsnException, IOException {
        this.encode(Tag.CLASS_UNIVERSAL, Tag.INTEGER, aos);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeInteger(tagClass, tag, value);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.value = ais.readInteger();
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ContextID) {
            return ((ContextID) obj).value == value;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.value ^ (this.value >>> 32));
        return hash;
    }

    @Override
    public String toString() {
        return "ContextID = " + value;
    }

}
