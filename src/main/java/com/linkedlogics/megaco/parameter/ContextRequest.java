/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ContextRequest ::= SEQUENCE
 * {
 * priority [0] INTEGER(0..15) OPTIONAL,
 * emergency [1] BOOLEAN OPTIONAL,
 * topologyReq [2] SEQUENCE OF TopologyRequest OPTIONAL,
 * ...,
 * iepscallind [3] BOOLEAN OPTIONAL,
 * contextProp [4] SEQUENCE OF PropertyParm OPTIONAL,
 * contextList [5] SEQUENCE OF ContextID OPTIONAL
 * }
 * -- When returning a contextList, the contextId in the ActionReply construct will
 * -- return the contextId from the associated ActionRequest
 *
 * @author eatakishiyev
 */
public class ContextRequest implements Parameter {

    private Integer priority = null;
    private Boolean emergency = null;
    private final List<TopologyRequest> topologyRequests = new ArrayList<>();
    private Boolean iepscallind = null;
    private final List<PropertyParm> contextProps = new ArrayList<>();
    private final List<ContextID> contextList = new ArrayList<>();

    public ContextRequest() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (priority != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, priority);
        }
        if (emergency != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 1, emergency);
        }

        if (topologyRequests.size() > 0) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 2);
            int lenPos1 = aos.StartContentDefiniteLength();
            for (TopologyRequest topologyRequest : topologyRequests) {
                topologyRequest.encode(aos);
            }
            aos.FinalizeContent(lenPos1);
        }

        if (iepscallind != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 3, iepscallind);
        }

        if (contextProps.size() > 0) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 4);
            int lenPos1 = aos.StartContentDefiniteLength();
            for (PropertyParm propertyParm : contextProps) {
                propertyParm.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
            }
            aos.FinalizeContent(lenPos1);
        }

        if (contextList.size() > 0) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 5);
            int lenPos1 = aos.StartContentDefiniteLength();
            for (ContextID contextID : contextList) {
                contextID.encode(aos);
            }
            aos.FinalizeContent(lenPos1);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.priority = (int) ais.readInteger();
                    break;
                case 1:
                    this.emergency = ais.readBoolean();
                    break;
                case 2:
                    this.decodeTopologyRequests(ais.readSequenceStream());
                    break;
                case 3:
                    this.iepscallind = ais.readBoolean();
                    break;
                case 4:
                    this.decodeContextProps(ais.readSequenceStream());
                    break;
                case 5:
                    this.decodeContextList(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeTopologyRequests(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            TopologyRequest topologyRequest = new TopologyRequest();
            topologyRequest.decode(ais);
            topologyRequests.add(topologyRequest);
        }
    }

    private void decodeContextProps(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            PropertyParm contextProp = new PropertyParm();
            contextProp.decode(ais);
            contextProps.add(contextProp);
        }
    }

    private void decodeContextList(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            ContextID contextID = new ContextID();
            contextID.decode(ais);
            contextList.add(contextID);
        }
    }

    public List<ContextID> getContextList() {
        return contextList;
    }

    public List<PropertyParm> getContextProps() {
        return contextProps;
    }

    public Boolean getIepscallind() {
        return iepscallind;
    }

    public void setIepscallind(Boolean iepscallind) {
        this.iepscallind = iepscallind;
    }

    public Boolean getEmergency() {
        return emergency;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setEmergency(Boolean emergency) {
        this.emergency = emergency;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<TopologyRequest> getTopologyRequests() {
        return topologyRequests;
    }
}
