/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * DigitMapValue ::= SEQUENCE
 * {
 * startTimer [0] INTEGER(0..99) OPTIONAL,
 * shortTimer [1] INTEGER(0..99) OPTIONAL,
 * longTimer [2] INTEGER(0..99) OPTIONAL,
 * digitMapBody [3] IA5String,
 * -- Units are seconds for start, short and long timers, and hundreds
 * -- of milliseconds for duration timer. Thus start, short, and long
 * -- range from 1 to 99 seconds and duration from 100 ms to 9.9 s
 * -- An exception is the start timer which may equal 0.
 * -- See A.3 for explanation of DigitMap syntax
 * ...,
 * durationTimer [4] INTEGER (0..99) OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class DigitMapValue implements EventDM, Parameter {

    private Integer startTimer;
    private Integer shortTimer;
    private Integer longTimer;
    private String digitMapBody;
    private Integer durationTimer;

    public DigitMapValue() {
    }

    public DigitMapValue(String digitMapBody) {
        this.digitMapBody = digitMapBody;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (startTimer != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, startTimer);
        }
        if (shortTimer != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 1, shortTimer);
        }
        if (longTimer != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, longTimer);
        }

        aos.writeStringIA5(Tag.CLASS_CONTEXT_SPECIFIC, 3, digitMapBody);

        if (durationTimer != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 4, durationTimer);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.startTimer = (int) ais.readInteger();
                    break;
                case 1:
                    this.shortTimer = (int) ais.readInteger();
                    break;
                case 2:
                    this.longTimer = (int) ais.readInteger();
                    break;
                case 3:
                    this.digitMapBody = ais.readIA5String();
                    break;
                case 4:
                    this.durationTimer = (int) ais.readInteger();
                    break;
            }
        }
    }

    public String getDigitMapBody() {
        return digitMapBody;
    }

    public void setDigitMapBody(String digitMapBody) {
        this.digitMapBody = digitMapBody;
    }

    public Integer getDurationTimer() {
        return durationTimer;
    }

    public void setDurationTimer(Integer durationTimer) {
        this.durationTimer = durationTimer;
    }

    public Integer getLongTimer() {
        return longTimer;
    }

    public void setLongTimer(Integer longTimer) {
        this.longTimer = longTimer;
    }

    public Integer getShortTimer() {
        return shortTimer;
    }

    public void setShortTimer(Integer shortTimer) {
        this.shortTimer = shortTimer;
    }

    public Integer getStartTimer() {
        return startTimer;
    }

    public void setStartTimer(Integer startTimer) {
        this.startTimer = startTimer;
    }

}
