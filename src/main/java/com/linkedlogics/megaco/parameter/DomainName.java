/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.Objects;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * DomainName ::= SEQUENCE
 * {
 * name [0] IA5String,
 * -- The name starts with an alphanumeric digit followed by a sequence
 * -- of alphanumeric digits, hyphens and dots. No two dots shall occur
 * -- consecutively.
 * portNumber [1] INTEGER(0..65535) OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class DomainName implements Parameter {

    private String name;
    private int portNumber = -1;

    public DomainName() {
    }

    public DomainName(String name, int portNumber) throws IOException {
        this.checkPortRange(portNumber);
        this.name = name;
        this.portNumber = portNumber;
    }

    public DomainName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DomainName:")
                .append(name);
        if (portNumber > 0) {
            sb.append("; PortNumber = ").append(portNumber);
        }
        return sb.toString();
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeStringIA5(Tag.CLASS_CONTEXT_SPECIFIC, 0, name);
        if (portNumber > 0) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 1, portNumber);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.name = ais.readIA5String();
                    break;
                case 1:
                    this.portNumber = (int) ais.readInteger();
                    checkPortRange(portNumber);
                    break;
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) throws IOException {
        checkPortRange(portNumber);
        this.portNumber = portNumber;
    }

    private void checkPortRange(int portNumber) throws IOException {
        if (portNumber < 0 || portNumber > 65535) {
            throw new IOException("PortNumber must be in range (0..65535)");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DomainName) {
            DomainName instance = (DomainName) obj;
            return name.equals(instance.name)
                    && ((portNumber > 0 || instance.portNumber > 0) ? (instance.portNumber == portNumber) : true);

        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + this.portNumber;
        return hash;
    }

}
