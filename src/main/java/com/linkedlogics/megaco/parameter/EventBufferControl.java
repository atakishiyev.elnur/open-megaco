/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum EventBufferControl {
    UNKNOWN(-1),
    OFF(0),
    LOCKSTEP(1);

    private final int value;

    private EventBufferControl(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

    public static EventBufferControl getInstance(int value) {
        switch (value) {
            case 0:
                return OFF;
            case 1:
                return LOCKSTEP;
            default:
                return UNKNOWN;
        }
    }

}
