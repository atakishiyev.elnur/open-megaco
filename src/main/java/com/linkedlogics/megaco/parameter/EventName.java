/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 * EventName ::= PkgdName
 *
 * @author eatakishiyev
 */
public class EventName extends PkgdName {

    private int eventName;

    public EventName() {
    }

    public EventName(int packageName, int eventName) {
        this.packageName = packageName;
        this.eventName = eventName;
    }

    @Override
    public int getObjectName() {
        return eventName;
    }

    @Override
    public void setObjectName(int objName) {
        this.eventName = objName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EventName) {
            EventName other = (EventName) obj;
            return other.eventName == this.eventName;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.eventName;
        return hash;
    }

    @Override
    public String toString() {
        return "EventID: " + eventName + " PackageID: " + getPackageName();
    }
}
