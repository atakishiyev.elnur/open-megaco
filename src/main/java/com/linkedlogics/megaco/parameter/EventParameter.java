/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * EventParameter ::= SEQUENCE
 * {
 * eventParameterName [0] Name,
 * value [1] Value,
 * -- For use of extraInfos see the comment related to PropertyParm
 * extraInfo CHOICE
 * {
 * relation [2] Relation,
 * range [3] BOOLEAN,
 * sublist [4] BOOLEAN
 * } OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class EventParameter implements Parameter {

    private Name eventParameterName;
    private Value value;
    //CHOICE
    private Relation relation;
    private Boolean range;
    private Boolean sublist;

    public EventParameter() {
    }

    public EventParameter(Name eventParameterName, Value value) {
        this.eventParameterName = eventParameterName;
        this.value = value;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        eventParameterName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        value.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        if (relation != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, relation.value());
        } else if (range != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 3, range);
        } else if (sublist != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 4, sublist
            );
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.eventParameterName = new Name();
                    eventParameterName.decode(ais);
                    break;
                case 1:
                    this.value = new Value();
                    value.decode(ais);
                    break;
                case 2:
                    this.relation = Relation.getInstance((int) ais.readInteger());
                    break;
                case 3:
                    this.range = ais.readBoolean();
                    break;
                case 4:
                    this.sublist = ais.readBoolean();
                    break;
            }
        }
    }

    public Name getEventParameterName() {
        return eventParameterName;
    }

    public void setEventParameterName(Name eventParameterName) {
        this.eventParameterName = eventParameterName;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Boolean getSublist() {
        return sublist;
    }

    public void setSublist(Boolean sublist) {
        this.sublist = sublist;
        range = null;
        relation = null;
    }

    public void setRange(Boolean range) {
        this.range = range;
        relation = null;
        sublist = null;
    }

    public Boolean getRange() {
        return range;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
        this.range = null;
        this.sublist = null;
    }

    public Relation getRelation() {
        return relation;
    }

}
