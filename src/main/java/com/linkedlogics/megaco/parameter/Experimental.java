/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * experimental IA5String(SIZE(8)),
 * -- first two characters should be "X-" or "X+"
 *
 * @author eatakishiyev
 */
public class Experimental implements NonStandardIdentifier {

    public static final int TAG = 2;
    private String experimental;

    public Experimental() {
    }

    public Experimental(String experimental) {
        this.experimental = experimental;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeStringIA5(tagClass, tag, experimental);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.experimental = ais.readIA5String();
    }

    public String getExperimental() {
        return experimental;
    }

    public void setExperimental(String experimental) {
        this.experimental = experimental;
    }

    @Override
    public int getTag() {
        return TAG;
    }

}
