/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * H221NonStandard
 * ::= SEQUENCE
 * {
 * t35CountryCode1 [0] INTEGER(0..255),
 * t35CountryCode2 [1] INTEGER(0..255), -- country, as per T.35
 * t35Extension [2] INTEGER(0..255), -- assigned nationally
 * manufacturerCode [3] INTEGER(0..65535), -- assigned nationally
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class H221NonStandard implements NonStandardIdentifier {

    public static final int TAG = 1;
    private int t35CountryCode1;
    private int t35CountryCode2;
    private int t35Extension;
    private int manufacturerCode;

    public H221NonStandard() {
    }

    public H221NonStandard(int t35CountryCode1, int t35CountryCode2, int t35Extension, int manufacturerCode) {
        this.t35CountryCode1 = t35CountryCode1;
        this.t35CountryCode2 = t35CountryCode2;
        this.t35Extension = t35Extension;
        this.manufacturerCode = manufacturerCode;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, t35CountryCode1);
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 1, t35CountryCode2);
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, t35Extension);
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 3, manufacturerCode);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.t35CountryCode1 = (int) ais.readInteger();
                    break;
                case 1:
                    this.t35CountryCode2 = (int) ais.readInteger();
                    break;
                case 2:
                    this.t35Extension = (int) ais.readInteger();
                    break;
                case 3:
                    this.manufacturerCode = (int) ais.readInteger();
                    break;
            }
        }
    }

    public int getManufacturerCode() {
        return manufacturerCode;
    }

    public void setManufacturerCode(int manufacturerCode) {
        this.manufacturerCode = manufacturerCode;
    }

    public int getT35CountryCode1() {
        return t35CountryCode1;
    }

    public void setT35CountryCode1(int t35CountryCode1) {
        this.t35CountryCode1 = t35CountryCode1;
    }

    public int getT35CountryCode2() {
        return t35CountryCode2;
    }

    public void setT35CountryCode2(int t35CountryCode2) {
        this.t35CountryCode2 = t35CountryCode2;
    }

    public int getT35Extension() {
        return t35Extension;
    }

    public void setT35Extension(int t35Extension) {
        this.t35Extension = t35Extension;
    }

    @Override
    public int getTag() {
        return TAG;
    }

}
