/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Objects;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IP4Address ::= SEQUENCE
 * {
 * address [0] OCTET STRING(SIZE(4)),
 * portNumber [1] INTEGER(0..65535) OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class IP4Address implements Parameter {

    private InetAddress address;
    private int portNumber = -1;

    public IP4Address() {
    }

    public IP4Address(InetAddress address, int portNumber) {
        this.address = address;
        this.portNumber = portNumber;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        aos.writeOctetString(Tag.CLASS_CONTEXT_SPECIFIC, 0, address.getAddress());
        if (portNumber >= 0) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 1, portNumber);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.address = Inet4Address.getByAddress(ais.readOctetString());
                    break;
                case 1:
                    this.portNumber = (int) ais.readInteger();
                    break;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("IP4Address:");
        sb.append("Address:").
                append(address.toString());
        if (portNumber > 0) {
            sb.append("; Port=").append(portNumber);
        }
        return sb.toString();
    }

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(Inet4Address address) {
        this.address = address;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IP4Address) {
            IP4Address instance = (IP4Address) obj;
            return address.equals(instance.address)
                    && ((portNumber > 0 || instance.portNumber > 0) ? (instance.portNumber == portNumber) : true);

        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.address);
        hash = 89 * hash + this.portNumber;
        return hash;
    }

}
