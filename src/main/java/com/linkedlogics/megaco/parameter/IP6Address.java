/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.Objects;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IP6Address ::= SEQUENCE{
 * address [0] OCTET STRING (SIZE(16)),
 * portNumber [1] INTEGER(0..65535) OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class IP6Address implements Parameter {

    private InetAddress address;
    private int portNumber = -1;

    public IP6Address() {
    }

    public IP6Address(InetAddress address, int portNumber) {
        this.address = address;
        this.portNumber = portNumber;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IP6Address:")
                .append(address);
        if (portNumber > 0) {
            sb.append("; PortNumber = ").append(portNumber);
        }
        return sb.toString();

    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        aos.writeOctetString(Tag.CLASS_CONTEXT_SPECIFIC, 0, address.getAddress());
        if (portNumber >= 0) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 1, portNumber);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.address = InetAddress.getByAddress(ais.readOctetString());
                    break;
                case 1:
                    this.portNumber = (int) ais.readInteger();
                    break;
            }
        }
    }

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(InetAddress address) {
        this.address = address;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IP6Address) {
            IP6Address instance = (IP6Address) obj;
            return address.equals(instance.address)
                    && ((portNumber > 0 || instance.portNumber > 0) ? (instance.portNumber == portNumber) : true);

        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.address);
        hash = 71 * hash + this.portNumber;
        return hash;
    }

}
