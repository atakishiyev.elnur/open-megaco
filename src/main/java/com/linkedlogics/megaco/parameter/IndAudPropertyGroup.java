/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudPropertyGroup ::= SEQUENCE OF IndAudPropertyParm
 *
 * @author eatakishiyev
 */
public class IndAudPropertyGroup implements Parameter {

    private final List<IndAudPropertyParm> audPropertyParms = new ArrayList<>();

    public IndAudPropertyGroup() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        for (IndAudPropertyParm indAudPropertyParm : audPropertyParms) {
            indAudPropertyParm.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException{
        while(ais.available() > 0){
            int tag = ais.readTag();
            IndAudPropertyParm indAudPropertyParm = new IndAudPropertyParm();
            indAudPropertyParm.decode(ais);
            audPropertyParms.add(indAudPropertyParm);
        }
    }

    public List<IndAudPropertyParm> getAudPropertyParms() {
        return audPropertyParms;
    }

}
