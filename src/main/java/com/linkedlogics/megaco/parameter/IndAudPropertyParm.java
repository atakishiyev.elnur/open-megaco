/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudPropertyParm ::= SEQUENCE
 * {
 * name [0] PkgdName,
 * ...,
 * propertyParms [1] PropertyParm OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class IndAudPropertyParm implements Parameter {

    private PropertyName name;
    private PropertyParm propertyParms;// Optional

    public IndAudPropertyParm() {
    }

    public IndAudPropertyParm(PropertyName name) {
        this.name = name;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        name.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (propertyParms != null) {
            propertyParms.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.name = new PropertyName();
                    name.decode(ais);
                    break;
                case 1:
                    this.propertyParms = new PropertyParm();
                    this.propertyParms.decode(ais);
                    break;
            }
        }
    }

    public PkgdName getName() {
        return name;
    }

    public void setName(PropertyName name) {
        this.name = name;
    }

    public PropertyParm getPropertyParms() {
        return propertyParms;
    }

    public void setPropertyParms(PropertyParm propertyParms) {
        this.propertyParms = propertyParms;
    }

}
