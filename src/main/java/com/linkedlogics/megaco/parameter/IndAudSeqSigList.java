/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudSeqSigList ::= SEQUENCE
 * {
 * id INTEGER(0..65535),
 * signalList IndAudSignal OPTIONAL
 * }
 *
 *
 *
 *
 * @author eatakishiyev
 */
public class IndAudSeqSigList implements Parameter {

    private Integer id;
    private IndAudSignal signalList;

    public IndAudSeqSigList() {
    }

    public IndAudSeqSigList(Integer id) {
        this.id = id;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, id);

        if (signalList != null) {
            signalList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.id = (int) ais.readInteger();
                    break;
                case 1:
                    this.signalList = new IndAudSignal();
                    signalList.decode(ais);
                    break;
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IndAudSignal getSignalList() {
        return signalList;
    }

    public void setSignalList(IndAudSignal signalList) {
        this.signalList = signalList;
    }

}
