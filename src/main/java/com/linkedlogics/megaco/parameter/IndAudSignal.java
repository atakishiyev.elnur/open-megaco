/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudSignal ::= SEQUENCE
 * {
 * signalName [0] PkgdName,
 * streamID [1] StreamID OPTIONAL,
 * ...,
 * signalRequestID [2] RequestID OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class IndAudSignal implements Parameter {

    private SignalName signalName;
    private StreamID streamID;
    private RequestID signalRequestID;

    public IndAudSignal() {
    }

    public IndAudSignal(SignalName signalName) {
        this.signalName = signalName;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        signalName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);

        if (streamID != null) {
            streamID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        if (signalRequestID != null) {
            signalRequestID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.signalName = new SignalName();
                    signalName.decode(ais);
                    break;
                case 1:
                    this.streamID = new StreamID();
                    streamID.decode(ais);
                    break;
                case 2:
                    this.signalRequestID = new RequestID();
                    signalRequestID.decode(ais);
                    break;
            }
        }
    }

    public PkgdName getSignalName() {
        return signalName;
    }

    public void setSignalName(SignalName signalName) {
        this.signalName = signalName;
    }

    public RequestID getSignalRequestID() {
        return signalRequestID;
    }

    public void setSignalRequestID(RequestID signalRequestID) {
        this.signalRequestID = signalRequestID;
    }

    public StreamID getStreamID() {
        return streamID;
    }

    public void setStreamID(StreamID streamID) {
        this.streamID = streamID;
    }

}
