/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.descriptor.Descriptor;
import com.linkedlogics.megaco.descriptor.IndAudLocalControlDescriptor;
import com.linkedlogics.megaco.descriptor.IndAudStatisticsDescriptor;
import com.linkedlogics.megaco.descriptor.IndAudLocalRemoteDescriptor;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAudStreamParms ::= SEQUENCE
 * {
 * localControlDescriptor [0] IndAudLocalControlDescriptor OPTIONAL,
 * localDescriptor [1] IndAudLocalRemoteDescriptor OPTIONAL,
 * remoteDescriptor [2] IndAudLocalRemoteDescriptor OPTIONAL,
 * ...,
 * statisticsDescriptor [3] IndAudStatisticsDescriptor OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class IndAudStreamParms implements Descriptor {

    private IndAudLocalControlDescriptor localControlDescriptor; //Optional
    private IndAudLocalRemoteDescriptor localDescriptor; //Optional
    private IndAudLocalRemoteDescriptor remoteDescriptor;// Optional
    private IndAudStatisticsDescriptor statisticsDescriptor; //Optional

    public IndAudStreamParms() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (localControlDescriptor != null) {
            localControlDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        if (localDescriptor != null) {
            localDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        if (remoteDescriptor != null) {
            remoteDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        if (statisticsDescriptor != null) {
            statisticsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
         while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.localControlDescriptor = new IndAudLocalControlDescriptor();
                    localControlDescriptor.decode(ais);
                    break;
                case 1:
                    this.localDescriptor = new IndAudLocalRemoteDescriptor();
                    localDescriptor.decode(ais);
                    break;
                case 2:
                    this.remoteDescriptor = new IndAudLocalRemoteDescriptor();
                    remoteDescriptor.decode(ais);
                    break;
                case 3:
                    this.statisticsDescriptor = new IndAudStatisticsDescriptor();
                    statisticsDescriptor.decode(ais);
                    break;
            }
        }
    }

    public IndAudLocalControlDescriptor getLocalControlDescriptor() {
        return localControlDescriptor;
    }

    public void setLocalControlDescriptor(IndAudLocalControlDescriptor localControlDescriptor) {
        this.localControlDescriptor = localControlDescriptor;
    }

    public IndAudLocalRemoteDescriptor getLocalDescriptor() {
        return localDescriptor;
    }

    public void setLocalDescriptor(IndAudLocalRemoteDescriptor localDescriptor) {
        this.localDescriptor = localDescriptor;
    }

    public IndAudLocalRemoteDescriptor getRemoteDescriptor() {
        return remoteDescriptor;
    }

    public void setRemoteDescriptor(IndAudLocalRemoteDescriptor remoteDescriptor) {
        this.remoteDescriptor = remoteDescriptor;
    }

    public IndAudStatisticsDescriptor getStatisticsDescriptor() {
        return statisticsDescriptor;
    }

    public void setStatisticsDescriptor(IndAudStatisticsDescriptor statisticsDescriptor) {
        this.statisticsDescriptor = statisticsDescriptor;
    }

}
