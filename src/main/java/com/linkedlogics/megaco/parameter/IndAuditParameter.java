/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.descriptor.IndAudDigitMapDescriptor;
import com.linkedlogics.megaco.descriptor.IndAudEventBufferDescriptor;
import com.linkedlogics.megaco.descriptor.IndAudEventsDescriptor;
import com.linkedlogics.megaco.descriptor.IndAudMediaDescriptor;
import com.linkedlogics.megaco.descriptor.IndAudPackagesDescriptor;
import com.linkedlogics.megaco.descriptor.IndAudSignalsDescriptor;
import com.linkedlogics.megaco.descriptor.IndAudStatisticsDescriptor;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * IndAuditParameter ::= CHOICE
 * {
 * indaudmediaDescriptor [0] IndAudMediaDescriptor,
 * indaudeventsDescriptor [1] IndAudEventsDescriptor,
 * indaudeventBufferDescriptor [2] IndAudEventBufferDescriptor,
 * indaudsignalsDescriptor [3] IndAudSignalsDescriptor,
 * indauddigitMapDescriptor [4] IndAudDigitMapDescriptor,
 * indaudstatisticsDescriptor [5] IndAudStatisticsDescriptor,
 * indaudpackagesDescriptor [6] IndAudPackagesDescriptor,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class IndAuditParameter implements Parameter {

    private IndAudMediaDescriptor indAudMediaDescriptor;
    private IndAudEventsDescriptor indAudEventsDescriptor;
    private IndAudEventBufferDescriptor indAudEventBufferDescriptor;
    private IndAudSignalsDescriptor indAudSignalsDescriptor;
    private IndAudDigitMapDescriptor indAudDigitMapDescriptor;
    private IndAudStatisticsDescriptor indAudStatisticsDescriptor;
    private IndAudPackagesDescriptor indAudPackagesDescriptor;

    public IndAuditParameter() {

    }

    public IndAuditParameter(IndAudMediaDescriptor indAudMediaDescriptor) {
        this.indAudMediaDescriptor = indAudMediaDescriptor;
    }

    public IndAuditParameter(IndAudEventsDescriptor indAudEventsDescriptor) {
        this.indAudEventsDescriptor = indAudEventsDescriptor;
    }

    public IndAuditParameter(IndAudEventBufferDescriptor indAudEventBufferDescriptor) {
        this.indAudEventBufferDescriptor = indAudEventBufferDescriptor;
    }

    public IndAuditParameter(IndAudSignalsDescriptor indAudSignalsDescriptor) {
        this.indAudSignalsDescriptor = indAudSignalsDescriptor;
    }

    public IndAuditParameter(IndAudDigitMapDescriptor indAudDigitMapDescriptor) {
        this.indAudDigitMapDescriptor = indAudDigitMapDescriptor;
    }

    public IndAuditParameter(IndAudStatisticsDescriptor indAudStatisticsDescriptor) {
        this.indAudStatisticsDescriptor = indAudStatisticsDescriptor;
    }

    public IndAuditParameter(IndAudPackagesDescriptor indAudPackagesDescriptor) {
        this.indAudPackagesDescriptor = indAudPackagesDescriptor;
    }

    public IndAudDigitMapDescriptor getIndAudDigitMapDescriptor() {
        return indAudDigitMapDescriptor;
    }

    public IndAudEventBufferDescriptor getIndAudEventBufferDescriptor() {
        return indAudEventBufferDescriptor;
    }

    public IndAudEventsDescriptor getIndAudEventsDescriptor() {
        return indAudEventsDescriptor;
    }

    public IndAudMediaDescriptor getIndAudMediaDescriptor() {
        return indAudMediaDescriptor;
    }

    public IndAudPackagesDescriptor getIndAudPackagesDescriptor() {
        return indAudPackagesDescriptor;
    }

    public IndAudSignalsDescriptor getIndAudSignalsDescriptor() {
        return indAudSignalsDescriptor;
    }

    public IndAudStatisticsDescriptor getIndAudStatisticsDescriptor() {
        return indAudStatisticsDescriptor;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (indAudMediaDescriptor != null) {
            indAudMediaDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else if (indAudEventsDescriptor != null) {
            indAudEventsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        } else if (indAudEventBufferDescriptor != null) {
            indAudEventBufferDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        } else if (indAudSignalsDescriptor != null) {
            indAudSignalsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        } else if (indAudDigitMapDescriptor != null) {
            indAudDigitMapDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
        } else if (indAudStatisticsDescriptor != null) {
            indAudStatisticsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
        } else if (indAudPackagesDescriptor != null) {
            indAudPackagesDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    public void encode(AsnOutputStream aos) throws AsnException, IOException {
        if (indAudMediaDescriptor != null) {
            indAudMediaDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else if (indAudEventsDescriptor != null) {
            indAudEventsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        } else if (indAudEventBufferDescriptor != null) {
            indAudEventBufferDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        } else if (indAudSignalsDescriptor != null) {
            indAudSignalsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        } else if (indAudDigitMapDescriptor != null) {
            indAudDigitMapDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
        } else if (indAudStatisticsDescriptor != null) {
            indAudStatisticsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
        } else if (indAudPackagesDescriptor != null) {
            indAudPackagesDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
        }
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.indAudMediaDescriptor = new IndAudMediaDescriptor();
                    indAudMediaDescriptor.decode(ais);
                    break;
                case 1:
                    this.indAudEventsDescriptor = new IndAudEventsDescriptor();
                    indAudEventsDescriptor.decode(ais);
                    break;
                case 2:
                    this.indAudEventBufferDescriptor = new IndAudEventBufferDescriptor();
                    indAudEventBufferDescriptor.decode(ais);
                    break;
                case 3:
                    this.indAudSignalsDescriptor = new IndAudSignalsDescriptor();
                    indAudSignalsDescriptor.decode(ais);
                    break;
                case 4:
                    this.indAudDigitMapDescriptor = new IndAudDigitMapDescriptor();
                    indAudDigitMapDescriptor.decode(ais);
                    break;
                case 5:
                    this.indAudStatisticsDescriptor = new IndAudStatisticsDescriptor();
                    indAudStatisticsDescriptor.decode(ais);
                    break;
                case 6:
                    this.indAudPackagesDescriptor = new IndAudPackagesDescriptor();
                    indAudPackagesDescriptor.decode(ais);
                    break;
            }
        }
    }

}
