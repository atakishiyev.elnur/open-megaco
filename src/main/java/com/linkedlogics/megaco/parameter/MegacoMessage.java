/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.message.Message;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * MegacoMessage ::= SEQUENCE {
 * authHeader AuthenticationHeader OPTIONAL,
 * mess Message
 * }
 *
 *
 * @author eatakishiyev
 */
public class MegacoMessage implements Encodable, Decodable {

    private AuthenticationHeader authHeader;
    private Message message;

    public MegacoMessage() {
    }

    public MegacoMessage(Message message) {
        this.message = message;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (authHeader != null) {
            authHeader.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        message.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        aos.FinalizeContent(lenPos);
    }

    public void encode(AsnOutputStream aos) throws AsnException, IOException {
        this.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
    }

    public byte[] encode() throws AsnException, IOException {
        AsnOutputStream aos = new AsnOutputStream();
        this.encode(aos);
        return aos.toByteArray();
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.authHeader = new AuthenticationHeader();
                    authHeader.decode(ais.readSequenceStream());
                    break;
                case 1:
                    this.message = new Message();
                    message.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

    public AuthenticationHeader getAuthHeader() {
        return authHeader;
    }

    public void setAuthHeader(AuthenticationHeader authHeader) {
        this.authHeader = authHeader;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

}
