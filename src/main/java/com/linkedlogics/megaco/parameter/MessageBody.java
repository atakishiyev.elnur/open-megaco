/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.transaction.SegmentReply;
import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import com.linkedlogics.megaco.transaction.Transaction;
import com.linkedlogics.megaco.transaction.TransactionPending;
import com.linkedlogics.megaco.transaction.TransactionReply;
import com.linkedlogics.megaco.transaction.TransactionRequest;
import com.linkedlogics.megaco.transaction.TransactionResponseAck;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * messageBody ::= CHOICE
 * {
 * messageError [0] ErrorDescriptor,
 * transactions [1] SEQUENCE OF Transaction
 * }
 *
 * @author eatakishiyev
 */
public class MessageBody implements Parameter {

    private ErrorDescriptor messageError;
    private final List<Transaction> transactions = new ArrayList<>();

    public MessageBody() {
    }

    public MessageBody(ErrorDescriptor messageError) {
        this.messageError = messageError;
    }

    public MessageBody(Transaction... transactions) {
        for (Transaction transaction : transactions) {
            this.transactions.add(transaction);
        }
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public ErrorDescriptor getMessageError() {
        return messageError;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (messageError != null) {
            messageError.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
            int lenPos1 = aos.StartContentDefiniteLength();

            for (Transaction transaction : transactions) {
                switch (transaction.getTransactionType()) {
                    case REQUEST:
                        transaction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
                        break;
                    case PENDING:
                        transaction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
                        break;
                    case REPLY:
                        transaction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
                        break;
                    case RESPONSE_ACK:
                        transaction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
                        break;
                    case SEGMENT_REPLY:
                        transaction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
                        break;
                }
            }

            aos.FinalizeContent(lenPos1);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        int tag = ais.readTag();
        switch (tag) {
            case 0:
                this.messageError = new ErrorDescriptor();
                messageError.decode(ais);
                break;
            case 1:
                this.decodeTransactions(ais.readSequenceStream());
                break;
        }
    }

    private void decodeTransactions(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            Transaction transaction = null;
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    transaction = new TransactionRequest();
                    break;
                case 1:
                    transaction = new TransactionPending();
                    break;
                case 2:
                    transaction = new TransactionReply();
                    break;
                case 3:
                    transaction = new TransactionResponseAck();
                    break;
                case 4:
                    transaction = new SegmentReply();
                    break;

            }

            if (transaction != null) {
                transaction.decode(ais.readSequenceStream());
                transactions.add(transaction);
            }
        }
    }

    public void addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
    }

}
