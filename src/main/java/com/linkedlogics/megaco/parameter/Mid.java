/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.MegacoProvider;
import java.io.IOException;
import java.util.Objects;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * Mid ::= CHOICE{
 * ip4Address [0] IP4Address,
 * ip6Address [1] IP6Address,
 * domainName [2] DomainName,
 * deviceName [3] PathName,
 * mtpAddress [4] OCTET STRING(SIZE(2..4)),
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class Mid implements Parameter {

    private IP4Address ip4Address;
    private IP6Address ip6Address;
    private DomainName domainName;
    private PathName deviceName;
    private MtpAddress mtpAddress;

    private AddressType addressType;

    public Mid() {
    }

    public Mid(IP4Address ip4Address) {
        this.ip4Address = ip4Address;
        this.addressType = AddressType.IP4ADDRESS;
    }

    public Mid(IP6Address ip6Address) {
        this.ip6Address = ip6Address;
        this.addressType = AddressType.IP6ADDRESS;
    }

    public Mid(DomainName domainName) {
        this.domainName = domainName;
        this.addressType = AddressType.DOMAIN_NAME;
    }

    public Mid(PathName deviceName) {
        this.deviceName = deviceName;
        this.addressType = AddressType.PATH_NAME;
    }

    public Mid(MtpAddress mtpAddress) {
        this.mtpAddress = mtpAddress;
        this.addressType = AddressType.MTP_ADDRESS;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Mid:[");
        if (ip4Address != null) {
            sb.append(ip4Address);
        } else if (ip6Address != null) {
            sb.append(ip6Address);
        } else if (domainName != null) {
            sb.append(domainName);
        } else if (deviceName != null) {
            sb.append(deviceName);
        } else if (mtpAddress != null) {
            sb.append(mtpAddress);
        }
        return sb.toString();
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        switch (addressType) {
            case IP4ADDRESS:
                ip4Address.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
                break;
            case IP6ADDRESS:
                ip6Address.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
                break;
            case DOMAIN_NAME:
                domainName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
                break;
            case PATH_NAME:
                deviceName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
                break;
            case MTP_ADDRESS:
                mtpAddress.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
                break;
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        int tag = ais.readTag();
        switch (tag) {
            case 0:
                this.ip4Address = new IP4Address();
                ip4Address.decode(ais);
                this.addressType = AddressType.IP4ADDRESS;
                break;
            case 1:
                this.ip6Address = new IP6Address();
                ip6Address.decode(ais);
                this.addressType = AddressType.IP6ADDRESS;
                break;
            case 2:
                this.domainName = new DomainName();
                domainName.decode(ais);
                this.addressType = AddressType.DOMAIN_NAME;
                break;
            case 3:
                this.deviceName = new PathName();
                deviceName.decode(ais);
                this.addressType = AddressType.PATH_NAME;
                break;
            case 4:
                this.mtpAddress = new MtpAddress();
                mtpAddress.decode(ais);
                this.addressType = AddressType.MTP_ADDRESS;
                break;
        }
    }

    public AddressType getAddressType() {
        return addressType;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Mid) {
            Mid mid = (Mid) obj;
            switch (mid.getAddressType()) {
                case DOMAIN_NAME:
                    return domainName.equals(mid.domainName);
                case IP4ADDRESS:
                    return ip4Address.equals(mid.ip4Address);
                case IP6ADDRESS:
                    return ip6Address.equals(mid.ip6Address);
                case MTP_ADDRESS:
                    return mtpAddress.equals(mid.mtpAddress);
                case PATH_NAME:
                    return deviceName.equals(mid.deviceName);
                default:
                    return false;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.ip4Address);
        hash = 73 * hash + Objects.hashCode(this.ip6Address);
        hash = 73 * hash + Objects.hashCode(this.domainName);
        hash = 73 * hash + Objects.hashCode(this.deviceName);
        hash = 73 * hash + Objects.hashCode(this.mtpAddress);
        return hash;
    }

}
