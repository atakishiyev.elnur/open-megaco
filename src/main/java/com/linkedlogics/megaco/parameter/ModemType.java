/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum ModemType {
    V_18(0),
    V_22(1),
    V_22bis(2),
    V_32(3),
    V_32bis(4),
    V_34(5),
    V_90(6),
    V_91(7),
    SYNCH_ISDN(8),
    UNKNOWN(-1);

    private final int value;

    private ModemType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

    public static ModemType getInstance(int value) {
        switch (value) {
            case 0:
                return V_18;
            case 1:
                return V_22;
            case 2:
                return V_22bis;
            case 3:
                return V_32;
            case 4:
                return V_32bis;
            case 5:
                return V_34;
            case 6:
                return V_90;
            case 7:
                return V_91;
            case 8:
                return SYNCH_ISDN;
            default:
                return UNKNOWN;
        }
    }
}
