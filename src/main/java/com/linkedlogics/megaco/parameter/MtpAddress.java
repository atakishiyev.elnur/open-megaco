/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.Arrays;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 *
 * @author eatakishiyev
 */
public class MtpAddress implements Parameter {

    private byte[] mtpAddress;

    public MtpAddress() {
    }

    public MtpAddress(byte[] mtpAddress) {
        this.mtpAddress = mtpAddress;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeOctetString(tagClass, tag, mtpAddress);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.mtpAddress = ais.readOctetString();
    }

    public byte[] getMtpAddress() {
        return mtpAddress;
    }

    public void setMtpAddress(byte[] mtpAddress) {
        this.mtpAddress = mtpAddress;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MtpAddress) {
            MtpAddress instance = (MtpAddress) obj;
            return Arrays.equals(instance.mtpAddress, mtpAddress);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.hashCode(this.mtpAddress);
        return hash;
    }

}
