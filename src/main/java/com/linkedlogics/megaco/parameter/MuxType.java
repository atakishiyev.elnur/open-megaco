/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum MuxType {
    UNKNOWN(-1),
    H221(0),
    H223(1),
    H226(2),
    V76(3),
    NX64K(4);
    private final int value;

    private MuxType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

    public static MuxType getInstance(int value) {
        switch (value) {
            case 0:
                return H221;
            case 1:
                return H223;
            case 2:
                return H226;
            case 3:
                return V76;
            case 4:
                return NX64K;
            default:
                return UNKNOWN;
        }
    }
}
