/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.Arrays;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 *
 * @author eatakishiyev
 */
public class Name implements Parameter {

    private byte[] value;

    public Name() {
    }

    public Name(byte[] value) {
        this.value = value;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeOctetString(tagClass, tag, value);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.value = ais.readOctetString();
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Name) {
            Name other = (Name) obj;
            return Arrays.equals(other.value, this.value);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Arrays.hashCode(this.value);
        return hash;
    }

}
