/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * NonStandardData
 * ::= SEQUENCE
 * {
 * nonStandardIdentifier [0] NonStandardIdentifier,
 * data [1] OCTET STRING
 * }
 *
 * @author eatakishiyev
 */
public class NonStandardData implements Parameter {

    private NonStandardIdentifier nonStandardIdentifier;
    private byte[] data;

    public NonStandardData() {
    }

    public NonStandardData(NonStandardIdentifier nonStandardIdentifier, byte[] data) {
        this.nonStandardIdentifier = nonStandardIdentifier;
        this.data = data;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        nonStandardIdentifier.encode(Tag.CLASS_CONTEXT_SPECIFIC, nonStandardIdentifier.getTag(), aos);
        aos.writeOctetString(Tag.CLASS_CONTEXT_SPECIFIC, 3, data);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case ObjectIdentifier.TAG:
                    this.nonStandardIdentifier = new ObjectIdentifier();
                    nonStandardIdentifier.decode(ais);
                    break;
                case H221NonStandard.TAG:
                    this.nonStandardIdentifier = new H221NonStandard();
                    nonStandardIdentifier.decode(ais);
                    break;
                case Experimental.TAG:
                    this.nonStandardIdentifier = new Experimental();
                    nonStandardIdentifier.decode(ais);
                    break;
                case 3:
                    data = ais.readOctetString();
                    break;
            }
        }
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public NonStandardIdentifier getNonStandardIdentifier() {
        return nonStandardIdentifier;
    }

    public void setNonStandardIdentifier(NonStandardIdentifier nonStandardIdentifier) {
        this.nonStandardIdentifier = nonStandardIdentifier;
    }

}
