/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.BitSetStrictLength;

/**
 * NotifyCompletion ::= BIT STRING
 * {
 * onTimeOut(0),
 * onInterruptByEvent(1),
 * onInterruptByNewSignalDescr(2),
 * otherReason(3),
 * onIteration(4)
 * }
 *
 * @author eatakishiyev
 */
public class NotifyCompletion implements Parameter {

    private BitSetStrictLength notifyCompletion = new BitSetStrictLength(5);

    public NotifyCompletion() {
    }

    public NotifyCompletion(boolean ontimeOut, boolean onInterruptByEvent, boolean onInterruptByNewSignalDesc, boolean onOtherReason, boolean onIteration) {
        this.setOnTimeOut(ontimeOut);
        this.setOnInterruptByEvent(onInterruptByEvent);
        this.setOnInterruptByNewSignalDescr(onInterruptByNewSignalDesc);
        this.setOtherReason(onOtherReason);
        this.setOnIteration(onIteration);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeBitString(tagClass, tag, notifyCompletion);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.notifyCompletion = ais.readBitString();
    }

    public final void setOnTimeOut(boolean value) {
        this.setBit(0, value);
    }

    public final boolean getOnTimeOut() {
        return getBit(0);
    }

    public final void setOnInterruptByEvent(boolean value) {
        this.setBit(1, value);
    }

    public final boolean getOnInterruptByEvent() {
        return this.getBit(1);
    }

    public final void setOnInterruptByNewSignalDescr(boolean value) {
        this.setBit(2, value);
    }

    public final boolean getOnInterruptByNewSignalDescr() {
        return this.getBit(2);
    }

    public final void setOtherReason(boolean value) {
        this.setBit(3, value);
    }

    public final boolean getOtherReason() {
        return this.getBit(3);
    }

    public final void setOnIteration(boolean value) {
        this.setBit(4, value);
    }

    public final boolean getOnIteration() {
        return this.getBit(4);
    }

    private void setBit(int index, boolean value) {
        notifyCompletion.set(index, value);
    }

    private boolean getBit(int index) {
        return notifyCompletion.get(index);
    }
}
