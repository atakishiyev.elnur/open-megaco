/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 *
 * @author eatakishiyev
 */
public class ObjectIdentifier implements NonStandardIdentifier {

    public static final int TAG = 0;
    private long[] oid;

    public ObjectIdentifier() {
    }

    public ObjectIdentifier(long[] oid) {
        this.oid = oid;
    }

    public long[] getOid() {
        return oid;
    }

    public void setOid(long[] oid) {
        this.oid = oid;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeObjectIdentifier(tagClass, tag, oid);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.oid = ais.readObjectIdentifier();
    }

    @Override
    public int getTag() {
        return TAG;
    }

}
