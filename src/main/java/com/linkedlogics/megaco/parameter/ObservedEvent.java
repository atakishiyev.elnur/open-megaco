/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ObservedEvent ::= SEQUENCE
 * {
 * eventName [0] EventName,
 * streamID [1] StreamID OPTIONAL,
 * eventParList [2] SEQUENCE OF EventParameter,
 * timeNotation [3] TimeNotation OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class ObservedEvent implements Parameter {

    private EventName eventName;
    private StreamID streamID;
    private final List<EventParameter> eventParList = new ArrayList<>();
    private TimeNotation timeNotation;

    public ObservedEvent() {
    }

    public ObservedEvent(EventName eventName) {
        this.eventName = eventName;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        eventName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (streamID != null) {
            streamID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 2);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (EventParameter eventParameter : eventParList) {
            eventParameter.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        if (timeNotation != null) {
            timeNotation.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.eventName = new EventName();
                    eventName.decode(ais);
                    break;
                case 1:
                    this.streamID = new StreamID();
                    streamID.decode(ais);
                    break;
                case 2:
                    this.decodeEventParameters(ais.readSequenceStream());
                    break;
                case 3:
                    this.timeNotation = new TimeNotation();
                    timeNotation.decode(ais);
                    break;
            }
        }
    }

    private void decodeEventParameters(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            EventParameter eventParameter = new EventParameter();
            eventParameter.decode(ais);
            eventParList.add(eventParameter);
        }
    }

    public EventName getEventName() {
        return eventName;
    }

    public void setEventName(EventName eventName) {
        this.eventName = eventName;
    }

    public List<EventParameter> getEventParList() {
        return eventParList;
    }

    public EventParameter getEventParameter(Name parameterName) {
        for (EventParameter eventParameter : eventParList) {
            if (eventParameter.getEventParameterName().equals(parameterName)) {
                return eventParameter;
            }
        }

        return null;
    }

    public void setStreamID(StreamID streamID) {
        this.streamID = streamID;
    }

    public StreamID getStreamID() {
        return streamID;
    }

    public void setTimeNotation(TimeNotation timeNotation) {
        this.timeNotation = timeNotation;
    }

    public TimeNotation getTimeNotation() {
        return timeNotation;
    }

}
