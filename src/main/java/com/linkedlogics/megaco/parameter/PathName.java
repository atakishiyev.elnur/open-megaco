/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.Objects;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * PathName ::= IA5String(SIZE (1..64))
 *
 * @author eatakishiyev
 */
public class PathName implements Parameter {

    private String pathName;

    public PathName() {
    }

    public PathName(String pathName) {
        this.pathName = pathName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PathName:").append(pathName);
        return sb.toString();
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeStringIA5(tagClass, tag, pathName);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.pathName = ais.readIA5String();
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PathName) {
            PathName instance = (PathName) obj;
            return pathName.equals(instance.pathName);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.pathName);
        return hash;
    }

}
