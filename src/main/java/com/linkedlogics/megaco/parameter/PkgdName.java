/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * PkgdName ::= OCTET STRING(SIZE(4))
 * -- represents Package Name (2 octets) plus property, event,
 * -- signal names or StatisticsID. (2 octets)
 * -- To wildcard a package use 0xFFFF for first two octets, CHOOSE
 * -- is not allowed. To reference native property tag specified in
 * -- Annex C, use 0x0000 as first two octets.
 * -- To wildcard a PropertyID, EventID, SignalID, or StatisticsID, use
 * -- 0xFFFF for last two octets, CHOOSE is not allowed.
 * -- Wildcarding of Package Name is permitted only if PropertyID,
 * -- EventID, SignalID, or StatisticsID are also wildcarded.
 *
 * @author eatakishiyev
 */
public abstract class PkgdName implements Parameter {

    int packageName;

    public PkgdName() {
    }

    private byte[] prepare() {
        byte[] data = new byte[4];
        data[0] = (byte) ((packageName >> 8) & 0xFF);
        data[1] = (byte) (packageName & 0xFF);

        data[2] = (byte) ((getObjectName() >> 8) & 0xFF);
        data[3] = (byte) (getObjectName() & 0xFF);
        return data;
    }

    public abstract int getObjectName();

    public abstract void setObjectName(int objName);

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws IOException, AsnException {
        aos.writeOctetString(tagClass, tag, prepare());
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        byte[] data = ais.readOctetString();
        packageName = data[0] & 0xFF;
        packageName = (packageName << 8) | (data[1]& 0xFF);

        int objName = data[2] & 0xFF;
        objName = (objName << 8) | (data[3] & 0xFF);

        setObjectName(objName);
    }

    public int getPackageName() {
        return packageName;
    }

}
