/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * PortNumber ::= INTEGER(0..65535) OPTIONAL
 *
 * @author eatakishiyev
 */
public class PortNumber implements Parameter {

    private int portNumber;

    public PortNumber() {
    }

    public PortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeInteger(tagClass, tag, portNumber);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.portNumber = (int) ais.readInteger();
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }
}
