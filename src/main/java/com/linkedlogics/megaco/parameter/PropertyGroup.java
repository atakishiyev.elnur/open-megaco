/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * PropertyGroup ::= SEQUENCE OF PropertyParm
 *
 * @author eatakishiyev
 */
public class PropertyGroup implements Parameter {

    private final List<PropertyParm> propertyParms = new ArrayList<>();

    public PropertyGroup() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        for (PropertyParm propertyParm : propertyParms) {
            propertyParm.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            PropertyParm propertyParm = new PropertyParm();
            propertyParm.decode(ais.readSequenceStream());
            propertyParms.add(propertyParm);
        }
    }

    public List<PropertyParm> getPropertyParms() {
        return propertyParms;
    }

    public void addPropertyParm(PropertyParm propertyParm) {
        propertyParms.add(propertyParm);
    }

}
