/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public class PropertyName extends PkgdName {

    private int propertyName;

    public PropertyName() {
    }

    public PropertyName(int packageName, int propertyName) {
        this.packageName = packageName;
        this.propertyName = propertyName;
    }

    @Override
    public int getObjectName() {
        return propertyName;
    }

    @Override
    public void setObjectName(int objName) {
        this.propertyName = objName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PropertyName) {
            PropertyName instance = (PropertyName) obj;
            return instance.packageName == packageName && instance.propertyName == propertyName;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.propertyName;
        hash = 79 * hash + this.packageName;
        return hash;
    }

}
