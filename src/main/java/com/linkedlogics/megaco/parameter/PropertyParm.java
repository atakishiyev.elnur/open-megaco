/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * PropertyParm ::= SEQUENCE
 * {
 * name [0] PkgdName,
 * values [1] SEQUENCE OF OCTET STRING,
 * extraInfo [2] CHOICE
 * {
 * relation [0] Relation,
 * range [1] BOOLEAN,
 * sublist [2]BOOLEAN
 * } OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class PropertyParm implements Parameter {

    private PropertyName propertyName;
    private final List<byte[]> values = new ArrayList<>();
    //extraInfo choice
    private Relation relation;//optional
    private Boolean range;//optional
    private Boolean sublist;//optional

    public PropertyParm() {
    }

    public PropertyParm(int packageId, int propertyName, byte[] values) {
        this.propertyName = new PropertyName(packageId, propertyName);
        this.values.add(values);
    }

    public PropertyParm(PropertyName propertyName, byte[] values) {
        this.propertyName = propertyName;
        this.values.add(values);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws IOException, AsnException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        propertyName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
        int lenPos1 = aos.StartContentDefiniteLength();

        for (byte[] val : values) {
            aos.writeOctetString(val);
        }   
        aos.FinalizeContent(lenPos1);

        if (relation != null) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 2);
            lenPos1 = aos.StartContentDefiniteLength();
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, relation.value());
            aos.FinalizeContent(lenPos1);
        } else if (range != null) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 2);
            lenPos1 = aos.StartContentDefiniteLength();
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 1, range);
            aos.FinalizeContent(lenPos1);
        } else if (sublist != null) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 2);
            lenPos1 = aos.StartContentDefiniteLength();
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 2, sublist);
            aos.FinalizeContent(lenPos1);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.propertyName = new PropertyName();
                    propertyName.decode(ais);
                    break;
                case 1:
                    this.decodeValue(ais.readSequenceStream());
                    break;
                case 2:
                    this.decodeExtraInfo(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeValue(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            byte[] data = ais.readOctetString();
            values.add(data);
        }
    }

    public PropertyName getName() {
        return propertyName;
    }

    public void setName(PropertyName propertyName) {
        this.propertyName = propertyName;
    }

    public List<byte[]> getValues() {
        return values;
    }

    public void addValue(byte[] value) {
        this.values.add(value);
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
        range = null;
        sublist = null;
    }

    public void setRange(Boolean range) {
        this.range = range;
        this.relation = null;
        this.sublist = null;
    }

    public void setSublist(Boolean sublist) {
        this.sublist = sublist;
        this.relation = null;
        this.range = null;
    }

    public Boolean getRange() {
        return range;
    }

    public Boolean getSublist() {
        return sublist;
    }

    private void decodeExtraInfo(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.relation = Relation.getInstance((int) ais.readInteger());
                    break;
                case 1:
                    this.range = ais.readBoolean();
                    break;
                case 2:
                    this.sublist = ais.readBoolean();
                    break;
            }
        }
    }

}
