/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum Relation {
    GREATER_THAN(0),
    SMALLER_THAN(1),
    UNEQUAL_TO(2),
    UNKNOWN(-1);

    private final int value;

    private Relation(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

    public static Relation getInstance(int value) {
        switch (value) {
            case 0:
                return GREATER_THAN;
            case 1:
                return SMALLER_THAN;
            case 2:
                return UNEQUAL_TO;
            default:
                return UNKNOWN;
        }
    }
}
