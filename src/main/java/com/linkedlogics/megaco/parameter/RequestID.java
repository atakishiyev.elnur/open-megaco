/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * RequestID ::= INTEGER(0..4294967295) -- 32-bit unsigned integer
 *
 * @author eatakishiyev
 */
public class RequestID implements Parameter {

    private long value;

    public RequestID() {
    }

    public RequestID(int value) {
        this.value = value;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeInteger(tagClass, tag, value);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.value = ais.readInteger();
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

}
