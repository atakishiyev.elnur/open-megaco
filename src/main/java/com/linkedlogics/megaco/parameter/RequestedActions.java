/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.descriptor.RegulatedEmbeddedDescriptor;
import com.linkedlogics.megaco.descriptor.SecondEventDescriptor;
import com.linkedlogics.megaco.descriptor.SignalsDescriptor;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * RequestedActions ::= SEQUENCE
 * {
 * keepActive [0] BOOLEAN OPTIONAL
 * eventDM [1] EventDM OPTIONAL
 * secondEvent [2] SecondEventsDescriptor OPTIONAL,
 * signalsDescriptor [3] SignalsDescriptor OPTIONAL,
 * ...,
 * notifyBehaviour NotifyBehaviour OPTIONAL,
 * resetEventsDescriptor [7] NULL OPTIONAL
 *
 * }
 *
 * @author eatakishiyev
 */
public class RequestedActions implements Parameter {

    private Boolean keepActive;
//    EventDM CHOICE
    private DigitMapName digitMapName;
    private DigitMapValue digitMapValue;
//    
    private SecondEventDescriptor secondEvent;
    private SignalsDescriptor signalsDescriptor;

//   NotifyBehaviour CHOICE
    private NullType notifyImmediate;
    private RegulatedEmbeddedDescriptor notifyRegulated;
    private NullType neverNotify;
//
    private NullType resetEventsDescriptor;

    public RequestedActions() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {

        int lenPos = aos.StartContentDefiniteLength();
        if (keepActive != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 0, keepActive);
        }
        if (digitMapName != null) {
            digitMapName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        } else if (digitMapValue != null) {
            digitMapValue.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        if (secondEvent != null) {
            secondEvent.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }

        if (signalsDescriptor != null) {
            signalsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
        }

        if (notifyImmediate != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 5);
        } else if (notifyRegulated != null) {
            notifyRegulated.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
        } else if (neverNotify != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 7);
        }

        if (resetEventsDescriptor != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 8);
        }
        aos.FinalizeContent(lenPos);
    }

        private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.keepActive = ais.readBoolean();
                    break;
                case 1:
                    this.digitMapName = new DigitMapName();
                    digitMapName.decode(ais);
                    break;
                case 2:
                    this.digitMapValue = new DigitMapValue();
                    digitMapValue.decode(ais);
                    break;
                case 3:
                    this.secondEvent = new SecondEventDescriptor();
                    secondEvent.decode(ais);
                    break;
                case 4:
                    this.signalsDescriptor = new SignalsDescriptor();
                    signalsDescriptor.decode(ais.readSequenceStream());
                    break;
                case 5:
                    this.notifyImmediate = new NullType();
                    ais.readNull();
                    break;
                case 6:
                    this.notifyRegulated = new RegulatedEmbeddedDescriptor();
                    notifyRegulated.decode(ais);
                    break;
                case 7:
                    this.neverNotify = new NullType();
                    ais.readNull();
                    break;
                case 8:
                    this.resetEventsDescriptor = new NullType();
                    ais.readNull();
                    break;
            }
        }
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    public DigitMapName getDigitMapName() {
        return digitMapName;
    }

    public Boolean getKeepActive() {
        return keepActive;
    }

    public void setKeepActive(Boolean keepActive) {
        this.keepActive = keepActive;
    }

    public NullType getResetEventsDescriptor() {
        return resetEventsDescriptor;
    }

    public SecondEventDescriptor getSecondEvent() {
        return secondEvent;
    }

    public void setResetEventsDescriptor(NullType resetEventsDescriptor) {
        this.resetEventsDescriptor = resetEventsDescriptor;
    }

    public SignalsDescriptor getSignalsDescriptor() {
        return signalsDescriptor;
    }

    public void setSecondEvent(SecondEventDescriptor secondEvent) {
        this.secondEvent = secondEvent;
    }

    public void setSignalsDescriptor(SignalsDescriptor signalsDescriptor) {
        this.signalsDescriptor = signalsDescriptor;
    }

    public void setDigitMapName(DigitMapName digitMapName) {
        this.digitMapName = digitMapName;
        this.digitMapValue = null;
    }

    public DigitMapValue getDigitMapValue() {
        return digitMapValue;
    }

    public void setDigitMapValue(DigitMapValue digitMapValue) {
        this.digitMapValue = digitMapValue;
        this.digitMapName = null;
    }

    public NullType getNotifyImmediate() {
        return notifyImmediate;
    }

    public void setNotifyImmediate() {
        this.notifyImmediate = new NullType();
        this.notifyRegulated = null;
        this.neverNotify = null;
    }

    public RegulatedEmbeddedDescriptor getNotifyRegulated() {
        return notifyRegulated;
    }

    public void setNotifyRegulated(RegulatedEmbeddedDescriptor notifyRegulated) {
        this.notifyRegulated = notifyRegulated;
        notifyImmediate = null;
        neverNotify = null;
    }

    public NullType getNeverNotify() {
        return neverNotify;
    }

    public void setNeverNotify() {
        this.neverNotify = new NullType();
        this.notifyImmediate = null;
        this.neverNotify = null;
    }

}
