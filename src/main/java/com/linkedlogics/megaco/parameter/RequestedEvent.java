/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * RequestedEvent ::= SEQUENCE
 * {
 * pkgdName [0] PkgdName,
 * streamID [1] StreamID OPTIONAL,
 * eventAction [2] RequestedActions OPTIONAL,
 * evParList [3] SEQUENCE OF EventParameter
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class RequestedEvent implements Parameter {

    private EventName eventName;
    private StreamID streamId;
    private RequestedActions eventAction;
    private final List<EventParameter> evParList = new ArrayList<>();

    public RequestedEvent() {
    }

    public RequestedEvent(EventName eventName) {
        this.eventName = eventName;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        eventName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (streamId != null) {
            streamId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
        if (eventAction != null) {
            eventAction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 3);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (EventParameter eventParameter : evParList) {
            eventParameter.encode(Tag.CLASS_CONTEXT_SPECIFIC, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.eventName = new EventName();
                    eventName.decode(ais);
                    break;
                case 1:
                    this.streamId = new StreamID();
                    streamId.decode(ais);
                    break;
                case 2:
                    this.eventAction = new RequestedActions();
                    eventAction.decode(ais);
                    break;
                case 3:
                    this.decodeEventParameters(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeEventParameters(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            EventParameter eventParameter = new EventParameter();
            eventParameter.decode(ais);
            evParList.add(eventParameter);
        }
    }

    public List<EventParameter> getEvParList() {
        return evParList;
    }

    public void setEventAction(RequestedActions eventAction) {
        this.eventAction = eventAction;
    }

    public RequestedActions getEventAction() {
        return eventAction;
    }

    public void setEventName(EventName eventName) {
        this.eventName = eventName;
    }

    public EventName getEventName() {
        return eventName;
    }

    public void setStreamId(StreamID streamId) {
        this.streamId = streamId;
    }

    public StreamID getStreamId() {
        return streamId;
    }

}
