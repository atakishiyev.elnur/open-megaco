/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.descriptor.RegulatedEmbeddedDescriptor;
import com.linkedlogics.megaco.descriptor.SignalsDescriptor;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * SecondRequestedActions ::= SEQUENCE
 * {
 * keepActive BOOLEAN OPTIONAL,
 * eventDM EventDM OPTIONAL,
 * signalsDescriptor SignalsDescriptor OPTIONAL,
 * ...,
 * notifyBehaviour NotifyBehaviour OPTIONAL,
 * resetEventsDescriptor NULL OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class SecondRequestedActions implements Parameter {

    private Boolean keepActive; // 0
//    EventDM CHOICE
    private DigitMapName digitMapName; // 1
    private DigitMapValue digitMapValue;// 2
//    
    private SignalsDescriptor signalsDescriptor; // 3
//    NotifyBehaviour CHOICE 
    private NullType notifyImmediate; // 4
    private RegulatedEmbeddedDescriptor notifyRegulated; // 5
    private NullType neverNotify; // 6
//    
    private NullType resetEventDescriptor; // 7

    public SecondRequestedActions() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (keepActive != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 0, keepActive);
        }

        if (digitMapName != null) {
            digitMapName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        } else if (digitMapValue != null) {
            digitMapValue.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        if (signalsDescriptor != null) {
            signalsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }

        if (notifyImmediate != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 4);
        } else if (notifyRegulated != null) {
            notifyRegulated.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
        } else if (neverNotify != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 6);
        }

        if (resetEventDescriptor != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 7);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.keepActive = ais.readBoolean();
                    break;
                case 1:
                    this.digitMapName = new DigitMapName();
                    digitMapName.decode(ais);
                    break;
                case 2:
                    this.digitMapValue = new DigitMapValue();
                    digitMapValue.decode(ais);
                    break;
                case 3:
                    this.signalsDescriptor = new SignalsDescriptor();
                    signalsDescriptor.decode(ais.readSequenceStream());
                    break;
                case 4:
                    this.notifyImmediate = new NullType();
                    ais.readNull();
                    break;
                case 5:
                    this.notifyRegulated = new RegulatedEmbeddedDescriptor();
                    notifyRegulated.decode(ais);
                    break;
                case 6:
                    this.neverNotify = new NullType();
                    ais.readNull();
                    break;
                case 7:
                    this.resetEventDescriptor = new NullType();
                    ais.readNull();
                    break;

            }
        }
    }

    public void setNeverNotify() {
        this.neverNotify = new NullType();
        notifyImmediate = null;
        notifyRegulated = null;
    }

    public void setResetEventDescriptor(NullType resetEventDescriptor) {
        this.resetEventDescriptor = resetEventDescriptor;
    }

    public NullType getResetEventDescriptor() {
        return resetEventDescriptor;
    }

    public NullType getNeverNotify() {
        return neverNotify;
    }

    public void setNotifyRegulated(RegulatedEmbeddedDescriptor notifyRegulated) {
        this.notifyRegulated = notifyRegulated;
        notifyImmediate = null;
        neverNotify = null;
    }

    public RegulatedEmbeddedDescriptor getNotifyRegulated() {
        return notifyRegulated;
    }

    public void setNotifyImmediate() {
        this.notifyImmediate = new NullType();
        notifyRegulated = null;
        notifyImmediate = null;
    }

    public NullType getNotifyImmediate() {
        return notifyImmediate;
    }

    public Boolean getKeepActive() {
        return keepActive;
    }

    public void setKeepActive(Boolean keepActive) {
        this.keepActive = keepActive;
    }

    public DigitMapName getDigitMapName() {
        return digitMapName;
    }

    public void setDigitMapName(DigitMapName digitMapName) {
        this.digitMapName = digitMapName;
        this.digitMapValue = null;
    }

    public DigitMapValue getDigitMapValue() {
        return digitMapValue;
    }

    public void setDigitMapValue(DigitMapValue digitMapValue) {
        this.digitMapValue = digitMapValue;
        this.digitMapName = null;
    }

    public SignalsDescriptor getSignalsDescriptor() {
        return signalsDescriptor;
    }

    public void setSignalsDescriptor(SignalsDescriptor signalsDescriptor) {
        this.signalsDescriptor = signalsDescriptor;
    }

}
