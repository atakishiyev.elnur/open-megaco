/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * SecondRequestedEvent ::= SEQUENCE
 * {
 * pkgdName [0] PkgdName,
 * streamID [1] StreamID OPTIONAL,
 * eventAction [2] SecondRequestedActions OPTIONAL,
 * evParList [3] SEQUENCE OF EventParameter,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class SecondRequestedEvent implements Parameter {

    private EventName eventName;
    private StreamID streamID;
    private SecondRequestedActions eventAction;
    private final List<EventParameter> evParList = new ArrayList<>();

    public SecondRequestedEvent() {
    }

    public SecondRequestedEvent(EventName eventName) {
        this.eventName = eventName;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        eventName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (streamID != null) {
            streamID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
        if (eventAction != null) {
            eventAction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 3);
        int lenPos2 = aos.StartContentDefiniteLength();
        for (EventParameter eventParameter : evParList) {
            eventParameter.encode(Tag.CLASS_CONTEXT_SPECIFIC, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos2);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.eventName = new EventName();
                    eventName.decode(ais);
                    break;
                case 1:
                    this.streamID = new StreamID();
                    streamID.decode(ais);
                    break;
                case 2:
                    this.eventAction = new SecondRequestedActions();
                    eventAction.decode(ais);
                    break;
                case 3:
                    this.decodeEvParList(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeEvParList(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            EventParameter evPar = new EventParameter();
            evPar.decode(ais);
            evParList.add(evPar);
        }
    }

    public List<EventParameter> getEvParList() {
        return evParList;
    }

    public void setEventAction(SecondRequestedActions eventAction) {
        this.eventAction = eventAction;
    }

    public SecondRequestedActions getEventAction() {
        return eventAction;
    }

    public void setEventName(EventName eventName) {
        this.eventName = eventName;
    }

    public EventName getEventName() {
        return eventName;
    }

    public void setStreamID(StreamID streamID) {
        this.streamID = streamID;
    }

    public StreamID getStreamID() {
        return streamID;
    }

}
