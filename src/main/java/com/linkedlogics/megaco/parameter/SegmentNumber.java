/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * SegmentNumber ::= INTEGER(0..65535)
 *
 * @author eatakishiyev
 */
public class SegmentNumber implements Parameter {

    private int segmentNumber;

    public SegmentNumber() {
    }

    public SegmentNumber(int segmentNumber) {
        this.segmentNumber = segmentNumber;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeInteger(tagClass, tag, segmentNumber);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.segmentNumber = (int) ais.readInteger();
    }

    public int getSegmentNumber() {
        return segmentNumber;
    }

    public void setSegmentNumber(int segmentNumber) {
        this.segmentNumber = segmentNumber;
    }

}
