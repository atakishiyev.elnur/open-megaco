/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * SeqSigList ::= SEQUENCE {
 * id [0] INTEGER(0..65535),
 * signalList [1] SEQUENCE OF Signal
 * }
 *
 * @author eatakishiyev
 */
public class SeqSigList implements Parameter {

    private Integer id;
    private final List<Signal> signalList = new ArrayList<>();

    public SeqSigList() {
    }

    public SeqSigList(Integer id) {
        this.id = id;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, id);

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (Signal signal : signalList) {
            signal.encode(Tag.CLASS_CONTEXT_SPECIFIC, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.id = (int) ais.readInteger();
                    break;
                case 1:
                    this.decodeSignalList(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeSignalList(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            Signal signal = new Signal();
            signal.decode(ais.readSequenceStream());
            signalList.add(signal);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Signal> getSignalList() {
        return signalList;
    }

}
