/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * SequenceNum ::= OCTET STRING(SIZE(4));
 *
 * @author eatakishiyev
 */
public class SequenceNum implements Parameter {

    private byte[] data;

    public SequenceNum() {
    }

    public SequenceNum(byte[] data) {
        checkLength(data);
        this.data = data;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws IOException, AsnException {
        aos.writeOctetString(tagClass, tag, data);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        data = ais.readOctetString();
        checkLength(data);
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        checkLength(data);
        this.data = data;
    }

    private void checkLength(byte[] data) {
        if (data.length != 4) {
            throw new ArrayIndexOutOfBoundsException("SequenceNum ::= OCTET STRING(SIZE(4)). Data should be 4 byte");
        }
    }
}
