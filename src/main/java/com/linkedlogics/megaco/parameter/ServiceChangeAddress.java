/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ServiceChangeAddress :: = CHOICE{
 * {
 * portNumber [0] INTEGER(0..65535), -- TCP/UDP port number
 * ip4Address [1] IP4Address,
 * ip6Address [2] IP6Address,
 * domainName [3] DomainName,
 * deviceName [4] PathName,
 * mtpAddress [5] OCTET STRING(SIZE(2..4)),
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class ServiceChangeAddress implements Parameter {

    private Integer portNumber;
    private IP4Address ip4Address;
    private IP6Address ip6Address;
    private DomainName domainName;
    private PathName deviceName;
    private MtpAddress mtpAddress;

    public ServiceChangeAddress() {
    }

    public ServiceChangeAddress(Integer portNumber) {
        this.portNumber = portNumber;
        this.addressType = AddressType.PORT_NUMBER;
    }

    public ServiceChangeAddress(IP4Address ip4Address) {
        this.ip4Address = ip4Address;
        this.addressType = AddressType.IP4ADDRESS;
    }

    public ServiceChangeAddress(IP6Address ip6Address) {
        this.ip6Address = ip6Address;
        this.addressType = AddressType.IP6ADDRESS;
    }

    public ServiceChangeAddress(DomainName domainName) {
        this.domainName = domainName;
        this.addressType = AddressType.DOMAIN_NAME;
    }

    public ServiceChangeAddress(PathName deviceName) {
        this.deviceName = deviceName;
        this.addressType = AddressType.PATH_NAME;
    }

    public ServiceChangeAddress(MtpAddress mtpAddress) {
        this.mtpAddress = mtpAddress;
        this.addressType = AddressType.MTP_ADDRESS;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        switch (addressType) {
            case PORT_NUMBER:
                aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, portNumber);
                break;
            case IP4ADDRESS:
                ip4Address.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
                break;
            case IP6ADDRESS:
                ip6Address.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
                break;
            case DOMAIN_NAME:
                domainName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
                break;
            case PATH_NAME:
                deviceName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
                break;
            case MTP_ADDRESS:
                mtpAddress.encode(Tag.CLASS_CONTEXT_SPECIFIC, 5, aos);
                break;
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.portNumber = (int) ais.readInteger();
                    break;
                case 1:
                    this.ip4Address = new IP4Address();
                    ip4Address.decode(ais);
                    break;
                case 2:
                    this.ip6Address = new IP6Address();
                    ip6Address.decode(ais);
                    break;
                case 3:
                    this.domainName = new DomainName();
                    domainName.decode(ais);
                    break;
                case 4:
                    this.deviceName = new PathName();
                    deviceName.decode(ais);
                    break;
                case 5:
                    this.mtpAddress = new MtpAddress();
                    mtpAddress.decode(ais);
                    break;
            }
        }
    }

    public Integer getPortNumber() {
        return portNumber;
    }

    public IP4Address getIp4Address() {
        return ip4Address;
    }

    public PathName getDeviceName() {
        return deviceName;
    }

    public DomainName getDomainName() {
        return domainName;
    }

    public IP6Address getIp6Address() {
        return ip6Address;
    }

    public MtpAddress getMtpAddress() {
        return mtpAddress;
    }

    private AddressType addressType;

    public AddressType getType() {
        return addressType;
    }
}
