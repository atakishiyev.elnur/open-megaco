/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum ServiceChangeMethod {
    FAILOVER(0),
    FORCED(1),
    GRACEFUL(2),
    RESTART(3),
    DISCONNECTED(4),
    HANDOFF(5),
    UNKNOWN(-1);

    private final int value;

    private ServiceChangeMethod(int value) {
        this.value = value;
    }

    public static ServiceChangeMethod getInstance(int value) {
        switch (value) {
            case 0:
                return FAILOVER;
            case 1:
                return FORCED;
            case 2:
                return GRACEFUL;
            case 3:
                return RESTART;
            case 4:
                return DISCONNECTED;
            case 5:
                return HANDOFF;
            default:
                return UNKNOWN;
        }
    }

    public int value() {
        return this.value;
    }

}
