/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.command.request.ServiceChangeReason;
import com.linkedlogics.megaco.descriptor.AuditDescriptor;
import java.io.IOException;
import javax.xml.bind.DatatypeConverter;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ServiceChangeParm ::= SEQUENCE
 * {
 * serviceChangeMethod [0] ServiceChangeMethod,
 * serviceChangeAddress [1] ServiceChangeAddress OPTIONAL,
 * serviceChangeVersion [2] INTEGER(0..99) OPTIONAL,
 * serviceChangeProfile [3] ServiceChangeProfile OPTIONAL,
 * serviceChangeReason [4] SEQUENCE OF OCTET STRING //Value,
 * -- A serviceChangeReason consists of a numeric reason code
 * -- and an optional text description.
 * -- The serviceChangeReason shall be a string consisting of
 * -- a decimal reason code, optionally followed by a single
 * -- space character and a textual description string.
 * -- This string is first BER-encoded as an IA5String.
 * -- The result of this BER-encoding is then encoded as
 * -- an ASN.1 OCTET STRING type, "double wrapping" the
 * -- value as was done for package elements.
 * serviceChangeDelay [5] INTEGER(0..4294967295) OPTIONAL,
 * -- 32-bit unsigned integer
 * serviceChangeMgcId [6] Mid OPTIONAL,
 * timeStamp [7] TimeNotation OPTIONAL,
 * nonStandardData [8] NonStandardData OPTIONAL,
 * ...,
 * serviceChangeInfo [9] AuditDescriptor OPTIONAL,
 * serviceChangeIncompleteFlag [10] NULL OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class ServiceChangeParm implements Parameter {

    private ServiceChangeMethod serviceChangeMethod;
    private ServiceChangeAddress serviceChangeAddress;
    private Integer serviceChangeVersion;
    private ServiceChangeProfile serviceChangeProfile;
    private ServiceChangeReason serviceChangeReason;
    private Long serviceChangeDelay;
    private Mid serviceChangeMgcId;
    private TimeNotation timeStamp;
    private NonStandardData nonStandardData;
    private AuditDescriptor serviceChangeInfo;
    private NullType serviceChangeIncompleteFlag;

    public ServiceChangeParm() {
    }

    public ServiceChangeParm(ServiceChangeMethod serviceChangeMethod) {
        this.serviceChangeMethod = serviceChangeMethod;
    }

    public NonStandardData getNonStandardData() {
        return nonStandardData;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 0, serviceChangeMethod.value());

        if (serviceChangeAddress != null) {
            serviceChangeAddress.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        if (serviceChangeVersion != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, serviceChangeVersion);
        }

        if (serviceChangeProfile != null) {
            serviceChangeProfile.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }

        if (serviceChangeReason != null) {
            AsnOutputStream tmp = new AsnOutputStream();
            tmp.writeStringIA5(serviceChangeReason.getServiceChangeReason());

            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 4);
            int lenPos1 = aos.StartContentDefiniteLength();
            aos.writeOctetString(tmp.toByteArray());
            aos.FinalizeContent(lenPos1);

            System.out.println(DatatypeConverter.printHexBinary(tmp.toByteArray()));

        }

        if (serviceChangeDelay != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 5, serviceChangeDelay);
        }

        if (serviceChangeMgcId != null) {
            serviceChangeMgcId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 6, aos);
        }

        if (timeStamp != null) {
            timeStamp.encode(Tag.CLASS_CONTEXT_SPECIFIC, 7, aos);
        }

        if (nonStandardData != null) {
            nonStandardData.encode(Tag.CLASS_CONTEXT_SPECIFIC, 8, aos);
        }

        if (serviceChangeInfo != null) {
            serviceChangeInfo.encode(Tag.CLASS_CONTEXT_SPECIFIC, 9, aos);
        }

        if (serviceChangeIncompleteFlag != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 10);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.serviceChangeMethod = ServiceChangeMethod.getInstance((int) ais.readInteger());
                    break;
                case 1:
                    this.serviceChangeAddress = new ServiceChangeAddress();
                    serviceChangeAddress.decode(ais);
                    break;
                case 2:
                    this.serviceChangeVersion = (int) ais.readInteger();
                    break;
                case 3:
                    this.serviceChangeProfile = new ServiceChangeProfile();
                    serviceChangeProfile.decode(ais);
                    break;
                case 4:
                    AsnInputStream tmpAis = ais.readSequenceStream();
                    byte[] data = tmpAis.readOctetString();
                    tmpAis = new AsnInputStream(data);
                    tag = tmpAis.readTag();
                    this.serviceChangeReason = new ServiceChangeReason(tmpAis.readIA5String());
                    break;
                case 5:
                    this.serviceChangeDelay = ais.readInteger();
                    break;
                case 6:
                    this.serviceChangeMgcId = new Mid();
                    serviceChangeMgcId.decode(ais);
                    break;
                case 7:
                    this.timeStamp = new TimeNotation();
                    timeStamp.decode(ais);
                    break;
                case 8:
                    this.nonStandardData = new NonStandardData();
                    nonStandardData.decode(ais);
                    break;
                case 9:
                    this.serviceChangeInfo = new AuditDescriptor();
                    serviceChangeInfo.decode(ais);
                    break;
                case 10:
                    this.serviceChangeIncompleteFlag = new NullType();
                    ais.readNull();
                    break;
            }
        }
    }

    public void setNonStandardData(NonStandardData nonStandardData) {
        this.nonStandardData = nonStandardData;
    }

    public ServiceChangeAddress getServiceChangeAddress() {
        return serviceChangeAddress;
    }

    public void setServiceChangeAddress(ServiceChangeAddress serviceChangeAddress) {
        this.serviceChangeAddress = serviceChangeAddress;
    }

    public Long getServiceChangeDelay() {
        return serviceChangeDelay;
    }

    public void setServiceChangeDelay(Long serviceChangeDelay) {
        this.serviceChangeDelay = serviceChangeDelay;
    }

    public NullType getServiceChangeIncompleteFlag() {
        return serviceChangeIncompleteFlag;
    }

    public void setServiceChangeIncompleteFlag(NullType serviceChangeIncompleteFlag) {
        this.serviceChangeIncompleteFlag = serviceChangeIncompleteFlag;
    }

    public AuditDescriptor getServiceChangeInfo() {
        return serviceChangeInfo;
    }

    public void setServiceChangeInfo(AuditDescriptor serviceChangeInfo) {
        this.serviceChangeInfo = serviceChangeInfo;
    }

    public ServiceChangeMethod getServiceChangeMethod() {
        return serviceChangeMethod;
    }

    public void setServiceChangeMethod(ServiceChangeMethod serviceChangeMethod) {
        this.serviceChangeMethod = serviceChangeMethod;
    }

    public Mid getServiceChangeMgcId() {
        return serviceChangeMgcId;
    }

    public void setServiceChangeMgcId(Mid serviceChangeMgcId) {
        this.serviceChangeMgcId = serviceChangeMgcId;
    }

    public ServiceChangeProfile getServiceChangeProfile() {
        return serviceChangeProfile;
    }

    public void setServiceChangeProfile(ServiceChangeProfile serviceChangeProfile) {
        this.serviceChangeProfile = serviceChangeProfile;
    }

    public ServiceChangeReason getServiceChangeReason() {
        return serviceChangeReason;
    }

    public void setServiceChangeReason(ServiceChangeReason serviceChangeReason) {
        this.serviceChangeReason = serviceChangeReason;
    }

    public Integer getServiceChangeVersion() {
        return serviceChangeVersion;
    }

    public void setServiceChangeVersion(Integer serviceChangeVersion) {
        this.serviceChangeVersion = serviceChangeVersion;
    }

    public TimeNotation getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(TimeNotation timeStamp) {
        this.timeStamp = timeStamp;
    }
}
