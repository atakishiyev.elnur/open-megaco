/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 *
 * @author eatakishiyev
 */
public class ServiceChangeProfile implements Parameter {

    private String profileName;

    public ServiceChangeProfile() {
    }

    public ServiceChangeProfile(String profileName) {
        this.profileName = profileName;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeStringIA5(tagClass, tag, profileName);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.profileName = ais.readIA5String();
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

}
