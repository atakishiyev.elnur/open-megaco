/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ServiceChangeResParm ::= SEQUENCE
 * {
 * serviceChangeMgcId [0] Mid OPTIONAL,
 * serviceChangeAddress [1] ServiceChangeAddress OPTIONAL,
 * serviceChangeVersion [2] INTEGER(0..99) OPTIONAL,
 * serviceChangeProfile [3] ServiceChangeProfile OPTIONAL,
 * timestamp [4] TimeNotation OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class ServiceChangeResParm implements Parameter {

    private Mid serviceChangeMgcId;
    private ServiceChangeAddress serviceChangeAddress;
    private Integer serviceChangeVersion;
    private ServiceChangeProfile serviceChangeProfile;
    private TimeNotation timestamp;

    public ServiceChangeResParm() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (serviceChangeMgcId != null) {
            serviceChangeMgcId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }

        if (serviceChangeAddress != null) {
            serviceChangeAddress.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        if (serviceChangeVersion != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, serviceChangeVersion);
        }

        if (serviceChangeProfile != null) {
            serviceChangeProfile.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }

        if (timestamp != null) {
            timestamp.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.serviceChangeMgcId = new Mid();
                    serviceChangeMgcId.decode(ais);
                    break;
                case 1:
                    this.serviceChangeAddress = new ServiceChangeAddress();
                    serviceChangeAddress.decode(ais);
                    break;
                case 2:
                    this.serviceChangeVersion = (int) ais.readInteger();
                    break;
                case 3:
                    this.serviceChangeProfile = new ServiceChangeProfile();
                    serviceChangeProfile.decode(ais);
                    break;
                case 4:
                    this.timestamp = new TimeNotation();
                    timestamp.decode(ais);
                    break;
            }
        }
    }

    /**
     * @return the serviceChangeMgcId
     */
    public Mid getServiceChangeMgcId() {
        return serviceChangeMgcId;
    }

    /**
     * @param serviceChangeMgcId the serviceChangeMgcId to set
     */
    public void setServiceChangeMgcId(Mid serviceChangeMgcId) {
        this.serviceChangeMgcId = serviceChangeMgcId;
    }

    /**
     * @return the serviceChangeAddress
     */
    public ServiceChangeAddress getServiceChangeAddress() {
        return serviceChangeAddress;
    }

    /**
     * @param serviceChangeAddress the serviceChangeAddress to set
     */
    public void setServiceChangeAddress(ServiceChangeAddress serviceChangeAddress) {
        this.serviceChangeAddress = serviceChangeAddress;
    }

    /**
     * @return the serviceChangeVersion
     */
    public Integer getServiceChangeVersion() {
        return serviceChangeVersion;
    }

    /**
     * @param serviceChangeVersion the serviceChangeVersion to set
     */
    public void setServiceChangeVersion(Integer serviceChangeVersion) {
        this.serviceChangeVersion = serviceChangeVersion;
    }

    /**
     * @return the serviceChangeProfile
     */
    public ServiceChangeProfile getServiceChangeProfile() {
        return serviceChangeProfile;
    }

    /**
     * @param serviceChangeProfile the serviceChangeProfile to set
     */
    public void setServiceChangeProfile(ServiceChangeProfile serviceChangeProfile) {
        this.serviceChangeProfile = serviceChangeProfile;
    }

    /**
     * @return the timestamp
     */
    public TimeNotation getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(TimeNotation timestamp) {
        this.timestamp = timestamp;
    }

}
