/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * ServiceChangeResult ::= CHOICE
 * {
 * errorDescriptor [0] ErrorDescriptor,
 * serviceChangeResParms [1] ServiceChangeResParm
 * }
 *
 * @author eatakishiyev
 */
public class ServiceChangeResult implements Parameter {

    private ErrorDescriptor errorDescriptor;
    private ServiceChangeResParm serviceChangeResParm;

    public ServiceChangeResult() {
    }

    public ServiceChangeResult(ErrorDescriptor errorDescriptor) {
        this.errorDescriptor = errorDescriptor;
    }

    public ServiceChangeResult(ServiceChangeResParm serviceChangeResParm) {
        this.serviceChangeResParm = serviceChangeResParm;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (errorDescriptor != null) {
            errorDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else {
            serviceChangeResParm.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.errorDescriptor = new ErrorDescriptor();
                    errorDescriptor.decode(ais);
                    break;
                case 1:
                    this.serviceChangeResParm = new ServiceChangeResParm();
                    serviceChangeResParm.decode(ais);
                    break;
            }
        }
    }

    public ErrorDescriptor getErrorDescriptor() {
        return errorDescriptor;
    }

    public ServiceChangeResParm getServiceChangeResParm() {
        return serviceChangeResParm;
    }

}
