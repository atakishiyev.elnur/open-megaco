/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum ServiceState {
    UNKNOWN(-1),
    TEST(0),
    OUT_OF_SVC(1),
    IN_SVC(2);

    private final int value;

    private ServiceState(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }

    public static ServiceState getInstance(int value) {
        switch (value) {
            case 0:
                return TEST;
            case 1:
                return OUT_OF_SVC;
            case 2:
                return IN_SVC;
            default:
                return UNKNOWN;
        }
    }
}
