/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * SigParameter ::= SEQUENCE
 * {
 * sigParameterName Name,
 * value Value,
 * -- For use of extraInfo see the comment related to PropertyParm
 * extraInfo CHOICE
 * {
 * relation Relation,
 * range BOOLEAN,
 * sublist BOOLEAN
 * } OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class SigParameter implements Parameter {

    private Name sigParameterName;
    private Value value;
    //Choice
    private Relation relation;
    private Boolean range;
    private Boolean sublist;

    public SigParameter() {

    }

    public SigParameter(Name sigParameterName, Value value) {
        this.sigParameterName = sigParameterName;
        this.value = value;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        sigParameterName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        value.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);

        if (relation != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, relation.value());
        } else if (range != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 3, range);
        } else if (sublist != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 4, sublist);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.sigParameterName = new Name();
                    sigParameterName.decode(ais);
                    break;
                case 1:
                    this.value = new Value();
                    value.decode(ais);
                    break;
                case 2:
                    this.relation = Relation.getInstance((int) ais.readInteger());
                    break;
                case 3:
                    this.range = ais.readBoolean();
                    break;
                case 4:
                    this.sublist = ais.readBoolean();
                    break;
            }
        }
    }

    public Name getSigParameterName() {
        return sigParameterName;
    }

    public void setSigParameterName(Name sigParameterName) {
        this.sigParameterName = sigParameterName;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
        range = null;
        sublist = null;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRange(Boolean range) {
        this.range = range;
        relation = null;
        sublist = null;
    }

    public Boolean getRange() {
        return range;
    }

    public void setSublist(Boolean sublist) {
        this.sublist = sublist;
        relation = null;
        range = null;
    }

    public Boolean getSublist() {
        return sublist;
    }

}
