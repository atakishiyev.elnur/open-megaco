/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * Signal ::= SEQUENCE
 * {
 * signalName [0] SignalName,
 * streamID [1] StreamID OPTIONAL,
 * sigType [2] SignalType OPTIONAL,
 * duration [3] INTEGER (0..65535) OPTIONAL,
 * notifyCompletion [4] NotifyCompletion OPTIONAL,
 * keepActive [5] BOOLEAN OPTIONAL,
 * sigParList [6] SEQUENCE OF SigParameter,
 * ...,
 * direction [7] SignalDirection OPTIONAL,
 * requestID [8] RequestID OPTIONAL,
 * intersigDelay [9] INTEGER (0..65535) OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class Signal implements Parameter {

    private SignalName signalName;
    private StreamID streamID;
    private SignalType signalType;
    private Integer duration;
    private NotifyCompletion notifyCompletion;
    private Boolean keepAlive;
    private final List<SigParameter> sigParList = new ArrayList<>();
    private SignalDirection direction;
    private RequestID requestID;
    private Integer intersigDelay = 0;

    public Signal() {
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.signalName = new SignalName();
                    signalName.decode(ais);
                    break;
                case 1:
                    this.streamID = new StreamID();
                    streamID.decode(ais);
                    break;
                case 2:
                    this.signalType = SignalType.getInstance((int) ais.readInteger());
                    break;
                case 3:
                    this.duration = (int) ais.readInteger();
                    break;
                case 4:
                    this.notifyCompletion = new NotifyCompletion();
                    notifyCompletion.decode(ais);
                    break;
                case 5:
                    this.keepAlive = ais.readBoolean();
                    break;
                case 6:
                    this.decodeSignalParamList(ais.readSequenceStream());
                    break;
                case 7:
                    this.direction = SignalDirection.getInstance((int) ais.readInteger());
                    break;
                case 8:
                    this.requestID = new RequestID();
                    requestID.decode(ais);
                    break;
                case 9:
                    this.intersigDelay = (int) ais.readInteger();
                    break;
            }
        }
    }

    private void decodeSignalParamList(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            SigParameter sigParameter = new SigParameter();
            sigParameter.decode(ais.readSequenceStream());
            sigParList.add(sigParameter);
        }
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        signalName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);

        if (streamID != null) {
            streamID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        if (signalType != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, signalType.value());
        }

        if (duration != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 3, duration);
        }

        if (notifyCompletion != null) {
            notifyCompletion.encode(Tag.CLASS_CONTEXT_SPECIFIC, 4, aos);
        }

        if (keepAlive != null) {
            aos.writeBoolean(Tag.CLASS_CONTEXT_SPECIFIC, 5, keepAlive);
        }

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 6);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (SigParameter sigParameter : sigParList) {
            sigParameter.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        if (direction != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 7, direction.value());
        }

        if (requestID != null) {
            requestID.encode(Tag.CLASS_CONTEXT_SPECIFIC, 8, aos);
        }

        if (intersigDelay != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 9, intersigDelay);
        }
        aos.FinalizeContent(lenPos);
    }

    public Signal(SignalName signalName) {
        this.signalName = signalName;
    }

    public SignalDirection getDirection() {
        return direction;
    }

    public void setDirection(SignalDirection direction) {
        this.direction = direction;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getIntersigDelay() {
        return intersigDelay;
    }

    public void setIntersigDelay(Integer intersigDelay) {
        this.intersigDelay = intersigDelay;
    }

    public Boolean getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(Boolean keepAlive) {
        this.keepAlive = keepAlive;
    }

    public NotifyCompletion getNotifyCompletion() {
        return notifyCompletion;
    }

    public void setNotifyCompletion(NotifyCompletion notifyCompletion) {
        this.notifyCompletion = notifyCompletion;
    }

    public RequestID getRequestID() {
        return requestID;
    }

    public void setRequestID(RequestID requestID) {
        this.requestID = requestID;
    }

    public List<SigParameter> getSigParList() {
        return sigParList;
    }

    public void setSignalName(SignalName signalName) {
        this.signalName = signalName;
    }

    public SignalName getSignalName() {
        return signalName;
    }

    public void setSignalType(SignalType signalType) {
        this.signalType = signalType;
    }

    public SignalType getSignalType() {
        return signalType;
    }

    public void setStreamID(StreamID streamID) {
        this.streamID = streamID;
    }

    public StreamID getStreamID() {
        return streamID;
    }

}
