/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum SignalDirection {
    INTERNAL(0),
    EXTERNAL(1),
    BOTH(2),
    UNKNOWN(-1);

    private final int value;

    private SignalDirection(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

    public static SignalDirection getInstance(int value) {
        switch (value) {
            case 0:
                return INTERNAL;
            case 1:
                return EXTERNAL;
            case 2:
                return BOTH;
            default:
                return UNKNOWN;
        }
    }
}
