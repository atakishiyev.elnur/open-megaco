/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public class SignalName extends PkgdName {

    private int signalName;

    public SignalName() {
    }

    public SignalName(int pkgName, int signalName) {
        this.packageName = pkgName;
        this.signalName = signalName;
    }

    @Override
    public int getObjectName() {
        return this.signalName;
    }

    @Override
    public void setObjectName(int objName) {
        this.signalName = objName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb
                .append("SignalName {PackageName: ")
                .append(getPackageName())
                .append("; SignalId: ")
                .append(signalName)
                .append("}");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SignalName) {
            SignalName other = (SignalName) obj;
            return other.signalName == this.signalName;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + this.signalName;
        return hash;
    }
}
