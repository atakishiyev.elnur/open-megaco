/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * SignalRequest ::=CHOICE
 * {
 * signal [0] Signal,
 * seqSigList [1] SeqSigList,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class SignalRequest implements Parameter {

    private Signal signal;
    private SeqSigList seqSigList;

    public SignalRequest() {
    }

    public SignalRequest(Signal signal) {
        this.signal = signal;
    }

    public SignalRequest(SeqSigList seqSigList) {
        this.seqSigList = seqSigList;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        if (signal != null) {
            signal.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else {
            seqSigList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    public void encode(AsnOutputStream aos) throws AsnException, IOException {
        if (signal != null) {
            signal.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else {
            seqSigList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.signal = new Signal();
                    signal.decode(ais.readSequenceStream());
                    break;
                case 1:
                    this.seqSigList = new SeqSigList();
                    seqSigList.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

    public SeqSigList getSeqSigList() {
        return seqSigList;
    }

    public Signal getSignal() {
        return signal;
    }

}
