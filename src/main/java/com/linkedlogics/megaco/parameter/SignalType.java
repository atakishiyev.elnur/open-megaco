/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum SignalType {
    UNKNOWN(-1),
    BRIEF(0),
    ON_OFF(1),
    TIMEOUT(2);

    private final int value;

    private SignalType(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }

    public static SignalType getInstance(int value) {
        switch (value) {
            case 0:
                return BRIEF;
            case 1:
                return ON_OFF;
            case 2:
                return TIMEOUT;
            default:
                return UNKNOWN;
        }
    }
}
