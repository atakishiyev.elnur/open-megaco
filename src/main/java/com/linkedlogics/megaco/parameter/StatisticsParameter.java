/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * StatisticsParameter ::= SEQUENCE
 * {
 * statName [0] PkgdName,
 * statValue [1] Value OPTIONAL
 * }
 * -- If statistic consists of a sub-lists there will be more than
 * -- one octetstring in statValue.
 *
 * @author eatakishiyev
 */
public class StatisticsParameter implements Parameter {

    private StatisticsId statName;
    private Value statValue;

    public StatisticsParameter() {
    }

    public StatisticsParameter(StatisticsId statName) {
        this.statName = statName;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        statName.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (statValue != null) {
            statValue.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.statName = new StatisticsId();
                    statName.decode(ais);
                    break;
                case 1:
                    this.statValue = new Value();
                    statValue.decode(ais);
                    break;
            }
        }
    }

    public StatisticsId getStatName() {
        return statName;
    }

    public void setStatName(StatisticsId statName) {
        this.statName = statName;
    }

    public Value getStatValue() {
        return statValue;
    }

    public void setStatValue(Value statValue) {
        this.statValue = statValue;
    }

}
