/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum StreamMode {
    SEND_ONLY(0),
    RECV_ONLY(1),
    SEND_RECV(2),
    INACTIVE(3),
    LOOPBACK(4),
    UNKNOWN(-1);

    private final int value;

    private StreamMode(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }

    public static StreamMode getInstance(int value) {
        switch (value) {
            case 0:
                return SEND_ONLY;
            case 1:
                return RECV_ONLY;
            case 2:
                return SEND_RECV;
            case 3:
                return INACTIVE;
            case 4:
                return LOOPBACK;
            default:
                return UNKNOWN;
        }
    }
}
