/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.descriptor.StatisticsDescriptor;
import com.linkedlogics.megaco.descriptor.LocalControlDescriptor;
import com.linkedlogics.megaco.descriptor.LocalRemoteDescriptor;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * StreamParms ::= SEQUENCE
 * {
 * localControlDescriptor [0] LocalControlDescriptor OPTIONAL,
 * localDescriptor [1] LocalRemoteDescriptor OPTIONAL,
 * remoteDescriptor [2] LocalRemoteDescriptor OPTIONAL,
 * statisticsDescriptor [3] StatisticsDescriptor OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class StreamParms implements Parameter {

    private LocalControlDescriptor localControlDescriptor;
    private LocalRemoteDescriptor localDescriptor;
    private LocalRemoteDescriptor remoteDescriptor;
    private StatisticsDescriptor statisticsDescriptor;

    public LocalControlDescriptor getLocalControlDescriptor() {
        return localControlDescriptor;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (localControlDescriptor != null) {
            localControlDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        }
        if (localDescriptor != null) {
            localDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }

        if (remoteDescriptor != null) {
            remoteDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);
        }

        if (statisticsDescriptor != null) {
            statisticsDescriptor.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.localControlDescriptor = new LocalControlDescriptor();
                    localControlDescriptor.decode(ais.readSequenceStream());
                    break;
                case 1:
                    this.localDescriptor = new LocalRemoteDescriptor();
                    localDescriptor.decode(ais.readSequenceStream());
                    break;
                case 2:
                    this.remoteDescriptor = new LocalRemoteDescriptor();
                    remoteDescriptor.decode(ais.readSequenceStream());
                    break;
                case 3:
                    this.statisticsDescriptor = new StatisticsDescriptor();
                    statisticsDescriptor.decode(ais.readSequenceStream());
                    break;
            }
        }
    }

    public void setLocalControlDescriptor(LocalControlDescriptor localControlDescriptor) {
        this.localControlDescriptor = localControlDescriptor;
    }

    public LocalRemoteDescriptor getLocalDescriptor() {
        return localDescriptor;
    }

    public void setLocalDescriptor(LocalRemoteDescriptor localDescriptor) {
        this.localDescriptor = localDescriptor;
    }

    public LocalRemoteDescriptor getRemoteDescriptor() {
        return remoteDescriptor;
    }

    public void setRemoteDescriptor(LocalRemoteDescriptor remoteDescriptor) {
        this.remoteDescriptor = remoteDescriptor;
    }

    public StatisticsDescriptor getStatisticsDescriptor() {
        return statisticsDescriptor;
    }

    public void setStatisticsDescriptor(StatisticsDescriptor statisticsDescriptor) {
        this.statisticsDescriptor = statisticsDescriptor;
    }

}
