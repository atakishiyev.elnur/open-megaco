/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * CHOICE
 * {
 * oneStream [0] StreamParms,
 * multiStream [1] SEQUENCE OF StreamDescriptor
 * } OPTIONAL
 *
 * @author eatakishiyev
 */
public class Streams implements Parameter {

    private StreamParms oneStream;
    private final List<StreamDescriptor> multiStream = new ArrayList<>();

    public Streams() {
    }

    public Streams(StreamParms oneStream) {
        this.oneStream = oneStream;
    }

    public Streams(StreamDescriptor streamDescriptor) {
        multiStream.add(streamDescriptor);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (oneStream != null) {
            oneStream.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else if (multiStream.size() > 0) {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
            int lenPos1 = aos.StartContentDefiniteLength();
            for (StreamDescriptor streamDescriptor : multiStream) {
                streamDescriptor.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
            }
            aos.FinalizeContent(lenPos1);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.oneStream = new StreamParms();
                    oneStream.decode(ais.readSequenceStream());
                    break;
                case 1:
                    this.decodeMultiStream(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeMultiStream(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            StreamDescriptor streamDescriptor = new StreamDescriptor();
            streamDescriptor.decode(ais);
            multiStream.add(streamDescriptor);
        }
    }

    public void setOneStream(StreamParms oneStream) {
        this.oneStream = oneStream;
    }

    public List<StreamDescriptor> getMultiStream() {
        return multiStream;
    }

    public void addStreamDescriptor(StreamDescriptor streamDescriptor) {
        multiStream.add(streamDescriptor);
    }

    public StreamParms getOneStream() {
        return oneStream;
    }

    public boolean isOneStream() {
        return oneStream != null;
    }

}
