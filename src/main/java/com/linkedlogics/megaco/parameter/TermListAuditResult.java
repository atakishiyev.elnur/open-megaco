/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TermListAuditResult ::= SEQUENCE
 * {
 * terminationIDList [0] TerminationIDList,
 * terminationAuditResult [1] TerminationAudit,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class TermListAuditResult implements Parameter {

    private TerminationIDList terminationIDList;
    private TerminationAudit terminationAuditResult;

    public TermListAuditResult() {
    }

    public TermListAuditResult(TerminationIDList terminationIDList, TerminationAudit terminationAuditResult) {
        this.terminationIDList = terminationIDList;
        this.terminationAuditResult = terminationAuditResult;
    }

    public TerminationAudit getTerminationAuditResult() {
        return terminationAuditResult;
    }

    public void setTerminationAuditResult(TerminationAudit terminationAuditResult) {
        this.terminationAuditResult = terminationAuditResult;
    }

    public TerminationIDList getTerminationIDList() {
        return terminationIDList;
    }

    public void setTerminationIDList(TerminationIDList terminationIDList) {
        this.terminationIDList = terminationIDList;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        terminationIDList.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        terminationAuditResult.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationIDList = new TerminationIDList();
                    terminationIDList.decode(ais);
                    break;
                case 1:
                    this.terminationAuditResult = new TerminationAudit();
                    terminationAuditResult.decode(ais);
                    break;
            }
        }
    }

}
