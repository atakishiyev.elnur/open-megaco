/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * TerminationAudit ::= SEQUENCE OF AuditReturnParameter
 *
 * @author eatakishiyev
 */
public class TerminationAudit implements Parameter {

    private final List<AuditReturnParameter> auditReturnParameters = new ArrayList<>();

    public TerminationAudit() {
    }

    public List<AuditReturnParameter> getAuditReturnParameters() {
        return auditReturnParameters;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        for (AuditReturnParameter auditReturnParameter : auditReturnParameters) {
            auditReturnParameter.encode(aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            AuditReturnParameter auditReturnParameter = new AuditReturnParameter();
            auditReturnParameter.decode(ais);
            auditReturnParameters.add(auditReturnParameter);
        }
    }

    public void addAuditReturnParameter(AuditReturnParameter auditReturnParameter) {
        auditReturnParameters.add(auditReturnParameter);
    }
}
