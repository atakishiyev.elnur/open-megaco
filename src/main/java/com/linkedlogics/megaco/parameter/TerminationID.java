/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.DatatypeConverter;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * The ASN.1 description uses octet strings of up to 8 octets in length for TerminationIDs. This means
 * that TerminationIDs consist of at most 64 bits. A fully specified TerminationID may be preceded by
 * a sequence of wildcarding fields. A wildcarding field is one octet in length. Bit 7 (the most
 * significant bit) of this octet specifies what type of wildcarding is invoked: if the bit value equals 1,
 * then the ALL wildcard is used; if the bit value if 0, then the CHOOSE wildcard is used. Bit 6 of the
 * wildcarding field specifies whether the wildcarding pertains to one level in the hierarchical naming
 * scheme (bit value 0) or to the level of the hierarchy specified in the wildcarding field plus all lower
 * levels (bit value 1). Bits 0 through 5 of the wildcarding field specify the bit position in the
 * TerminationID at which the wildcarding starts.
 * TerminationID ::= SEQUENCE
 * {
 * wildcard [0] SEQUENCE OF WildcardField,
 * id [1] OCTET STRING(SIZE(1..8)),
 * ...
 * }
 * -- See Rec. ITU-T H.248.1 (A.1) for explanation of wildcarding mechanism.
 * -- TerminationID 0xFFFFFFFFFFFFFFFF indicates the Root Termination.
 *
 * @author eatakishiyev
 */
public class TerminationID implements Parameter {

    public static final TerminationID TERMINATION_ID_ROOT
            = new TerminationID(new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF});

    public static final TerminationID TERMINATION_ID_CHOOSE
            = new TerminationID(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
            new WildcardField(WildcardField.WildcardMode.CHOOSE, WildcardField.WildcardLevel.THIS_AND_BELOW_LEVELS, 63));

    private final List<WildcardField> wildcard = new ArrayList<>();
    private byte[] id;
    private final long terminationKey = System.nanoTime();

    public TerminationID() {
    }

    public TerminationID(byte[] id) {
        this.id = id;
    }

    public TerminationID(byte[] id, WildcardField... wildcardFields) {
        this.id = id;
        for (WildcardField wildcardField : wildcardFields) {
            wildcard.add(wildcardField);
        }
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 0);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (WildcardField wildcardField : wildcard) {
            wildcardField.encode(aos);
        }
        aos.FinalizeContent(lenPos1);

        aos.writeOctetString(Tag.CLASS_CONTEXT_SPECIFIC, 1, id);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    public List<WildcardField> getWildcard() {
        return wildcard;
    }

    public void addWilcardField(WildcardField wildcardField) {
        wildcard.add(wildcardField);
    }

    public byte[] getId() {
        return id;
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0://Wildcard tag
                    this.decodeWildcard(ais.readSequenceStream());
                    break;
                case 1://id tag
                    this.id = ais.readOctetString();
                    break;
            }
        }
    }

    private void decodeWildcard(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            WildcardField wildcardField = new WildcardField();
            wildcardField.decode(ais);
            wildcard.add(wildcardField);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TerminationID) {
            TerminationID terminationID = (TerminationID) obj;
            return Arrays.equals(terminationID.getId(), id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Arrays.hashCode(this.id);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TerminationID [")
                .append("Id = ").append(DatatypeConverter.printHexBinary(id));
        if (wildcard.size() > 0) {
            sb.append("WildcardFields[");
            for (WildcardField wildcardField : wildcard) {
                sb.append(wildcardField).append("\r\n");
            }
            sb.append("]");
        }
        return sb.toString();
    }

    public long getTerminationKey() {
        return terminationKey;
    }

}
