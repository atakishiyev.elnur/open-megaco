/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TerminationIDList ::= SEQUENCE OF TerminationID
 *
 * @author eatakishiyev
 */
public class TerminationIDList implements Parameter {

    private final List<TerminationID> terminationIDs = new ArrayList<>();

    public TerminationIDList() {
    }

    public TerminationIDList(TerminationID... terminationIDs) {
        for (TerminationID terminationID : terminationIDs) {
            this.terminationIDs.add(terminationID);
        }
    }

    public List<TerminationID> getTerminationIDs() {
        return terminationIDs;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        for (TerminationID terminationId : terminationIDs) {
            terminationId.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            this.parseTerminationIDList(ais);
        }
    }

    private void parseTerminationIDList(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            TerminationID terminationID = new TerminationID();
            terminationID.decode(ais);
            terminationIDs.add(terminationID);
        }
    }

    public void addTerminationID(TerminationID terminationID) {
        terminationIDs.add(terminationID);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TerminationIDList[");
        for (TerminationID terminationID : terminationIDs) {
            sb.append(terminationID).append("\r\n");
        }
        sb.append("]");
        return sb.toString();
    }

}
