/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 *
 * @author eatakishiyev
 */
public class TimeNotation implements Parameter {

    private String date;//-- yyyymmdd format
    private String time;//-- hhmmssss format

    public TimeNotation() {
    }

    /**
     *
     * @param date yyyymmdd format date
     * @param time hhmmssss format time
     */
    public TimeNotation(String date, String time) {
        this.date = date;
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        aos.writeStringIA5(Tag.CLASS_CONTEXT_SPECIFIC, 0, date);
        aos.writeStringIA5(Tag.CLASS_CONTEXT_SPECIFIC, 1, time);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.date = ais.readIA5String();
                    break;
                case 1:
                    this.time = ais.readIA5String();
                    break;
            }
        }
    }

}
