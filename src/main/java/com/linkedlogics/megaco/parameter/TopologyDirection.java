/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum TopologyDirection {
    BOTHWAY(0),
    ISOLATE(1),
    ONEWAY(2),
    UNKNOWN(-1);

    private final int value;

    private TopologyDirection(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }

    public static TopologyDirection getInstance(int value) {
        switch (value) {
            case 0:
                return BOTHWAY;
            case 1:
                return ISOLATE;
            case 2:
                return ONEWAY;
            default:
                return UNKNOWN;
        }
    }
}
