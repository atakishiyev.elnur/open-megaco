/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum TopologyDirectionExtension {
    ONE_WAY_EXTERNAL(0),
    ONE_WAY_BOTH(1),
    UNKNOWN(-1);

    private final int value;

    private TopologyDirectionExtension(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }

    public static final TopologyDirectionExtension getInstance(int value) {
        switch (value) {
            case 0:
                return ONE_WAY_EXTERNAL;
            case 1:
                return ONE_WAY_BOTH;
            default:
                return UNKNOWN;
        }
    }

}
