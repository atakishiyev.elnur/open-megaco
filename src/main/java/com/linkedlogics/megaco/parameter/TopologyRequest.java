/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TopologyRequest ::= SEQUENCE
 * {
 * terminationFrom [0] TerminationID,
 * terminationTo [1] TerminationID,
 * topologyDirection [2] ENUMERATED
 * {
 * bothway(0),
 * isolate(1),
 * oneway(2)
 * },
 * ...,
 * streamID [3] StreamID OPTIONAL,
 * topologyDirectionExtension [4] ENUMERATED
 * {
 * onewayexternal(0),
 * onewayboth(1),
 * ...
 * } OPTIONAL
 * }
 * -- if present, topologyDirectionExtension takes precedence over
 * -- topologyDirection
 *
 * @author eatakishiyev
 */
public class TopologyRequest implements Parameter {

    private TerminationID terminationFrom;
    private TerminationID terminationTo;
    private TopologyDirection topologyDirection;
    private StreamID streamId; //optional
    private TopologyDirectionExtension topologyDirectionExtension; // optional

    public TopologyRequest() {
    }

    public TopologyRequest(TerminationID terminationFrom, TerminationID terminationTo, TopologyDirection topologyDirection) {
        this.terminationFrom = terminationFrom;
        this.terminationTo = terminationTo;
        this.topologyDirection = topologyDirection;
    }

    public void encode(AsnOutputStream aos) throws AsnException, IOException {
        this.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        terminationFrom.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        terminationTo.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 2, topologyDirection.value());
        if (streamId != null) {
            streamId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }
        if (topologyDirectionExtension != null) {
            aos.writeInteger(Tag.CLASS_CONTEXT_SPECIFIC, 4, topologyDirectionExtension.value());
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.terminationFrom = new TerminationID();
                    this.terminationFrom.decode(ais);
                    break;
                case 1:
                    this.terminationTo = new TerminationID();
                    this.terminationTo.decode(ais);
                    break;
                case 2:
                    this.topologyDirection = TopologyDirection.getInstance((int) ais.readInteger());
                    break;
                case 3:
                    this.streamId = new StreamID();
                    this.streamId.decode(ais);
                    break;
                case 4:
                    this.topologyDirectionExtension = TopologyDirectionExtension.getInstance((int) ais.readInteger());
                    break;
            }
        }
    }

    public TerminationID getTerminationFrom() {
        return terminationFrom;
    }

    public void setTerminationFrom(TerminationID terminationFrom) {
        this.terminationFrom = terminationFrom;
    }

    public TerminationID getTerminationTo() {
        return terminationTo;
    }

    public void setTerminationTo(TerminationID terminationTo) {
        this.terminationTo = terminationTo;
    }

    public StreamID getStreamId() {
        return streamId;
    }

    public void setStreamId(StreamID streamId) {
        this.streamId = streamId;
    }

    public TopologyDirection getTopologyDirection() {
        return topologyDirection;
    }

    public void setTopologyDirection(TopologyDirection topologyDirection) {
        this.topologyDirection = topologyDirection;
    }

    public TopologyDirectionExtension getTopologyDirectionExtension() {
        return topologyDirectionExtension;
    }

    public void setTopologyDirectionExtension(TopologyDirectionExtension topologyDirectionExtension) {
        this.topologyDirectionExtension = topologyDirectionExtension;
    }

}
