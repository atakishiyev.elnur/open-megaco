/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 *
 * @author eatakishiyev
 */
public class TransactionID implements Parameter {

    private long transactionId;

    public TransactionID() {
    }

    public TransactionID(long transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeInteger(tagClass, tag, this.transactionId);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this.transactionId = ais.readInteger();
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

}
