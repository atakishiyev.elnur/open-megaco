/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import com.linkedlogics.megaco.action.ActionReply;
import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import com.linkedlogics.megaco.parameter.Parameter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * * transactionResult ::= CHOICE
 * {
 * transactionError [0] ErrorDescriptor,
 * actionReplies [1] SEQUENCE OF ActionReply
 * },
 *
 * @author eatakishiyev
 */
public class TransactionResult implements Parameter {

    private ErrorDescriptor transactionError;
    private List<ActionReply> actionReplies = new ArrayList<>();

    ;

    public TransactionResult() {

    }

    public TransactionResult(ErrorDescriptor transactionError) {
        this.transactionError = transactionError;
    }

    public TransactionResult(ActionReply... actionReplies) {
        for (ActionReply actionReply : actionReplies) {
            this.actionReplies.add(actionReply);
        }
    }

    public List<ActionReply> getActionReplies() {
        return actionReplies;
    }

    public ErrorDescriptor getTransactionError() {
        return transactionError;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        if (transactionError != null) {
            transactionError.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        } else {
            aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
            int lenPos1 = aos.StartContentDefiniteLength();
            for (ActionReply actionReply : actionReplies) {
                actionReply.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
            }
            aos.FinalizeContent(lenPos1);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        int tag = ais.readTag();
        switch (tag) {
            case 0:
                this.transactionError = new ErrorDescriptor();
                transactionError.decode(ais);
                break;
            case 1:
                this.decodeActionReplies(ais.readSequenceStream());
                break;
        }
    }

    private void decodeActionReplies(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            if (tag == Tag.SEQUENCE) {
                ActionReply actionReply = new ActionReply();
                actionReply.decode(ais.readSequenceStream());
                this.actionReplies.add(actionReply);
            } else {

            }
        }
    }

    public void addActionReply(ActionReply actionReply) {
        actionReplies.add(actionReply);
    }
}
