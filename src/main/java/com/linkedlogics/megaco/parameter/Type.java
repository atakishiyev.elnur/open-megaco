/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

/**
 *
 * @author eatakishiyev
 */
public enum Type {
    MEDIA_DESCRIPTOR(0),
    MODEM_DESCRIPTOR(1),
    MUX_DESCRIPTOR(2),
    EVENTS_DESCRIPTOR(3),
    EVENT_BUFFER_DESCRIPTOR(4),
    SIGNALS_DESCRIPTOR(5),
    DIGIT_MAP_DESCRIPTOR(6),
    AUDIT_DESCRIPTOR(7),
    STATISTICS_DESCRIPTOR(8),
    ERROR_DESCRIPTOR(9),
    OBSERVED_EVENTS_DESCRIPTOR(10),
    PACKAGES_DESCRIPTOR(11),
    UNKNOWN(-1);

    private final int value;

    private Type(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }

    public static Type getInstance(int value) {
        switch (value) {
            case 0:
                return MEDIA_DESCRIPTOR;
            case 1:
                return MODEM_DESCRIPTOR;
            case 2:
                return MUX_DESCRIPTOR;
            case 3:
                return EVENTS_DESCRIPTOR;
            case 4:
                return EVENT_BUFFER_DESCRIPTOR;
            case 5:
                return SIGNALS_DESCRIPTOR;
            case 6:
                return DIGIT_MAP_DESCRIPTOR;
            case 7:
                return AUDIT_DESCRIPTOR;
            case 8:
                return STATISTICS_DESCRIPTOR;
            case 9:
                return ERROR_DESCRIPTOR;
            case 10:
                return OBSERVED_EVENTS_DESCRIPTOR;
            case 11:
                return PACKAGES_DESCRIPTOR;
            default:
                return UNKNOWN;
        }
    }
}
