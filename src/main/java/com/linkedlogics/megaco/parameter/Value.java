/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 * Value ::= SEQUENCE OF OCTET STRING
 *
 * @author eatakishiyev
 */
public class Value implements Parameter {

    private final List<Data> values = new ArrayList<>();

    public Value() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        for (Data value : values) {
            aos.writeOctetString(value.data);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        this._decode(ais.readSequenceStream());
    }

    private void _decode(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            byte[] value = ais.readOctetString();
            Data data = new Data(value);
            values.add(data);
        }
    }

    public List<Data> getValues() {
        return values;
    }

    public void addValue(byte[] value) {
        values.add(new Data(value));
    }

    public void addInteger(long value) throws IOException, AsnException {
        values.add(new Data(value));
    }

    public void addString(String value) throws AsnException, IOException {
        values.add(new Data(value));
    }

    public static class Data {

        private byte[] data;

        public Data() {
        }

        public Data(byte[] data) {
            this.data = data;
        }

        public Data(String str) throws AsnException, IOException {
            this.writeString(str);
        }

        public Data(long value) throws IOException, AsnException {
            this.writeInteger(value);
        }

        public byte[] getData() {
            return data;
        }

        public long asInteger() throws IOException, AsnException {
            AsnInputStream ais = new AsnInputStream(data);
            ais.readTag();
            return ais.readInteger();
        }

        public String asString() throws AsnException, IOException {
            AsnInputStream ais = new AsnInputStream(data);
            ais.readTag();
            return ais.readIA5String();
        }

        private final void writeInteger(long value) throws IOException, AsnException {
            AsnOutputStream aos = new AsnOutputStream();
            aos.writeInteger(value);
            this.data = aos.toByteArray();
        }

        private final void writeString(String str) throws AsnException, IOException {
            AsnOutputStream aos = new AsnOutputStream();
            aos.writeStringIA5(str);
            this.data = aos.toByteArray();
        }
    }
}
