/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter;

import java.io.IOException;
import java.util.Objects;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 *
 * @author eatakishiyev
 */
public class WildcardField implements Parameter {

    private WildcardMode wildcardMode;
    private WildcardLevel wildcardLevel;
    private int wildcardingPosition;

    public WildcardField() {
    }

    public WildcardField(WildcardMode wildcardMode, WildcardLevel wildcardLevel, int wildcardingPosition) {
        this.wildcardMode = wildcardMode;
        this.wildcardLevel = wildcardLevel;
        this.wildcardingPosition = wildcardingPosition;
    }

    public WildcardLevel getWildcardLevel() {
        return wildcardLevel;
    }

    public WildcardMode getWildcardMode() {
        return wildcardMode;
    }

    public int getWildcardingPosition() {
        return wildcardingPosition;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        int value = wildcardMode.value;
        value = (value << 1) | wildcardLevel.value;
        value = (value << 6) | wildcardingPosition;

        aos.writeInteger(tagClass, tag, value);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        int value = (int) ais.readInteger();
        this.wildcardMode = WildcardMode.getInstance(value >> 7);
        this.wildcardLevel = WildcardLevel.getInstance((value >> 6) & 0x01);
        this.wildcardingPosition = value & 0b00111111;
    }

    public void encode(AsnOutputStream aos) throws IOException, AsnException {
        this.encode(Tag.CLASS_UNIVERSAL, Tag.STRING_OCTET, aos);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof WildcardField) {
            WildcardField other = (WildcardField) obj;
            return other.wildcardLevel == this.wildcardLevel
                    && other.wildcardMode == this.wildcardMode
                    && other.wildcardingPosition == this.wildcardingPosition;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.wildcardMode);
        hash = 89 * hash + Objects.hashCode(this.wildcardLevel);
        hash = 89 * hash + this.wildcardingPosition;
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WildCardField [").
                append("WildCardMode = ").append(this.wildcardMode).
                append("WildCardLevel = ").append(this.wildcardLevel).
                append("WildcardingPosition = ").append(this.wildcardingPosition)
                .append("]");
        return sb.toString();
    }

    public enum WildcardMode {
        CHOOSE(0),
        ALL(1);

        private final int value;

        private WildcardMode(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static WildcardMode getInstance(int value) {
            switch (value) {
                case 0:
                    return CHOOSE;
                default:
                    return ALL;
            }
        }
    }

    public enum WildcardLevel {
        ONE_LEVEL(0),
        THIS_AND_BELOW_LEVELS(1);

        private final int value;

        private WildcardLevel(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static WildcardLevel getInstance(int value) {
            switch (value) {
                case 0:
                    return ONE_LEVEL;
                default:
                    return THIS_AND_BELOW_LEVELS;
            }
        }

    }

}
