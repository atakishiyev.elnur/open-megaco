/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.parameter.sdp;

import com.linkedlogics.megaco.parameter.PropertyGroup;
import com.linkedlogics.megaco.parameter.PropertyName;
import com.linkedlogics.megaco.parameter.PropertyParm;
import gov.nist.javax.sdp.MediaDescriptionImpl;
import gov.nist.javax.sdp.TimeDescriptionImpl;
import gov.nist.javax.sdp.fields.AttributeField;
import gov.nist.javax.sdp.fields.BandwidthField;
import gov.nist.javax.sdp.fields.EmailField;
import gov.nist.javax.sdp.fields.OriginField;
import gov.nist.javax.sdp.fields.PhoneField;
import gov.nist.javax.sdp.fields.RepeatField;
import gov.nist.javax.sdp.fields.SDPFieldNames;
import gov.nist.javax.sdp.fields.TimeField;
import gov.nist.javax.sdp.fields.URIField;
import gov.nist.javax.sdp.fields.ZoneAdjustment;
import java.io.IOException;
import java.util.Vector;
import javax.sdp.Connection;
import javax.sdp.Key;
import javax.sdp.SdpException;
import javax.sdp.SdpFactory;
import javax.sdp.SdpParseException;
import javax.sdp.SessionDescription;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

/**
 *
 * @author eatakishiyev
 */
public class MediaStreamProperties {

    private final AsnOutputStream aos = new AsnOutputStream();
    private static final int PACKAGE_ID = 0x00;

    public static final int SDP_V = 0xb001;//protocol version
    public static final int SDP_O = 0xb002;//owner/creator and session ID
    public static final int SDP_S = 0xb003;//session name
    public static final int SDP_I = 0xb004;//session identifier
    public static final int SDP_U = 0xb005;//URI of descriptor
    public static final int SDP_E = 0xb006;//email address
    public static final int SDP_P = 0xb007;//phone number
    public static final int SDP_C = 0xb008;//connection information
    public static final int SDP_B = 0xb009;//bandwidth information
    public static final int SDP_Z = 0xb00A;//time zone adjustment
    public static final int SDP_K = 0xb00B;//encryption key
    public static final int SDP_A = 0xb00C;//zero or more session attributes
    public static final int SDP_T = 0xb00D;//active session time
    public static final int SDP_R = 0xb00E;//zero or more repeat times
    public static final int SDP_M = 0xb00F;//media type, port , transport and format

    public MediaStreamProperties() {
    }

    public SessionDescription getSessionDescription(PropertyGroup propertyGroup) throws SdpParseException, AsnException, IOException {
        SdpFactory sdpFactory = SdpFactory.getInstance();
        StringBuilder sb = new StringBuilder();
        for (PropertyParm propertyParm : propertyGroup.getPropertyParms()) {
            int propertyId = propertyParm.getName().getObjectName();
            byte[] data = propertyParm.getValues().get(0);
            AsnInputStream ais = new AsnInputStream(data);
            int tag = ais.readTag();
            String value = ais.readIA5String();
            switch (propertyId) {
                case 0x0000b001:
                    sb.append(SDPFieldNames.PROTO_VERSION_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b002:
                    sb.append(SDPFieldNames.ORIGIN_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b003:
                    sb.append(SDPFieldNames.SESSION_NAME_FIELD);
                    sb.append(value).
                            append("\n");
                    break;
                case 0x0000b004:
                    sb.append(SDPFieldNames.INFORMATION_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b005:
                    sb.append(SDPFieldNames.URI_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b006:
                    sb.append(SDPFieldNames.EMAIL_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b007:
                    sb.append(SDPFieldNames.PHONE_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b008:
                    sb.append(SDPFieldNames.CONNECTION_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b009:
                    sb.append(SDPFieldNames.BANDWIDTH_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b00A:
                    sb.append(SDPFieldNames.ZONE_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b00B:
                    sb.append(SDPFieldNames.KEY_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b00C:
                    sb.append(SDPFieldNames.ATTRIBUTE_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b00D:
                    sb.append(SDPFieldNames.TIME_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
                case 0x0000b00F:
                    sb.append(SDPFieldNames.MEDIA_FIELD);
                    sb.append(value)
                            .append("\n");
                    break;
            }
        }
        return sdpFactory.createSessionDescription(sb.toString());
    }

    public PropertyGroup getPropertyGroup(SessionDescription sessionDescription) throws SdpParseException, SdpException, AsnException, IOException {
        aos.reset();
        PropertyGroup propertyGroup = new PropertyGroup();
        //SDP Version adding [SDP_V]
        PropertyParm sdpVersion = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_V),
                createVersion(sessionDescription.getVersion().getVersion()));//version + \r\n
//        sdpVersion.setSublist(Boolean.TRUE);
        propertyGroup.addPropertyParm(sdpVersion);

        //SDP owner/creator,session-id [SDP_O]
        OriginField origin = (OriginField) sessionDescription.getOrigin();
        PropertyParm sdpOwner = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_O),
                createOwner(origin.getUsername(), origin.getSessIdAsString(), origin.getSessVersionAsString(), origin.getNetworkType(),
                        origin.getAddressType(), origin.getAddress()));
//        sdpOwner.setSublist(Boolean.TRUE);
        propertyGroup.addPropertyParm(sdpOwner);

        //SDP Session name [SDP_S]
        PropertyParm sdpSessionName = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_S),
                createSessionName(sessionDescription.getSessionName().getValue()));
//        sdpSessionName.setSublist(Boolean.TRUE);
        propertyGroup.addPropertyParm(sdpSessionName);

        //SDP Session information [SDP_I]
        if (sessionDescription.getInfo() != null) {
            PropertyParm sdpSessinInformation = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_I),
                    createSessionInformation(sessionDescription.getInfo().getValue()));
//            sdpSessinInformation.setSublist(Boolean.TRUE);
            propertyGroup.addPropertyParm(sdpSessinInformation);
        }

        //SDP URI of descriptor [SDP_U]
        if (sessionDescription.getURI() != null) {
            PropertyParm sdpUriOfDescriptor = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_U),
                    createUriOfDescriptor(((URIField) sessionDescription.getURI()).getURI()));
//            sdpUriOfDescriptor.setSublist(Boolean.TRUE);
            propertyGroup.addPropertyParm(sdpUriOfDescriptor);
        }

        //SDP email address [SDP_E]
        Vector<EmailField> emails = sessionDescription.getEmails(false);
        if (emails != null) {
            for (EmailField email : emails) {
                PropertyParm sdpEmailAddress = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_E),
                        createEmail(email.getEmailAddress().encode()));
//                sdpEmailAddress.setSublist(Boolean.TRUE);
                propertyGroup.addPropertyParm(sdpEmailAddress);
            }
        }

        //SDP phone number [SDP_P]
        Vector<PhoneField> phones = sessionDescription.getPhones(false);
        if (phones != null) {
            for (PhoneField phone : phones) {
                PropertyParm sdpPhone = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_P),
                        createPhone(phone.getPhoneNumber()));
//                sdpPhone.setSublist(Boolean.TRUE);
                propertyGroup.addPropertyParm(sdpPhone);
            }
        }

        //SDP connection information [SDP_C]
        Connection connection = sessionDescription.getConnection();
        if (connection != null) {
            PropertyParm sdpConnection = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_C),
                    createConnection(connection.getNetworkType(), connection.getAddressType(),
                            connection.getAddress()));
//            sdpConnection.setSublist(Boolean.TRUE);
            propertyGroup.addPropertyParm(sdpConnection);
        }

        //SDP bandwidth information [SDP_B]
        Vector<BandwidthField> bandwidths = sessionDescription.getBandwidths(false);
        if (bandwidths != null) {
            for (BandwidthField bandwidthField : bandwidths) {
                PropertyParm sdpBandwith = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_B),
                        createBandwidth(bandwidthField.getBwtype(), bandwidthField.getBandwidth()));
//                sdpBandwith.setSublist(Boolean.TRUE);
                propertyGroup.addPropertyParm(sdpBandwith);
            }
        }

        //SDP time zone adjusment [SDP_Z]
        Vector<ZoneAdjustment> zoneAdjustments = sessionDescription.getZoneAdjustments(false);
        if (zoneAdjustments != null) {
            for (ZoneAdjustment zoneAdjustment : zoneAdjustments) {
                PropertyParm sdpZoneAdjustment = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_Z),
                        createZoneAjustment(zoneAdjustment.getTime(),
                                zoneAdjustment.getOffset().encode()));
//                sdpZoneAdjustment.setSublist(Boolean.TRUE);
                propertyGroup.addPropertyParm(sdpZoneAdjustment);
            }
        }

        //SDP encryption key [SDP_K]
        Key key = sessionDescription.getKey();
        if (key != null) {
            PropertyParm sdpKey = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_K),
                    createKey(key.getMethod(), key.getKey()));
//            sdpKey.setSublist(Boolean.TRUE);
            propertyGroup.addPropertyParm(sdpKey);
        }

        //SDP zero or more session attributes [SDP_A]
        Vector<AttributeField> attributes = sessionDescription.getAttributes(false);
        if (attributes != null) {
            for (AttributeField attribute : attributes) {
                PropertyParm sdpAttribute = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_A),
                        createAttribute(attribute));
//                sdpAttribute.setSublist(Boolean.TRUE);
                propertyGroup.addPropertyParm(sdpAttribute);
            }
        }

        //SDP active session time [SDP_T]
        Vector<TimeDescriptionImpl> timeDescriptions = sessionDescription.getTimeDescriptions(false);
        if (timeDescriptions != null) {
            for (TimeDescriptionImpl timeDescription : timeDescriptions) {
                TimeField timeField = (TimeField) timeDescription.getTime();
                PropertyParm sdpTime = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_T),
                        createTime(timeField.getStartTime(),
                                timeField.getStopTime()));
//                sdpTime.setSublist(Boolean.TRUE);
                propertyGroup.addPropertyParm(sdpTime);

                Vector<RepeatField> repeatTimes = timeDescription.getRepeatTimes(false);
                if (repeatTimes != null) {
                    for (RepeatField repeatField : repeatTimes) {
                        PropertyParm sdpRepeatTime = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_R),
                                createRepeatTime(repeatField.getRepeatInterval(),
                                        repeatField.getActiveDuration(),
                                        repeatField.getOffsetArray()));
//                        sdpRepeatTime.setSublist(Boolean.TRUE);
                        propertyGroup.addPropertyParm(sdpRepeatTime);
                    }
                }
            }
        }

        //SDP media type, port, transport and format
        Vector<MediaDescriptionImpl> mediaDescriptions = sessionDescription.getMediaDescriptions(false);
        for (MediaDescriptionImpl mediaDescription : mediaDescriptions) {
            PropertyParm sdpMediaDescription = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_M),
                    createMediaDescription(mediaDescription.getMedia().getMediaType(),
                            mediaDescription.getMedia().getMediaPort(),
                            mediaDescription.getMedia().getProtocol(),
                            mediaDescription.getMedia().getMediaFormats(false)));
            propertyGroup.addPropertyParm(sdpMediaDescription);
            attributes = mediaDescription.getAttributes(false);
            if (attributes != null) {
                for (AttributeField attribute : attributes) {
                    PropertyParm sdpAttribute = new PropertyParm(new PropertyName(PACKAGE_ID, SDP_A),
                            createAttribute(attribute));
//                    sdpAttribute.setSublist(Boolean.TRUE);
                    propertyGroup.addPropertyParm(sdpAttribute);
                }
            }

        }

        return propertyGroup;
    }

    private byte[] createVersion(int version) throws AsnException, IOException {
        aos.reset();
        StringBuilder sb = new StringBuilder();
        sb.append(version);
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createOwner(String username, String sessionId, String sessionVersion, String networkType,
            String addressType, String address) throws SdpParseException, AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(username)
                .append(" ")
                .append(sessionId)
                .append(" ")
                .append(sessionVersion)
                .append(" ")
                .append(networkType)
                .append(" ")
                .append(addressType)
                .append(" ")
                .append(address);
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createSessionName(String sessionName) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(sessionName);
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createSessionInformation(String info) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(info);
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createUriOfDescriptor(String uri) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(uri);

        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createEmail(String email) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(email);
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createPhone(String phone) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(phone);
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createConnection(String networkType, String addressType, String address) throws SdpParseException, AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(networkType).
                append(" ").
                append(addressType).
                append(" ").
                append(address);
//                append("\r\n");
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createBandwidth(String modifier, int bandwidth) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(modifier)
                .append(":")
                .append(bandwidth);

        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createTime(long startTime, long stopTime) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(startTime)
                .append(" ")
                .append(stopTime);
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createKey(String method, String key) throws SdpParseException, AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(method);
        if (key != null) {
            sb.append(":")
                    .append(key);
        }

        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createAttribute(AttributeField attribute) throws SdpParseException, AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(attribute.getName());
        if (attribute.getValue() != null && attribute.getValue().trim().length() > 0) {
            sb.append(":").
                    append(attribute.getValue());
        }
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createRepeatTime(int repeatInterval, int activeDuration, int... offsets) throws SdpParseException, AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(repeatInterval)
                .append(" ")
                .append(activeDuration);
        if (offsets != null) {
            for (int offset : offsets) {
                sb.append(" ")
                        .append(offset);
            }
        }
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createZoneAjustment(long time, String offset) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(time)
                .append(" ")
                .append(offset);
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createMediaDescription(String mediaType, int port, String transport, Vector<String> mediaFormats) throws AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(mediaType)
                .append(" ")
                .append(port)
                .append(" ")
                .append(transport);
        if (mediaFormats != null) {
            for (String mediaFormat : mediaFormats) {
                sb.append(" ").append(mediaFormat);
            }
        }
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }

    private byte[] createMediaDescription(String mediaType, int port, String transport, String... mediaFormats) throws SdpParseException, AsnException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(mediaType)
                .append(" ")
                .append(port)
                .append(" ")
                .append(transport);
        if (mediaFormats != null) {
            for (String mediaFormat : mediaFormats) {
                sb.append(" ").append(mediaFormat);
            }
        }
        aos.reset();
        aos.writeStringIA5(sb.toString());
        return aos.toByteArray();
    }
}
