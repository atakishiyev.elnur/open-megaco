package com.linkedlogics.megaco.transaction;

import com.linkedlogics.megaco.parameter.NullType;
import com.linkedlogics.megaco.parameter.Parameter;
import com.linkedlogics.megaco.parameter.SegmentNumber;
import com.linkedlogics.megaco.parameter.TransactionID;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * SegmentReply ::= SEQUENCE
 * {
 * transactionId TransactionID,
 * segmentNumber SegmentNumber,
 * segmentationComplete NULL OPTIONAL,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class SegmentReply implements Transaction {

    private TransactionID transactionId;
    private SegmentNumber segmentNumber;
    private NullType segmentationComplete;

    public SegmentReply() {
    }

    public SegmentReply(TransactionID transactionId, SegmentNumber segmentNumber) {
        this.transactionId = transactionId;
        this.segmentNumber = segmentNumber;
    }

    public SegmentReply(long transactionId, int segmentNumber) {
        this.transactionId = new TransactionID(transactionId);
        this.segmentNumber = new SegmentNumber(segmentNumber);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        transactionId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        segmentNumber.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        if (segmentationComplete != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 2);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.transactionId = new TransactionID();
                    transactionId.decode(ais);
                    break;
                case 1:
                    this.segmentNumber = new SegmentNumber();
                    segmentNumber.decode(ais);
                    break;
                case 2:
                    this.segmentationComplete = new NullType();
                    ais.readNull();
                    break;
            }
        }
    }

    /**
     * @return the transactionId
     */
    @Override
    public TransactionID getTransactionID() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionID(TransactionID transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the segmentNumber
     */
    public SegmentNumber getSegmentNumber() {
        return segmentNumber;
    }

    /**
     * @param segmentNumber the segmentNumber to set
     */
    public void setSegmentNumber(SegmentNumber segmentNumber) {
        this.segmentNumber = segmentNumber;
    }

    /**
     * @return the segmentationComplete
     */
    public NullType getSegmentationComplete() {
        return segmentationComplete;
    }

    /**
     * @param segmentationComplete the segmentationComplete to set
     */
    public void setSegmentationComplete(NullType segmentationComplete) {
        this.segmentationComplete = segmentationComplete;
    }

    @Override
    public TransactionType getTransactionType() {
        return TransactionType.SEGMENT_REPLY;
    }

}
