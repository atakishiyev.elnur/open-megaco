/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.transaction;

import com.linkedlogics.megaco.parameter.Decodable;
import com.linkedlogics.megaco.parameter.Encodable;
import com.linkedlogics.megaco.parameter.TransactionID;

/**
 *
 * @author eatakishiyev
 */
public interface Transaction extends Encodable, Decodable {

    public TransactionType getTransactionType();

    public TransactionID getTransactionID();
}
