/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.transaction;

import com.linkedlogics.megaco.parameter.TransactionID;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TransactionAck ::= SEQUENCE
 * {
 * firstAck TransactionID,
 * lastAck TransactionID OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class TransactionAck implements Transaction {

    private TransactionID firstTransaction;
    private TransactionID lastTransaction;

    public TransactionAck() {
    }

    public TransactionAck(TransactionID firstTransactionId) {
        this.firstTransaction = firstTransactionId;
    }

    public TransactionAck(long firstTransactionId) {
        this.firstTransaction = new TransactionID(firstTransactionId);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        firstTransaction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (lastTransaction != null) {
            lastTransaction.encode(Tag.CLASS_CONTEXT_SPECIFIC, 1, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.firstTransaction = new TransactionID();
                    firstTransaction.decode(ais);
                    break;
                case 1:
                    this.lastTransaction = new TransactionID();
                    lastTransaction.decode(ais);
                    break;
            }
        }
    }

    public TransactionID getFirstTransaction() {
        return firstTransaction;
    }

    public void setFirstTransaction(TransactionID firstTransaction) {
        this.firstTransaction = firstTransaction;
    }

    public TransactionID getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(TransactionID lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    @Override
    public TransactionType getTransactionType() {
        return TransactionType.ACK;
    }

    @Override
    public TransactionID getTransactionID() {
        return firstTransaction;
    }

    
}
