/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.transaction;

import com.linkedlogics.megaco.parameter.TransactionResult;
import com.linkedlogics.megaco.action.ActionReply;
import com.linkedlogics.megaco.action.ActionRequest;
import com.linkedlogics.megaco.command.reply.CommandReply;
import com.linkedlogics.megaco.parameter.CommandRequest;
import com.linkedlogics.megaco.parameter.ContextID;
import com.linkedlogics.megaco.parameter.TransactionID;

/**
 *
 * @author eatakishiyev
 */
public class TransactionFactory {

    private TransactionFactory() {

    }

    public static TransactionRequest createTransactionRequest(long transactionId, ActionRequest... actions) {
        TransactionRequest trRequest = new TransactionRequest(transactionId);
        for (ActionRequest action : actions) {
            trRequest.addAction(action);
        }
        return trRequest;
    }

    public static TransactionPending createTransactionPending(long transactionId) {
        return new TransactionPending(transactionId);
    }

    public static TransactionReply createTransactionReply(long transactionId, TransactionResult trResult) {
        return new TransactionReply(transactionId, trResult);
    }

    public static TransactionResponseAck createTransactionResponseAck(TransactionAck... transactionAcks) {
        TransactionResponseAck trResponseAck = new TransactionResponseAck();
        for (TransactionAck transactionAck : transactionAcks) {
            trResponseAck.addTransactionAck(transactionAck);
        }
        return trResponseAck;
    }

    public static SegmentReply createSegmentReply(long transactionId, int segmentNumber) {
        return new SegmentReply(transactionId, segmentNumber);
    }

    public static TransactionRequest createTranactionRequest(TransactionID transactionID, ContextID contextID, CommandRequest... commandRequests) {
        TransactionRequest transactionRequest = new TransactionRequest(transactionID);
        ActionRequest actionRequest = new ActionRequest(contextID);
        for (CommandRequest commandRequest : commandRequests) {
            actionRequest.addCommandRequest(commandRequest);
        }
        transactionRequest.addAction(actionRequest);
        return transactionRequest;
    }

    public static TransactionReply createTransactionReply(TransactionID transactionID, ContextID contextID, CommandReply... commandReplys) {
        TransactionResult transactionResult = new TransactionResult();
        TransactionReply transactionReply = new TransactionReply(transactionID, transactionResult);
        for (CommandReply commandReply : commandReplys) {
            ActionReply actionReply = new ActionReply();
            actionReply.addCommandReply(commandReply);
            transactionResult.addActionReply(actionReply);
        }
        return transactionReply;
    }

    public static TransactionPending createTransactionPending(TransactionID transactionID) {
        TransactionPending transactionPending = new TransactionPending(transactionID);
        return transactionPending;
    }
}
