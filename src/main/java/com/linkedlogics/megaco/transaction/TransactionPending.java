/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.transaction;

import com.linkedlogics.megaco.parameter.TransactionID;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TransactionPending ::= SEQUENCE
 * {
 * transactionId TransactionID,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class TransactionPending implements Transaction {

    private TransactionID transactionId;

    public TransactionPending() {
    }

    public TransactionPending(TransactionID transactionId) {
        this.transactionId = transactionId;
    }

    public TransactionPending(long transactionId) {
        this.transactionId = new TransactionID(transactionId);
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        transactionId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            this.transactionId = new TransactionID();
            transactionId.decode(ais);
        }
    }

    @Override
    public TransactionID getTransactionID() {
        return transactionId;
    }

    public void setTransactionId(TransactionID transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public TransactionType getTransactionType() {
        return TransactionType.PENDING;
    }

}
