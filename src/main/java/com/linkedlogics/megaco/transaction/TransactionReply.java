/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.transaction;

import com.linkedlogics.megaco.parameter.TransactionResult;
import com.linkedlogics.megaco.parameter.NullType;
import com.linkedlogics.megaco.parameter.SegmentNumber;
import com.linkedlogics.megaco.parameter.TransactionID;
import java.io.IOException;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TransactionReply ::= SEQUENCE
 * {
 * transactionId [0] TransactionID,
 * immAckRequired [1] NULL OPTIONAL,
 *
 * transactionResult [2] CHOICE
 * {
 * transactionError [0] ErrorDescriptor,
 * actionReplies [1] SEQUENCE OF ActionReply
 * },
 * ...,
 * segmentNumber [3] SegmentNumber OPTIONAL,
 * segmentationComplete [4] NULL OPTIONAL
 * }
 *
 * @author eatakishiyev
 */
public class TransactionReply implements Transaction {

    private TransactionID transactionId;
    private NullType immAckRequest;

    private TransactionResult transactionResult;

    private SegmentNumber segmentNumber;
    private NullType segmentationComplete;

    public TransactionReply() {
    }

    public TransactionReply(TransactionID transactionId, TransactionResult transactionResult) {
        this.transactionId = transactionId;
        this.transactionResult = transactionResult;
    }

    public TransactionReply(long transactionId, TransactionResult transactionResult) {
        this.transactionId = new TransactionID(transactionId);
        this.transactionResult = transactionResult;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        transactionId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);
        if (immAckRequest != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 1);
        }

        transactionResult.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);

        if (segmentNumber != null) {
            segmentNumber.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }

        if (segmentationComplete != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 4);
        }

        aos.FinalizeContent(lenPos);
    }

    public byte[] getByteData() throws AsnException, IOException {
        AsnOutputStream aos = new AsnOutputStream();
        transactionId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);

        if (immAckRequest != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 1);
        }

        transactionResult.encode(Tag.CLASS_CONTEXT_SPECIFIC, 2, aos);

        if (segmentNumber != null) {
            segmentNumber.encode(Tag.CLASS_CONTEXT_SPECIFIC, 3, aos);
        }

        if (segmentationComplete != null) {
            aos.writeNull(Tag.CLASS_CONTEXT_SPECIFIC, 4);
        }
        return aos.toByteArray();
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.transactionId = new TransactionID();
                    transactionId.decode(ais);
                    break;
                case 1:
                    this.immAckRequest = new NullType();
                    ais.readNull();
                    break;
                case 2:
                    this.transactionResult = new TransactionResult();
                    transactionResult.decode(ais);
                    break;
                case 3:
                    this.segmentNumber = new SegmentNumber();
                    segmentNumber.decode(ais);
                    break;
                case 4:
                    this.segmentationComplete = new NullType();
                    ais.readNull();
                    break;
            }
        }
    }

    public void setSegmentationComplete(NullType segmentationComplete) {
        this.segmentationComplete = segmentationComplete;
    }

    public SegmentNumber getSegmentNumber() {
        return segmentNumber;
    }

    public NullType getSegmentationComplete() {
        return segmentationComplete;
    }

    public void setSegmentNumber(SegmentNumber segmentNumber) {
        this.segmentNumber = segmentNumber;
    }

    public NullType getImmAckRequest() {
        return immAckRequest;
    }

    public void setImmAckRequest(NullType immAckRequest) {
        this.immAckRequest = immAckRequest;
    }

    public void setTransactionId(TransactionID transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public TransactionID getTransactionID() {
        return transactionId;
    }

    public void setTransactionResult(TransactionResult transactionResult) {
        this.transactionResult = transactionResult;
    }

    public TransactionResult getTransactionResult() {
        return transactionResult;
    }

    @Override
    public TransactionType getTransactionType() {
        return TransactionType.REPLY;
    }

}
