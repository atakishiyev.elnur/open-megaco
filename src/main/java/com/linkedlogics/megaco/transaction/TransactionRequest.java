/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.transaction;

import com.linkedlogics.megaco.action.ActionRequest;
import com.linkedlogics.megaco.parameter.TransactionID;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TransactionRequest ::= SEQUENCE {
 * transactionId [0] TransactionID,
 * actions [1] SEQUENCE OF ActionRequest,
 * ...
 * }
 *
 * @author eatakishiyev
 */
public class TransactionRequest implements Transaction {

    private TransactionID transactionId;
    private final List<ActionRequest> actions = new ArrayList<>();

    public TransactionRequest() {
    }

    public TransactionRequest(long transactionId) {
        this.transactionId = new TransactionID(transactionId);
    }

    public TransactionRequest(TransactionID transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();

        transactionId.encode(Tag.CLASS_CONTEXT_SPECIFIC, 0, aos);

        aos.writeTag(Tag.CLASS_CONTEXT_SPECIFIC, false, 1);
        int lenPos1 = aos.StartContentDefiniteLength();
        for (ActionRequest actionRequest : actions) {
            actionRequest.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos1);

        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            switch (tag) {
                case 0:
                    this.transactionId = new TransactionID();
                    transactionId.decode(ais);
                    break;
                case 1:
                    this.decodeActionRequests(ais.readSequenceStream());
                    break;
            }
        }
    }

    private void decodeActionRequests(AsnInputStream ais) throws IOException, AsnException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            if (tag == Tag.SEQUENCE) {
                ActionRequest actionRequest = new ActionRequest();
                actionRequest.decode(ais.readSequenceStream());
                actions.add(actionRequest);
            } else {
                throw new AsnException("Unexpected tag received: Expecting SEQUENCE, received " + tag);
            }
        }
    }

    public List<ActionRequest> getActions() {
        return actions;
    }

    public void setTransactionId(TransactionID transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public TransactionID getTransactionID() {
        return transactionId;
    }

    public void addAction(ActionRequest actionRequest) {
        actions.add(actionRequest);
    }

    @Override
    public TransactionType getTransactionType() {
        return TransactionType.REQUEST;
    }

}
