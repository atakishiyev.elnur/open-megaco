/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.transaction;

import com.linkedlogics.megaco.parameter.TransactionID;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

/**
 * TransactionResponseAck ::= SEQUENCE OF TransactionAck
 *
 * @author eatakishiyev
 */
public class TransactionResponseAck implements Transaction {

    private final List<TransactionAck> transactionAcks = new ArrayList<>();

    public TransactionResponseAck() {
    }

    @Override
    public void encode(int tagClass, int tag, AsnOutputStream aos) throws AsnException, IOException {
        aos.writeTag(tagClass, false, tag);
        int lenPos = aos.StartContentDefiniteLength();
        for (TransactionAck transactionAck : transactionAcks) {
            transactionAck.encode(Tag.CLASS_UNIVERSAL, Tag.SEQUENCE, aos);
        }
        aos.FinalizeContent(lenPos);
    }

    @Override
    public void decode(AsnInputStream ais) throws AsnException, IOException {
        while (ais.available() > 0) {
            int tag = ais.readTag();
            TransactionAck transactionAck = new TransactionAck();
            transactionAck.decode(ais.readSequenceStream());
            transactionAcks.add(transactionAck);
        }
    }

    public List<TransactionAck> getTransactionAcks() {
        return transactionAcks;
    }

    public void addTransactionAck(TransactionAck transactionAck) {
        this.transactionAcks.add(transactionAck);
    }

    @Override
    public TransactionType getTransactionType() {
        return TransactionType.RESPONSE_ACK;
    }

    @Override
    public TransactionID getTransactionID() {
        return null;
    }

}
