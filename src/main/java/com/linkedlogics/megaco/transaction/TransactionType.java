/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco.transaction;

/**
 *
 * @author eatakishiyev
 */
public enum TransactionType {
    REQUEST(0),
    PENDING(1),
    REPLY(2),
    RESPONSE_ACK(3),
    SEGMENT_REPLY(4),
    ACK(5),
    UNKNOWN(-1);

    private final int value;

    private TransactionType() {
        this.value = 0;
    }

    private TransactionType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

    public static TransactionType getTransactionType(int value) {
        switch (value) {
            case 0:
                return REQUEST;
            case 1:
                return PENDING;
            case 2:
                return REPLY;
            case 3:
                return RESPONSE_ACK;
            case 4:
                return SEGMENT_REPLY;
            case 5:
                return ACK;
            default:
                return UNKNOWN;
        }
    }

}
