
import com.linkedlogics.megaco.MegacoFactory;
import com.linkedlogics.megaco.MegacoStack;
import com.linkedlogics.megaco.OperationMode;
import com.linkedlogics.megaco.command.request.AmmRequest;
import com.linkedlogics.megaco.descriptor.AmmDescriptor;
import com.linkedlogics.megaco.descriptor.LocalRemoteDescriptor;
import com.linkedlogics.megaco.descriptor.MediaDescriptor;
import com.linkedlogics.megaco.parameter.CommandRequest;
import com.linkedlogics.megaco.parameter.MegacoMessage;
import com.linkedlogics.megaco.parameter.Mid;
import com.linkedlogics.megaco.parameter.PathName;
import com.linkedlogics.megaco.parameter.sdp.MediaStreamProperties;
import com.linkedlogics.megaco.transaction.TransactionRequest;
import gov.nist.javax.sdp.SessionDescriptionImpl;
import java.io.IOException;
import javax.sdp.SessionDescription;
import javax.xml.bind.DatatypeConverter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author eatakishiyev
 */
public class TestMGW {

    public static void main(String[] args) throws IOException, Exception {
//        byte[] data = DatatypeConverter.parseHexBinary("308201d8a18201d4800103a10683044d474331a28201c5a18201c1a08201bd800500adfc643fa18201b2308201ae8001fea38201a7308201a3a082019fa082019ba011300fa00304017f81080000000000000000a1820184a0820180a182017ca0820178a2820174a08201703082016c300d80040000b001a1050403160130302780040000b002a11f041d161b5a2030203020494e20495034203137322e31382e3138382e323532300d80040000b003a105040316015a302180040000b008a11904171615494e20495034203137322e31382e3138382e323532300f80040000b00da10704051603302030303080040000b00fa12804261624617564696f203430353838205254502f4156502030203820332031313020393820313031302180040000b00ca119041716157274706d61703a3131302073706565782f38303030301f80040000b00ca117041516137274706d61703a393820694c42432f38303030301b80040000b00ca1130411160f666d74703a3938206d6f64653d3230302b80040000b00ca1230421161f7274706d61703a3130312074656c6570686f6e652d6576656e742f38303030301980040000b00ca111040f160d666d74703a31303120302d3135301480040000b00ca10c040a160873656e6472656376");
//        MegacoFactory megacoFactory = new MegacoFactory(null);
//        MegacoMessage megacoMessage = megacoFactory.createMessage(data);
//        TransactionRequest tr = (TransactionRequest) megacoMessage.getMessage().getMessageBody().getTransactions().get(0);
//        CommandRequest cr = tr.getActions().get(0).getCommandRequests().get(0);
//        AmmRequest addCommand = (AmmRequest) cr.getCommand();
//        MediaDescriptor mediaDescriptor = (MediaDescriptor) addCommand.getDescriptors().get(0);
//        LocalRemoteDescriptor localRemoteDescriptor = mediaDescriptor.getStreams().getOneStream().getRemoteDescriptor();
//
//        MediaStreamProperties mediaStreamProperties = new MediaStreamProperties();
//        SessionDescriptionImpl sd =(SessionDescriptionImpl) mediaStreamProperties.getSessionDescription(localRemoteDescriptor.getPropGrps().get(0));
//        
//        System.out.println(sd);
        
        
        
        MegacoStack.createInstance(OperationMode.MG, new Mid(new PathName("test-mgw")));
    }
}
