/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package megaco.test;

import com.linkedlogics.megaco.Termination;

/**
 *
 * @author eatakishiyev
 */
public class TestTermination extends Termination {

    @Override
    public void onCreated() {
        System.out.println("TerminationCreated");
    }

    @Override
    public void onModified() {
        System.out.println("TerminationModified");
    }

    @Override
    public void onDestroyed(boolean timeOut) {
        System.out.println("TerminationDestroyed");
    }

}
