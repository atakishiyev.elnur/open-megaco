/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.command.AmmRequest;
import com.linkedlogics.megaco.command.AuditRequest;
import com.linkedlogics.megaco.command.NotifyRequest;
import com.linkedlogics.megaco.command.ServiceChangeRequest;
import com.linkedlogics.megaco.command.SubtractRequest;
import com.linkedlogics.megaco.command.reply.NotifyReply;
import com.linkedlogics.megaco.descriptor.error.ErrorDescriptor;
import com.linkedlogics.megaco.parameter.TerminationAudit;
import com.linkedlogics.megaco.parameter.TransactionID;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mobicents.protocols.asn.AsnException;

/**
 *
 * @author eatakishiyev
 */
public class MegacoUser implements MgwListener {

    @Override
    public void onAddRequest(TransactionID transactionID, AmmRequest addRequest, Context context, MegacoTransactionRequest transactionRequest, boolean lastTransaction) {
        transactionRequest.sendAddReply(transactionID, context.getContextID(), addRequest.getTerminationIDList(), new TerminationAudit());
        if (lastTransaction) {
            try {
                transactionRequest.replyTransaction();
            } catch (IOException ex) {
                Logger.getLogger(MegacoUser.class.getName()).log(Level.SEVERE, null, ex);
            } catch (AsnException ex) {
                Logger.getLogger(MegacoUser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void onMoveRequest(TransactionID transactionId, Context context, AmmRequest moveReq, MegacoTransactionRequest transactionRequest, boolean lastTransaction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onModifyRequest(TransactionID transactionId, Context context, AmmRequest modifyReq, MegacoTransactionRequest transactionRequest, boolean lastTransaction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onSubtractRequest(TransactionID transactionId, Context context, SubtractRequest subtractReq, MegacoTransactionRequest transactionRequest, boolean lastTransaction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onAuditCapRequest(TransactionID transactionId, Context context, AuditRequest auditCapRequest, MegacoTransactionRequest transactionRequest, boolean lastTransaction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onAuditValueRequest(TransactionID transactionId, Context context, AuditRequest auditRequest, MegacoTransactionRequest transactionRequest, boolean lastTransaction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onNotifyRequest(TransactionID transactionId, Context context, NotifyRequest notifyRequest, MegacoTransactionRequest transactionRequest, boolean lastTransaction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onServiceChangeRequest(TransactionID transactionId, Context context, ServiceChangeRequest serviceChangeRequest, MegacoTransactionRequest transactionRequest, boolean lastTransaction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onNotifyReply(TransactionID transactionId, Context context, NotifyReply notifyReply) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onError(TransactionID transactionId, Context context, ErrorDescriptor errorDescriptor) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
