/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.megaco;

import com.linkedlogics.megaco.parameter.MegacoMessage;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.sdp.SdpFactory;
import javax.sdp.SessionDescription;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author eatakishiyev
 */
public class Test {

    public static void main(String[] args) throws Exception {
        HashMap<byte[], Integer> ss= new HashMap<>();
        ss.put(new byte[]{0x01, 0x02, 0x03}, Integer.MIN_VALUE);
        System.out.println(ss.get(new byte[]{0x01,0x02, 0x03}));
        
        byte[] b1 = {0x01, 0x02, 0x03};
        byte[] b2 = {0x01, 0x02, 0x03};
        
        System.out.println(Arrays.hashCode(b1));
        System.out.println(Arrays.hashCode(b2));
        
        SecureRandom secureRandom = new SecureRandom();
        for(int i =0 ; i < 10000; i++){
//            System.out.println(secureRandom.nextInt(Integer.MAX_VALUE));
        }
        
        //3064a162800102a106840400003deaa255a153a05180041900a3a9a149304780040400157da33f303da03ba239a00a3008a0008104489ebf8ea12ba029a127a025a023800102a31e300d8004001e0001a10504030a0105300d8004000d0008a1050403010101
        //3035a133800102a106840400003de6a226a124a22280041900b3d0a21aa11830168004050004aea30ea20ca00a3008a0008104489ecc46
        //3035a133800102a106840400003de6a226a124a22280041900b3c7a21aa1183016800404000045a30ea30ca00a3008a0008104489ed1a4
        //3064a162800102a106840400003deaa255a153a05180041900a3a9a149304780040400157da33f303da03ba239a00a3008a0008104489ebf8ea12ba029a127a025a023800102a31e300d8004001e0001a10504030a0105300d8004000d0008a1050403010101
        //3037a135800102a106840400003deaa228a126a02480041900b3b8a11c301a8002009ca3143012a010a30ea00a3008a0008104489ecde2a100
        //3081eea181eb800102a106840400003deaa281dda181daa081d780041900b3b3a181ce3081cb800500fffffffea381c13081bea081bba081b8a00a3008a0008104480feeb9a181a9a066a164a062a014800102a30f300d8004001e0001a10504030a0105a124a0223020300d800400009001a1050403040100300f800400009023a107040504038090a3a224a0223020300d800400009001a1050403040100300f800400009023a107040504038090a3a33f800500a0002056a1363010800400010001810101a2038001ffa3003010800400010002810101a2038001ffa3003010800400980001810101a2038001ffa300
        //3037a135800102a106840400003deaa228a126a02480041900b3b8a11c301a8002009ca3143012a010a30ea00a3008a0008104489ecde2a100
        //dtmf-tone generator //304ea14c800102a106840400003deaa23fa13da03b80041900b349a1333031800403000d54a3293027a025a223a00a3008a0008104489f41f0a115a513a011800400050010810101820101850100a600
        //failure-cause //3081faa181f7800102a106840400003deaa281e9a181e6a081e380041900b33da181da3081d780020097a381d03081cda081caa081c7a00a3008a0008104489ece28a181b8a075a173a071a023800101a31e300d8004001e0001a10504030a0105300d8004000d0008a1050403010101a124a0223020300d800400009001a1050403040103300f800400009023a107040504039090a3a224a0223020300d800400009001a1050403040103300f800400009023a107040504039090a3a33f800500b00022caa1363010800400010001810101a2038001ffa3003010800400010002810101a2038001ffa3003010800400980001810101a2038001ffa300

        //test-error //30820131a182012d800102a10ca00a8004ac12bcfc810213c4a2820118a1820114a0820110800204d1a1820108308201048001fea381fe3081fba081f8a081f5a00a3008a003040100810100a181e6a081e3a181e0a081dda281daa081d73081d4300f80040000b001a1050403300d0ab000302d80040000b002a12304215a6f697065722030203020494e20495034203137322e31382e3230312e33310d0ab000301480040000b003a10a04085a6f697065720d0ab000302280040000b008a1180416494e20495034203137322e31382e3230312e33310d0ab000302b80040000b00da121041f2d32323038393838383030303030202d323230383938383830303030300d0ab000302b80040000b00fa121041f617564696f203438353830205254502f415650203320302038203130310d0ab000
        String sdpStr = "v=0\n"
                + "o=yate 1486657354 1486657354 IN IP4 10.13.44.142\n"
                + "s=SIP Call\n"
                + "c=IN IP4 10.13.44.142\n"
                + "t=0 0\n"
                + "m=audio 18572 RTP/AVP 8 101\n"
                + "a=rtpmap:8 PCMA/8000\n"
                + "a=rtpmap:101 telephone-event/8000";

        SessionDescription sdp = SdpFactory.getInstance().createSessionDescription(sdpStr);
        com.linkedlogics.megaco.parameter.sdp.SessionDescription sessionDescription = new com.linkedlogics.megaco.parameter.sdp.SessionDescription(sdp);

        ExecutorService executor = Executors.newFixedThreadPool(10);
//        Future<String> result = executor.submit(new MyCallable());

        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(255);
        bb.flip();
        System.out.println(DatatypeConverter.printHexBinary(bb.array()));

//        ByteBuffer b1 = ByteBuffer.allocate(8);
//        b1.put(bb.array());
//        b1.flip();
//
//        System.out.println(b1.getInt());
//        System.out.println(result.get());

        MegacoStack megacoStack = MegacoStack.getInstance();
//        MegacoFactory messageFactory = megacoStack.createMessageFactory();

        MegacoMessage message = megacoStack.getMessageFactory().createMessage(DatatypeConverter.parseHexBinary("30820123a182011f800102a10ca00a8004ac12bcfc810213c4a282010aa1820106a0820102800204d1a181fb3081f88001fea381f23081efa081eca081e9a00a3008a003040100810100a181daa081d7a181d4a081d1a281cea081cb3081c8300d80040000b001a1050403300d0a302b80040000b002a12304215a6f697065722030203020494e20495034203137322e31382e3230312e33310d0a301280040000b003a10a04085a6f697065720d0a302080040000b008a1180416494e20495034203137322e31382e3230312e33310d0a302980040000b00da121041f2d32323038393838383030303030202d323230383938383830303030300d0a302980040000b00fa121041f617564696f203438353832205254502f415650203320302038203130310d0a"));

        com.linkedlogics.megaco.parameter.sdp.SessionDescription sessionDescription1
                = new com.linkedlogics.megaco.parameter.sdp.SessionDescription(message.getMessage().getMessageBody().getTransactions().get(0)
                        .getTransactionRequest().getActions().get(0).getCommandRequests()
                        .get(0).getCommand().getAddRequest().getDescriptors().get(0).
                        getMediaDescriptor().getStreams().getOneStream().
                        getRemoteDescriptor().getPropGrps().get(0));

        int port = sessionDescription1.getMediaDescriptions().getMedia().getMediaPort();

        String remoteAddress = sessionDescription1.getConnectionInformation().getAddress();
        int remotePort = sessionDescription1.getMediaDescriptions().getMedia().getMediaPort();
        //message.getMessage().getMessageBody().getTransactions().get(0).getTransactionRequest().getActions().get(0).getCommandRequests().get(0)
        //      .getCommand().getAddRequest().getTerminationIDList().getTerminationIDs().get(0).getIdAsLong();
        System.out.println("");
//        SessionDescription sessionDescription = new SessionDescription();
//        sessionDescription.setConnectionInformation("IN", "IP4", "192.168.1.1");
//        sessionDescription.setSessionName("test-session");
//        sessionDescription.getPropertyParms();
    }

}
